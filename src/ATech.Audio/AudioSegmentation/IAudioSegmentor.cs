﻿using System.Collections.Generic;
using ATech.Audio.Interfaces;

namespace ATech.Audio.AudioSegmentation
{
    public interface IAudioSegmentor
    {
        IAudioDescriptor CropBySilence(IAudioDescriptor descriptor);

        List<int> FramesCountByFragmentWithoutSilenceFirstChannel { get; set; }

        List<int> FramesCountByFragmentWithoutSilenceSecondChannel { get; set; }

        string FragmentsDir { get; set; }

        void RemoveFragmentsDir();
    }
}

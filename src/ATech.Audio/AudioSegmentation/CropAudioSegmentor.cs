﻿using ATech.Audio.Interfaces;
using ATech.Configuration;
using ATech.Configuration.Types;
using ATech.Logging.Interfaces;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ATech.Audio.AudioSegmentation
{
    public class CropAudioSegmentor : IAudioSegmentor
    {
        #region Data

        public List<int> FramesCountByFragmentWithoutSilenceFirstChannel { get; set; }

        public List<int> FramesCountByFragmentWithoutSilenceSecondChannel { get; set; }

        public string FragmentsDir { get; set; }

        #endregion

        #region Private fields

        private IAudioDescriptor _audioDescriptor;

        private int[] _framesFirstChannel;

        private int[] _framesSecondChannel;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        private double _frameLengthSeconds;

        #endregion

        #region Public methods

        public void RemoveFragmentsDir()
        {
            if (string.IsNullOrEmpty(FragmentsDir)) return;
            if (Directory.Exists(FragmentsDir))
                Directory.Delete(FragmentsDir, true);
        }

        public IAudioDescriptor CropBySilence(IAudioDescriptor descriptor)
        {
            return descriptor.AudioChannelsType == AudioChannelsType.BothVoicesInOneChannel
                ? CropBySilenceBothVoicesInOneChannel(descriptor)
                : CropBySilenceOneVoiceInEveryChannel(descriptor);
        }

        public void CropFileForDatasets(string srcFile, string resultDir)
        {
            try
            {
                var amplitudes = GetFramesAmplitudesTwoChannels(srcFile);

                var amplitudeThresholdLeft = 30;
                var silenceThresholdLeft = 1;
                var amplitudeThresholdRight = 30;
                var silenceThresholdRight = 1;

                var framesCount = Math.Min(amplitudes[0].Count, amplitudes[1].Count);

                _framesFirstChannel = new int[framesCount];
                _framesSecondChannel = new int[framesCount];

                using (var reader = new Mp3FileReader(srcFile))
                {
                    var mp3Frame = reader.ReadNextFrame();
                    var idxFrame = 0;

                    while (mp3Frame != null)
                    {
                        var sumLeft = amplitudes[0][idxFrame];
                        var sumRight = amplitudes[1][idxFrame];

                        if (sumLeft > amplitudeThresholdLeft)
                            _framesFirstChannel[idxFrame] = 1;

                        if (sumRight > amplitudeThresholdRight)
                            _framesSecondChannel[idxFrame] = 1;

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    _frameLengthSeconds = reader.TotalTime.TotalSeconds / framesCount;
                }

                _framesFirstChannel = RemoveSilence(silenceThresholdLeft, _framesFirstChannel);
                _framesSecondChannel = RemoveSilence(silenceThresholdRight, _framesSecondChannel);

                using (var reader = new Mp3FileReader(srcFile))
                {
                    var mp3Frame = reader.ReadNextFrame();
                    var idxPart = 1;
                    var idxFrame = 0;

                    var tmpPathFirstChannel = Path.Combine(resultDir,
                        Path.GetFileNameWithoutExtension(srcFile) + "tmp_first_channel.mp3");
                    var tmpPathSecondChannel = Path.Combine(resultDir,
                        Path.GetFileNameWithoutExtension(srcFile) + "tmp_second_channel.mp3");

                    var fsFirstChannel = new FileStream(tmpPathFirstChannel, FileMode.Create, FileAccess.Write);
                    var fsSecondChannel = new FileStream(tmpPathSecondChannel, FileMode.Create, FileAccess.Write);

                    fsFirstChannel.Close();
                    fsSecondChannel.Close();

                    File.Delete(tmpPathFirstChannel);
                    File.Delete(tmpPathSecondChannel);

                    var partResultFile = Path.Combine(resultDir, Path.GetFileNameWithoutExtension(srcFile));

                    var soundFinishedLeft = true;
                    var soundFinishedRight = true;

                    var soundStartedLeft = false;
                    var soundStartedRight = false;
                    var emptyBlock = new byte[mp3Frame.RawData.Length];

                    while (mp3Frame != null)
                    {
                        string num;
                        if (_framesSecondChannel[idxFrame] == 1)
                        {
                            if (soundFinishedRight)
                            {
                                num = Convert.ToString(100000 + idxPart).Substring(1);
                                fsSecondChannel = new FileStream($"{partResultFile}&part{num}_second.mp3", FileMode.Create,
                                    FileAccess.Write);
                                idxPart++;
                                soundFinishedRight = false;
                            }

                            soundStartedRight = true;
                            fsSecondChannel.Write(mp3Frame.RawData, 0, mp3Frame.RawData.Length);
                        }
                        else
                        {
                            if (soundStartedRight)
                            {
                                for (var i = 0; i < 1; i++)
                                    fsSecondChannel.Write(emptyBlock, 0, mp3Frame.RawData.Length); //Add final silence
                                fsSecondChannel.Close();
                            }

                            soundStartedRight = false;
                            soundFinishedRight = true;
                        }

                        if (_framesFirstChannel[idxFrame] == 1)
                        {
                            if (soundFinishedLeft)
                            {
                                num = Convert.ToString(100000 + idxPart).Substring(1);
                                fsFirstChannel = new FileStream($"{partResultFile}&part{num}_first.mp3", FileMode.Create,
                                    FileAccess.Write);
                                idxPart++;
                                soundFinishedLeft = false;
                            }

                            soundStartedLeft = true;
                            fsFirstChannel.Write(mp3Frame.RawData, 0, mp3Frame.RawData.Length);
                        }
                        else
                        {
                            if (soundStartedLeft)
                            {
                                for (var i = 0; i < 1; i++)
                                    fsFirstChannel.Write(emptyBlock, 0, mp3Frame.RawData.Length);
                                fsFirstChannel.Close();
                            }

                            soundStartedLeft = false;
                            soundFinishedLeft = true;
                        }

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    if (!soundFinishedRight)
                        fsSecondChannel.Close();

                    if (!soundFinishedLeft)
                        fsFirstChannel.Close();
                }

            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CropAudioSegmentor CropFilesForDatasets");
            }
        }

        #endregion

        #region Private methods

        private IAudioDescriptor CropBySilenceOneVoiceInEveryChannel(IAudioDescriptor descriptor)
        {
            _audioDescriptor = descriptor;

            try
            {
                FragmentsDir =
                    Path.Combine(StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.TmpSrcFragmentsDir,
                        Guid.NewGuid().ToString());
                Directory.CreateDirectory(FragmentsDir);

                var srcPath = _audioDescriptor.MainAudioFilePath;

                var amplitudes = GetFramesAmplitudesTwoChannels(srcPath);

                var amplitudeThresholdLeft = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceAmplitudeThresholdFirstChannel;
                var silenceThresholdLeft = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceMaxCountFramesForBeingNoizeFirstChannel;

                var amplitudeThresholdRight = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceAmplitudeThresholdSecondChannel;
                var silenceThresholdRight = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceMaxCountFramesForBeingNoizeSecondChannel;

                var framesCount = Math.Min(amplitudes[0].Count, amplitudes[1].Count);

                _framesFirstChannel = new int[framesCount];
                _framesSecondChannel = new int[framesCount];

                using (var reader = new Mp3FileReader(srcPath))
                {
                    _audioDescriptor.AudioDialogDetails.AudioDialogDuration = reader.TotalTime;
                    var mp3Frame = reader.ReadNextFrame();
                    var idxFrame = 0;

                    while (mp3Frame != null)
                    {
                        var sumLeft = amplitudes[0][idxFrame];
                        var sumRight = amplitudes[1][idxFrame];

                        if (sumLeft > amplitudeThresholdLeft)
                            _framesFirstChannel[idxFrame] = 1;

                        if (sumRight > amplitudeThresholdRight)
                            _framesSecondChannel[idxFrame] = 1;

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    _frameLengthSeconds = reader.TotalTime.TotalSeconds / framesCount;
                }

                _framesFirstChannel = RemoveSilence(silenceThresholdLeft, _framesFirstChannel);
                _framesSecondChannel = RemoveSilence(silenceThresholdRight, _framesSecondChannel);

                using (var reader = new Mp3FileReader(srcPath))
                {
                    var mp3Frame = reader.ReadNextFrame();
                    var idxPart = 1;
                    var idxFrame = 0;

                    var tmpPathFirstChannel = Path.Combine(FragmentsDir,
                        Path.GetFileNameWithoutExtension(srcPath) + "tmp_first_channel.mp3");
                    var tmpPathSecondChannel = Path.Combine(FragmentsDir,
                        Path.GetFileNameWithoutExtension(srcPath) + "tmp_second_channel.mp3");

                    var fsFirstChannel = new FileStream(tmpPathFirstChannel, FileMode.Create, FileAccess.Write);
                    var fsSecondChannel = new FileStream(tmpPathSecondChannel, FileMode.Create, FileAccess.Write);

                    fsFirstChannel.Close();
                    fsSecondChannel.Close();

                    File.Delete(tmpPathFirstChannel);
                    File.Delete(tmpPathSecondChannel);

                    var partResultFile = Path.Combine(FragmentsDir, Path.GetFileNameWithoutExtension(srcPath));

                    var soundFinishedLeft = true;
                    var soundFinishedRight = true;

                    var soundStartedLeft = false;
                    var soundStartedRight = false;
                    var emptyBlock = new byte[mp3Frame.RawData.Length];

                    while (mp3Frame != null)
                    {
                        string num;
                        if (_framesSecondChannel[idxFrame] == 1)
                        {
                            if (soundFinishedRight)
                            {
                                num = Convert.ToString(100000 + idxPart).Substring(1);
                                fsSecondChannel = new FileStream($"{partResultFile}&part{num}_second.mp3", FileMode.Create,
                                    FileAccess.Write);
                                idxPart++;
                                soundFinishedRight = false;
                            }

                            soundStartedRight = true;
                            fsSecondChannel.Write(mp3Frame.RawData, 0, mp3Frame.RawData.Length);
                        }
                        else
                        {
                            if (soundStartedRight)
                            {
                                for (var i = 0; i < 1; i++)
                                    fsSecondChannel.Write(emptyBlock, 0, mp3Frame.RawData.Length); //Add final silence
                                fsSecondChannel.Close();
                            }

                            soundStartedRight = false;
                            soundFinishedRight = true;
                        }

                        if (_framesFirstChannel[idxFrame] == 1)
                        {
                            if (soundFinishedLeft)
                            {
                                num = Convert.ToString(100000 + idxPart).Substring(1);
                                fsFirstChannel = new FileStream($"{partResultFile}&part{num}_first.mp3", FileMode.Create,
                                    FileAccess.Write);
                                idxPart++;
                                soundFinishedLeft = false;
                            }

                            soundStartedLeft = true;
                            fsFirstChannel.Write(mp3Frame.RawData, 0, mp3Frame.RawData.Length);
                        }
                        else
                        {
                            if (soundStartedLeft)
                            {
                                for (var i = 0; i < 1; i++)
                                    fsFirstChannel.Write(emptyBlock, 0, mp3Frame.RawData.Length);
                                fsFirstChannel.Close();
                            }

                            soundStartedLeft = false;
                            soundFinishedLeft = true;
                        }

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    if (!soundFinishedRight)
                        fsSecondChannel.Close();

                    if (!soundFinishedLeft)
                        fsFirstChannel.Close();
                }

                GenerateFragmentsData();

                FramesCountByFragmentWithoutSilenceFirstChannel =
                    CalculateFramesCountByFragmentWithoutSilenceForChannel(_framesFirstChannel);
                FramesCountByFragmentWithoutSilenceSecondChannel =
                    CalculateFramesCountByFragmentWithoutSilenceForChannel(_framesSecondChannel);

                CalculateSilenceDurationOneVoiceInEveryChannel();
                CalculateMaxSilenceDurationOneVoiceInEveryChannel();
                CalculateInterruptsCountForOneVoiceInEveryChannelType();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CropAudioSegmentor CropBySilenceOneVoiceInEveryChannel");
            }

            return _audioDescriptor;
        }

        private IAudioDescriptor CropBySilenceBothVoicesInOneChannel(IAudioDescriptor descriptor)
        {
            _audioDescriptor = descriptor;

            try
            {
                FragmentsDir =
                    Path.Combine(StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.TmpSrcFragmentsDir,
                        Guid.NewGuid().ToString());
                Directory.CreateDirectory(FragmentsDir);

                var srcPath = _audioDescriptor.MainAudioFilePath;

                var amplitudes = GetFramesAmplitudes(srcPath);

                var amplitudeThreshold = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceAmplitudeThresholdFirstChannel;
                var silenceThreshold = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CropBySilenceMaxCountFramesForBeingNoizeFirstChannel;

                _framesFirstChannel = new int[amplitudes.Count];
                using (var reader = new Mp3FileReader(srcPath))
                {
                    _audioDescriptor.AudioDialogDetails.AudioDialogDuration = reader.TotalTime;
                    var mp3Frame = reader.ReadNextFrame();
                    var idxFrame = 0;

                    while (mp3Frame != null)
                    {
                        var sum = amplitudes[idxFrame];
                        if (sum > amplitudeThreshold)
                            _framesFirstChannel[idxFrame] = 1;

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    _frameLengthSeconds = reader.TotalTime.TotalSeconds / _framesFirstChannel.Length;
                }

                _framesFirstChannel = RemoveSilence(silenceThreshold, _framesFirstChannel);

                using (var reader = new Mp3FileReader(srcPath))
                {
                    var mp3Frame = reader.ReadNextFrame();
                    var idxPart = 1;
                    var idxFrame = 0;

                    var pathTmp = Path.Combine(FragmentsDir,
                        Path.GetFileNameWithoutExtension(srcPath) + "&parttmp.mp3");
                    var fs = new FileStream(pathTmp, FileMode.Create, FileAccess.Write);
                    fs.Close();
                    File.Delete(pathTmp);
                    var partResultFile = Path.Combine(FragmentsDir, Path.GetFileNameWithoutExtension(srcPath));

                    var soundFinished = false;
                    var soundStarted = false;
                    var emptyBlock = new byte[mp3Frame.RawData.Length];
                    while (mp3Frame != null)
                    {
                        if (_framesFirstChannel[idxFrame] == 1)
                        {
                            if (soundFinished)
                            {
                                var num = Convert.ToString(100000 + idxPart).Substring(1);
                                fs = new FileStream($"{partResultFile}&part{num}.mp3", FileMode.Create,
                                    FileAccess.Write);
                                idxPart++;
                                soundFinished = false;
                            }

                            soundStarted = true;
                            fs.Write(mp3Frame.RawData, 0, mp3Frame.RawData.Length);
                        }
                        else
                        {
                            if (soundStarted)
                            {
                                for (var i = 0; i < 1; i++)
                                    fs.Write(emptyBlock, 0, mp3Frame.RawData.Length); //Add final silence
                                fs.Close();
                            }

                            soundStarted = false;
                            soundFinished = true;
                        }

                        mp3Frame = reader.ReadNextFrame();
                        idxFrame++;
                    }

                    if (!soundFinished)
                        fs.Close();
                }

                GenerateFragmentsData();
                FramesCountByFragmentWithoutSilenceFirstChannel =
                    CalculateFramesCountByFragmentWithoutSilenceForChannel(_framesFirstChannel);

                CalculateSilenceDurationBothVoicesInOneChannel();
                CalculateMaxSilenceDurationBothVoicesInOneChannel();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CropAudioSegmentor CropBySilenceBothVoicesInOneChannel");
            }

            return _audioDescriptor;
        }

        private void GenerateFragmentsData()
        {
            if (string.IsNullOrEmpty(FragmentsDir))
            {
                _logger.LogError("Fragments directory is empty", "CropAudioSegmentor GenerateFragmentsData");
                return;
            }

            var fragmentsPathes = Directory.GetFiles(FragmentsDir);
            _audioDescriptor.Fragments = new List<IAudioFragmentDescriptor>();
            foreach (var fragmentsPath in fragmentsPathes)
            {
                IAudioFragmentDescriptor fragment = new AudioFragmentDescriptor
                {
                    SrcFilePath = fragmentsPath
                };
                _audioDescriptor.Fragments.Add(fragment);
            }
        }

        private static int GetFrameLength(string audioFilePath)
        {
            using (var reader = new Mp3FileReader(audioFilePath))
            {
                var mp3Frame = reader.ReadNextFrame();
                var frameLength = mp3Frame.FrameLength;
                return frameLength;
            }
        }

        private static int CalcSumInFrame(byte[] frame)
        {
            var max = 0;
            for (var i = 0; i < frame.Length / 2; i++)
            {
                var num = Math.Abs(BitConverter.ToInt16(frame, i * 2));
                if (num > max)
                    max = num;
            }

            return max;
        }

        private static int CalcSumInFrameChannel(byte[] frame, int channelIdx)
        {
            var max = 0;
            for (var i = 0; i < frame.Length; i += 4)
            {

                var num = Math.Abs(BitConverter.ToInt16(frame, i + 2 * channelIdx));
                if (num > max)
                    max = num;
            }

            return max;
        }

        private static List<int> GetFramesAmplitudes(string srcPath)
        {
            const int length = 16 * 72;
            var result = new List<int>();
            using (var reader = new Mp3FileReader(srcPath))
            {
                var buffer = new byte[length];
                using (var pcm = WaveFormatConversionStream.CreatePcmStream(reader))
                {
                    while (pcm.Position < pcm.Length)
                    {
                        var rc = pcm.Read(buffer, 0, length);
                        if (rc == 0)
                            break;
                        result.Add(CalcSumInFrame(buffer));
                    }
                }
            }

            return result;
        }

        private static List<List<int>> GetFramesAmplitudesTwoChannels(string srcPath)
        {
            const int length = 16 * 72 * 2;
            var result = new List<List<int>>();
            var left = new List<int>();
            var right = new List<int>();
            using (var reader = new Mp3FileReader(srcPath))
            {
                var buffer = new byte[length];
                using (var pcm = WaveFormatConversionStream.CreatePcmStream(reader))
                {
                    while (pcm.Position < pcm.Length)
                    {
                        var rc = pcm.Read(buffer, 0, length);
                        if (rc == 0)
                            break;
                        left.Add(CalcSumInFrameChannel(buffer, 0));
                        right.Add(CalcSumInFrameChannel(buffer, 1));
                    }
                }
            }

            result.Add(left);
            result.Add(right);
            return result;
        }

        private static int[] RemoveSilence(int silenceFramesMaxCount, int[] frames)
        {
            var i = 0;
            while (i < frames.Length)
            {
                if (frames[i] == 0)
                {
                    var count = -1;
                    for (var j = i; j < frames.Length; j++)
                    {
                        if (frames[j] == 0)
                            count++;

                        if (frames[j] == 1)
                            break;
                    }

                    if (count < silenceFramesMaxCount)
                    {
                        for (var j = i; j <= (i + count); j++)
                        {
                            frames[j] = 1;
                        }
                    }

                    i += (count + 1);
                }
                else
                {
                    i++;
                }
            }

            return frames;
        }

        private static List<int> CalculateFramesCountByFragmentWithoutSilenceForChannel(IEnumerable<int> frames)
        {
            var result = new List<int>();
            var currentFragmentFramesCount = 0;
            foreach (var frame in frames)
            {
                if (frame == 1)
                {
                    currentFragmentFramesCount++;
                }
                else
                {
                    if (currentFragmentFramesCount != 0)
                        result.Add(currentFragmentFramesCount);
                    currentFragmentFramesCount = 0;
                }
            }

            if (currentFragmentFramesCount != 0)
                result.Add(currentFragmentFramesCount);

            return result;
        }

        private void CalculateSilenceDurationBothVoicesInOneChannel()
        {
            var framesSilenceCount = _framesFirstChannel.Count(frame => frame == 0);
            _audioDescriptor.AudioDialogDetails.SilenceDurationInSeconds =
                Math.Round(framesSilenceCount * _frameLengthSeconds, 2);
        }

        private void CalculateSilenceDurationOneVoiceInEveryChannel()
        {
            var framesCount = _framesFirstChannel.Length;
            var framesSilenceCount = 0;
            for (var i = 0; i < framesCount; i++)
            {
                if (_framesFirstChannel[i] == 0 && _framesSecondChannel[i] == 0)
                    framesSilenceCount++;
            }

            _audioDescriptor.AudioDialogDetails.SilenceDurationInSeconds = 
                Math.Round(framesSilenceCount * _frameLengthSeconds, 2);

        }

        private void CalculateMaxSilenceDurationBothVoicesInOneChannel()
        {
            var currentCount = 0;
            var maxCount = -1;

            foreach (var frame in _framesFirstChannel)
            {
                if (frame == 0)
                {
                    currentCount++;
                    if (currentCount > maxCount)
                        maxCount = currentCount;
                }
                else
                    currentCount = 0;
            }

            _audioDescriptor.AudioDialogDetails.MaxSilenceIntervalInSeconds =
                Math.Round(maxCount * _frameLengthSeconds, 2);
        }

        private void CalculateMaxSilenceDurationOneVoiceInEveryChannel()
        {
            var framesCount = _framesFirstChannel.Length;
            var currentCount = 0;
            var maxCount = -1;

            for (var i = 0; i < framesCount; i++)
            {
                if (_framesFirstChannel[i] == 0 && _framesSecondChannel[i] == 0)
                {
                    currentCount++;
                    if (currentCount > maxCount)
                        maxCount = currentCount;
                }
                else
                    currentCount = 0;
            }

            _audioDescriptor.AudioDialogDetails.MaxSilenceIntervalInSeconds =
                Math.Round(maxCount * _frameLengthSeconds, 2);
        }

        private void CalculateInterruptsCountForOneVoiceInEveryChannelType()
        {
            // calc logic and channel
            var framesCount = _framesFirstChannel.Length;
            var logicAndChannel = new int[framesCount];
            for (var i = 0; i < framesCount; i++)
                logicAndChannel[i] = _framesFirstChannel[i] * _framesSecondChannel[i];

            // calc one time fragments count
            var oneTimeFragmentsCount = CalculateFramesCountByFragmentWithoutSilenceForChannel(logicAndChannel);
            _audioDescriptor.AudioDialogDetails.InterruptsCount = oneTimeFragmentsCount.Count;

            var middleOneTimeSpeechDuration = 0.0;
            foreach (var item in oneTimeFragmentsCount)
                middleOneTimeSpeechDuration += item;

            middleOneTimeSpeechDuration /= oneTimeFragmentsCount.Count;
            _audioDescriptor.AudioDialogDetails.MiddleDurationOneTimeSpeech = Math.Round(middleOneTimeSpeechDuration * _frameLengthSeconds, 2);
        }

        #endregion
    }
}

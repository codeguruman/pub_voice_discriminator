﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ATech.Audio.AudioProcessing;
using ATech.Audio.AudioSegmentation;
using ATech.Audio.SpeechRecognition;
using ATech.Audio.VoiceSeparation;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.Common.Dal.Models;
using ATech.Configuration.Types;

namespace ATech.Audio.Interfaces
{
    public interface IAudioDescriptor
    {
        string MainAudioFilePath { get; set; }

        string SecondMainAudioFilePath { get; set; }

        DateTime CreationDate { get; set; }

        AudioChannelsType AudioChannelsType { get; set; }

        string Name { get; set; }

        string OperatorId { get; set; }

        string ClientId { get; set; }

        string TmpTransformForVoicesIdentifyFilesDir { get; set; }

        string TmpTransformForRecognizingFilesDir { get; set; }

        List<IAudioFragmentDescriptor> Fragments { get; set; }

        IAudioDialogDetails AudioDialogDetails { get; set; }

        void Create(IAudioConvertingComposite audioConvertingComposite, string filePath, string secondFilePath);

        IAudioDescriptor Segment(IAudioSegmentor segmentor);

        void CalculateRoleDuration(IAudioSegmentor segmentor);

        Task IdentifySpeakersBothVoicesInOnChannel(IVoicesSeparator voicesIdentifier);

        Task IdentifySpeakersOneVoiceInEveryChannel(IVoicesSeparator voicesIdentifier);

        void IdentifySpeakersOneVoiceInEveryChannelWhenOperatorNameFromVocabularyOrFileName();

        void GetTextAndRoleFromFragment(IVoicesSeparator voicesIdentifier, ISpeechRecognizingStrategy primarySpeechRecognizer, ISpeechRecognizingStrategy secondarySpeechRecognizer = null, List<string> operatorNamesVocabulary = null);

        Task GetDialogFromFullFile(int channelsCount, RecognizerType recognizerType, string language, bool onlyRoles = false, bool useUsedeskKeys = true);

        Task Recognize(ISpeechRecognizingStrategy primarySpeechRecognizer, ISpeechRecognizingStrategy secondarySpeechRecognizer = null);

        Task RecognizeWithYandex(int channelsCount);

        Task RecognizeWithYandexAlloka(int channelsCount);

        Task RecognizeWithKaldi(int channelsCount, string language);

        IDialogDescriptor GenerateDialog();

        void BackupAudioFile(bool forceIntoCorrupted = false);
        void ClearTmpDirs();
    }
}

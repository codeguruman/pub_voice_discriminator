﻿namespace ATech.Audio.Interfaces
{
    public interface IAudioFragmentDescriptor
    {
        string SrcFilePath { get; set; }
        string RecognizedText { get; set; }
        string AuthorId { get; set; }
        string AuthorType { get; set; }
    }
}

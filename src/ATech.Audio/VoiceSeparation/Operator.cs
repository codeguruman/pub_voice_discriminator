﻿namespace ATech.Audio.VoiceSeparation
{
    public class Operator
    {
        public string ProfileId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
    }
}

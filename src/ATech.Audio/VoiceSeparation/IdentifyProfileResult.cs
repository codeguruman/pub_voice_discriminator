﻿namespace ATech.Audio.VoiceSeparation
{
    public class IdentifyProfileResult
    {
        public string ProfileId { get; set; } //identifiedProfileId

        public string Confidence { get; set; } // confidence
    }
}

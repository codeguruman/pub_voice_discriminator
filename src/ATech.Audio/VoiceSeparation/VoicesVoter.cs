﻿using System.Collections.Generic;

namespace ATech.Audio.VoiceSeparation
{
    public class VoicesVoter
    {
        private readonly int _attempts;

        private readonly Dictionary<string, int> _profilesIncidence;

        public VoicesVoter(int attempts)
        {
            _attempts = attempts;
            _profilesIncidence = new Dictionary<string, int>();
        }

        public void AddProfile(string profile)
        {
            if (_profilesIncidence.ContainsKey(profile))
                _profilesIncidence[profile]++;
            else
                _profilesIncidence.Add(profile, 1);
        }

        public string GetFinalProfile()
        {
            foreach (var (profile, incidence) in _profilesIncidence)
            {
                if (incidence >= _attempts)
                    return profile;
            }

            return null;
        }

        public string GetMostProbableProfile()
        {
            var maxIncidence = -1;
            string probableProfile = null;
            foreach (var (profile, incidence) in _profilesIncidence)
            {
                if (incidence >= maxIncidence)
                {
                    probableProfile = profile;
                    maxIncidence = incidence;
                }
            }

            return probableProfile;
        }
    }
}

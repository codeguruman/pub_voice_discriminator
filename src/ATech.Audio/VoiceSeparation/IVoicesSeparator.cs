﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ATech.Audio.VoiceSeparation
{
    public interface IVoicesSeparator
    {
        Task<Operator> GetTargetVoiceProfile(string fragmentPath);
        Task<string> CreateTargetProfile(string targetVoiceFile);
        Task<(bool, bool)> IsTargetVoiceProfile(string fragmentPath, string profileId);
        void BackupProfiles(string filePath);
        void LoadProfiles(string filePath);
        List<Operator> GetProfiles();
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ATech.Audio.VoiceSeparation
{
    public class CsVoicesSeparator : IVoicesSeparator
    {
        #region Parameters

        private readonly string _csUrl = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.VoicesIdentificationUrl;

        private readonly string _apiKey;

        private readonly int _attemptsGetOperationResultCount = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.VoicesIdentAttemptsGetOperationResultCount;

        private readonly int _pauseWaitingOperationResultLength = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.VoicesIdentPauseWaitingOperationResultLength;

        private const int BatchSizeProfiles = 10; // maximum allowed size for cognitive services

        private int _stepsOnProfilesUsingBatchSize; 
        
        #endregion

        #region Data

        private Dictionary<string, Operator> _targetRoleProfiles;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        #endregion

        #region c-tor

        public CsVoicesSeparator(string apiKey)
        {
            _apiKey = apiKey;
            _targetRoleProfiles = new Dictionary<string, Operator>();
        }

        #endregion

        #region Public Methods

        public async Task<Operator> GetTargetVoiceProfile(string fragmentPath)
        {
            try
            {
                if (_targetRoleProfiles.Count == 0)
                    return null;

                for (var i = 0; i < _stepsOnProfilesUsingBatchSize; i++)
                {
                    var batch = _targetRoleProfiles.Keys.Skip(i * BatchSizeProfiles).Take(BatchSizeProfiles).ToList();
                    var identifyProfileResult = await IdentifySpeaker(fragmentPath, batch).ConfigureAwait(false);
                    if (identifyProfileResult == null)
                        continue;

                    if (_targetRoleProfiles.ContainsKey(identifyProfileResult.ProfileId))
                        return _targetRoleProfiles[identifyProfileResult.ProfileId];
                }

                return null;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CsVoicesSeparator GetTargetVoiceProfile");
                return null;
            } 
        }

        public async Task<(bool, bool)> IsTargetVoiceProfile(string fragmentPath, string profileId)
        {
            try
            {
                var identifyProfileResult = await IdentifySpeaker(fragmentPath, new []{ profileId }).ConfigureAwait(false);
                if (identifyProfileResult == null)
                    return (false, false);

                var isTargetVoiceProfile = identifyProfileResult.ProfileId == profileId;
                var oneTimeSpeech = isTargetVoiceProfile && identifyProfileResult.Confidence != "High";
                return (isTargetVoiceProfile, oneTimeSpeech);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CsVoicesSeparator IsTargetVoiceProfile");
            }

            return (false, false);
        }

        public async Task<string> CreateTargetProfile(string targetVoiceFile)
        {
            try
            {
                var profileLocaleJson = "{\"locale\":\"en-US\"}";
                var profileJson = "";

                for (var i = 0; i < _attemptsGetOperationResultCount; i++)
                {
                    profileJson = await CreateProfile(profileLocaleJson).ConfigureAwait(false);
                    if (!string.IsNullOrEmpty(profileJson))
                        break;
                }

                if (profileJson == null)
                    return null;

                var profile = JObject.Parse(profileJson);
                var bytesProfile = await File.ReadAllBytesAsync(targetVoiceFile).ConfigureAwait(false);

                var profileId = profile["identificationProfileId"].ToString();
                var createEnrollmentOperationLocationUrl = "";
                for (var i = 0; i < _attemptsGetOperationResultCount; i++)
                {
                    createEnrollmentOperationLocationUrl = await CreateEnrollment(bytesProfile, profileId).ConfigureAwait(false);
                    if (!string.IsNullOrEmpty(createEnrollmentOperationLocationUrl))
                        break;
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                }

                for (var i = 0; i < _attemptsGetOperationResultCount; i++)
                {
                    var operationCreateEnrollmentResultJson = await GetOperationResult(createEnrollmentOperationLocationUrl).ConfigureAwait(false);

                    if (string.IsNullOrEmpty(operationCreateEnrollmentResultJson))
                        continue;
                    
                    var operationCreateEnrollmentResult = JObject.Parse(operationCreateEnrollmentResultJson);
                    var status = operationCreateEnrollmentResult["status"].ToString();
                    if (status == "failed")
                        return null;

                    if (status == "succeeded")
                    {
                        _targetRoleProfiles.Add(profileId, new Operator { ProfileId = profileId});
                        return profileId;
                    }
                }

                return null;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CsVoicesSeparator CreateTargetProfile");
                return null;
            }
        }

        public void BackupProfiles(string filePath)
        {
            if (_targetRoleProfiles == null)
                return;
            if (_targetRoleProfiles.Count == 0)
                return;

            var obj = _targetRoleProfiles.ToArray();
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            File.WriteAllText(filePath, json);
        }

        public void LoadProfiles(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            _targetRoleProfiles = new Dictionary<string, Operator>();
            var json = File.ReadAllText(filePath);
            var arrayProfiles = (JArray)JsonConvert.DeserializeObject(json);
            foreach (var arrayProfile in arrayProfiles)
            {
                Operator operatorItem;
                try
                {
                    operatorItem = arrayProfile["Value"].ToObject<Operator>();
                }
                catch
                {
                    operatorItem = arrayProfile.ToObject<Operator>();
                }
                _targetRoleProfiles.Add(operatorItem.ProfileId, operatorItem);
            }

            var additionalPart = _targetRoleProfiles.Count % BatchSizeProfiles == 0 ? 0 : 1;
            _stepsOnProfilesUsingBatchSize = _targetRoleProfiles.Count / BatchSizeProfiles + additionalPart;
        }

        #endregion

        #region Private Methods

        private async Task<IdentifyProfileResult> IdentifySpeaker(string testVoiceFile, IEnumerable<string> profilesId)
        {
            var bytes = await File.ReadAllBytesAsync(testVoiceFile).ConfigureAwait(false);

            var identifySpeakerOperationLocationUrl = "";
            for (var i = 0; i < _attemptsGetOperationResultCount; i++)
            {
                identifySpeakerOperationLocationUrl = await PostIdentify(bytes, profilesId).ConfigureAwait(false);
                if (!string.IsNullOrEmpty(identifySpeakerOperationLocationUrl))
                    break;
                await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
            }

            for (var i = 0; i < _attemptsGetOperationResultCount; i++)
            {
                var operationIdentifySpeakerResultJson = await GetOperationResult(identifySpeakerOperationLocationUrl).ConfigureAwait(false);

                if (string.IsNullOrEmpty(operationIdentifySpeakerResultJson))
                {
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                    continue;
                }

                var operationIdentifySpeakerResult = JObject.Parse(operationIdentifySpeakerResultJson);
                var status = operationIdentifySpeakerResult["status"].ToString();
                if (status == "failed")
                    return null;

                if (status != "succeeded")
                {
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                    continue;
                }

                var identifyProfileResult = new IdentifyProfileResult
                {
                    Confidence = operationIdentifySpeakerResult["processingResult"]["confidence"].ToString(),
                    ProfileId = operationIdentifySpeakerResult["processingResult"]["identifiedProfileId"].ToString(),
                };
                return identifyProfileResult;
            }

            return null;
        }

        private async Task<string> CreateProfile(string localeJsonData)
        {
            return await PostCreateProfile(localeJsonData).ConfigureAwait(false);
        }

        private async Task<string> CreateEnrollment(byte[] audioBytes, string identificationId)
        {
            return await PostCreateEnrollment(audioBytes, identificationId).ConfigureAwait(false);
        }

        private async Task<string> PostCreateProfile(string jsonData)
        {
            var bytes = Encoding.UTF8.GetBytes(jsonData);
            var request = (HttpWebRequest)WebRequest.Create(_csUrl + "identificationProfiles/");

            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "application/json";
            request.Headers.Add("Ocp-Apim-Subscription-Key", _apiKey);

            try
            {
                var stream = await request.GetRequestStreamAsync().ConfigureAwait(false);
                await stream.WriteAsync(bytes, 0, bytes.Length).ConfigureAwait(false);
                var response = await request.GetResponseAsync().ConfigureAwait(false);

                string result;
                using (var rdr = new StreamReader(response.GetResponseStream()))
                    result = await rdr.ReadToEndAsync().ConfigureAwait(false);
                return result;

            }
            catch (Exception exception)
            {
                if ((int)((HttpWebResponse)((WebException)exception).Response).StatusCode ==
                    429)
                    _logger.LogError("Request limit", "CsVoicesSeparator PostCreateProfile");
                
                if (int.TryParse(((HttpWebResponse)((WebException)exception).Response).Headers["Retry-After"],
                    out var delaySecs))
                {
                    _logger.LogError($"Stop requests recommendation {delaySecs}", "CsVoicesSeparator GetOperationResult");
                    if (delaySecs <= 5)
                        await Task.Delay(delaySecs * 1000).ConfigureAwait(false);
                    else
                        await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                }

                else
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                return null;
            }
        }

        private async Task<string> PostCreateEnrollment(byte[] audioBytes, string identificationId)
        {
            var request = (HttpWebRequest)WebRequest.Create(_csUrl + $"identificationProfiles/{identificationId}/enroll?shortAudio=true");

            request.Method = "POST";
            request.ContentType = "application/octet-stream"; // WAV PCM 16bit 16000GZ MONO
            request.Headers.Add("Ocp-Apim-Subscription-Key", _apiKey);

            try
            {
                var stream = await request.GetRequestStreamAsync().ConfigureAwait(false);
                await stream.WriteAsync(audioBytes, 0, audioBytes.Length).ConfigureAwait(false);
                var response = await request.GetResponseAsync().ConfigureAwait(false);

                using (var rdr = new StreamReader(response.GetResponseStream()))
                    await rdr.ReadToEndAsync().ConfigureAwait(false);

                var url = response.Headers["Operation-Location"];
                return url;
            }
            catch (Exception exception)
            {
                if ((int)((HttpWebResponse)((WebException)exception).Response).StatusCode ==
                    429)
                    _logger.LogError("Request limit", "CsVoicesSeparator PostCreateEnrollment");
                
                if (int.TryParse(((HttpWebResponse)((WebException)exception).Response).Headers["Retry-After"],
                    out var delaySecs))
                {
                    _logger.LogError($"Stop requests recommendation {delaySecs}", "CsVoicesSeparator GetOperationResult");
                    if (delaySecs <= 5)
                        await Task.Delay(delaySecs * 1000).ConfigureAwait(false);
                    else
                        await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                }

                else
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                return null;
            }
        }

        private async Task<string> GetOperationResult(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(new Uri(url));

                request.Method = "GET";
                request.Headers.Add("Ocp-Apim-Subscription-Key", _apiKey);

            
                var response = await request.GetResponseAsync().ConfigureAwait(false);

                string result;
                using (var rdr = new StreamReader(response.GetResponseStream()))
                     result = await rdr.ReadToEndAsync().ConfigureAwait(false);
                return result;
            }
            catch (Exception exception)
            {
                if ((int) ((HttpWebResponse) ((WebException) exception).Response).StatusCode ==
                    429)
                    _logger.LogError("Request limit", "CsVoicesSeparator GetOperationResult");
                
                if (int.TryParse(((HttpWebResponse) ((WebException) exception).Response).Headers["Retry-After"],
                    out var delaySecs))
                {
                    _logger.LogError($"Stop requests recommendation {delaySecs}", "CsVoicesSeparator GetOperationResult");
                    if (delaySecs <= 5)
                        await Task.Delay(delaySecs * 1000).ConfigureAwait(false);
                    else
                        await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                }
                   
                else
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                return null;
            }
        }

        private async Task<string> PostIdentify(byte[] audioBytes, IEnumerable<string> profilesId)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(_csUrl + $"identify?identificationProfileIds={string.Join(",",profilesId)}&shortAudio=true");

                request.Method = "POST";
                request.ContentType = "application/octet-stream"; // WAV PCM 16bit 16000GZ MONO
                request.Headers.Add("Ocp-Apim-Subscription-Key", _apiKey);

            
                var stream = await request.GetRequestStreamAsync().ConfigureAwait(false);
                await stream.WriteAsync(audioBytes, 0, audioBytes.Length).ConfigureAwait(false);
                var response = await request.GetResponseAsync().ConfigureAwait(false);

                using (var rdr = new StreamReader(response.GetResponseStream()))
                    await rdr.ReadToEndAsync().ConfigureAwait(false);

                var url = response.Headers["Operation-Location"];
                return url;
            }
            catch (Exception exception)
            {
                try
                {
                    if ((int)((HttpWebResponse)((WebException)exception).Response).StatusCode ==
                        429)
                        _logger.LogError("Request limit", "CsVoicesSeparator PostIdentify");

                    if (int.TryParse(((HttpWebResponse)((WebException)exception).Response).Headers["Retry-After"],
                        out var delaySecs))
                    {
                        _logger.LogError($"Stop requests recommendation {delaySecs}", "CsVoicesSeparator GetOperationResult");
                        if (delaySecs <= 5)
                            await Task.Delay(delaySecs * 1000).ConfigureAwait(false);
                        else
                            await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                    }

                    else
                        await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                    return null;
                }
                catch (Exception)
                {
                    await Task.Delay(_pauseWaitingOperationResultLength).ConfigureAwait(false);
                    return null;
                }
            }
        }

        public List<Operator> GetProfiles()
        {
            return _targetRoleProfiles.Values.ToList();
        }

        #endregion
    }
}

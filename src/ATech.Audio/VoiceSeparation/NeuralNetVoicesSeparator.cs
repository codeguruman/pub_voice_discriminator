﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ATech.Audio.VoiceSeparation
{
    public class NeuralNetVoicesSeparator : IVoicesSeparator
    {
        public Task<Operator> GetTargetVoiceProfile(string fragmentPath)
        {
            throw new System.NotImplementedException();
        }

        public Task<string> CreateTargetProfile(string targetVoiceFile)
        {
            throw new System.NotImplementedException();
        }

        public Task<(bool, bool)> IsTargetVoiceProfile(string fragmentPath, string profileId)
        {
            throw new System.NotImplementedException();
        }

        public void BackupProfiles(string filePath)
        {

        }

        public void LoadProfiles(string filePath)
        {

        }

        public List<Operator> GetProfiles()
        {
            throw new System.NotImplementedException();
        }
    }
}

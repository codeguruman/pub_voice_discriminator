﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using ATech.Configuration;
using ATech.Logging.Interfaces;

namespace ATech.Audio.AudioProcessing.Ffmpeg
{
    public static class FfmpegHelper
    {
#if DEBUG
        private static readonly string _ffmpegPath = Path.Combine("ffmpeg", "ffmpeg.exe");
#else
        private static readonly string _ffmpegPath = "ffmpeg";
#endif


        private static readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;
        public static void RunCommand(string workDir, string command)
        {
            try
            {
                using var ffmpegProcess = new Process();
                var ffmpegStartInfo = new ProcessStartInfo(_ffmpegPath, command)
                {
                    WorkingDirectory = workDir,
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                };

                ffmpegProcess.StartInfo = ffmpegStartInfo;
                ffmpegProcess.Start();
                ffmpegProcess.WaitForExit(5 * 60 * 1000);
                if (!ffmpegProcess.HasExited)
                    ffmpegProcess.Kill();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "FfmpegHelper RunCommand");
            }
        }

        public static async Task<string> GetCommandOutput(string workDir, string command)
        {
            try
            {
                using var ffmpegProcess = new Process();
                var ffmpegStartInfo = new ProcessStartInfo(_ffmpegPath, command)
                {
                    WorkingDirectory = workDir,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true
                };

                ffmpegProcess.StartInfo = ffmpegStartInfo;
                ffmpegProcess.Start();
                var output = await ffmpegProcess.StandardOutput.ReadToEndAsync();
                if (string.IsNullOrWhiteSpace(output))
                    output = await ffmpegProcess.StandardError.ReadToEndAsync();

                ffmpegProcess.WaitForExit(5 * 60 * 1000);
                if (!ffmpegProcess.HasExited)
                    ffmpegProcess.Kill();
                return output;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "FfmpegHelper GetCommandOutput");
                return null;
            }
        }
    }
}

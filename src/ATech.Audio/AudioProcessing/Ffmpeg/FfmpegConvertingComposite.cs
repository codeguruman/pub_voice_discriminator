﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ATech.Audio.AudioProcessing.Ffmpeg
{
    public class FfmpegConvertingComposite : IAudioConvertingComposite
    {
        private readonly IAudioFileHandler audioFileHandler;

        private readonly string wav16Suffix;

        private readonly string wav8Suffix;

        public FfmpegConvertingComposite(IAudioFileHandler audioFileHandler)
        {
            this.audioFileHandler = audioFileHandler;
            wav16Suffix = "_wav16.wav";
            wav8Suffix = "_wav8.wav";
        }

        public Task<string> TransformFromCroppedMp3ToWavMono16OneChannel(string tmpDir, int idxChannel)
        {
            throw new NotImplementedException();
        }

        public string TransformFromCroppedMp3ToWavMono16BothChannelsInOne(string tmpDir)
        {
            throw new NotImplementedException();
        }

        public Task<string> TransformFromMp3ToWavMono16OneChannel(string srcFilePath, string tmpDir, int idxChannel)
        {
            throw new NotImplementedException();
        }

        public (string, string) TransformFromMp3ToTwoWavMonoChannels(string srcFilePath)
        {
            var wave16FileLCH = Guid.NewGuid() + wav16Suffix;
            var wave16FileRCH = Guid.NewGuid() + wav16Suffix;

            var workdir = Path.GetDirectoryName(srcFilePath);
            var srcName = Path.GetFileName(srcFilePath);

            var command = $"-i {srcName} -acodec pcm_s16le -ar 16000 -map_channel 0.0.0 {wave16FileLCH} -ar 16000 -map_channel 0.0.1 {wave16FileRCH}";

            FfmpegHelper.RunCommand(workdir, command);

            return (Path.Combine(workdir, wave16FileLCH), Path.Combine(workdir, wave16FileRCH));
        }

        public (string, string) TransformFromMp3ToTwoWav8MonoChannels(string srcFilePath)
        {
            var wave8FileLCH = Guid.NewGuid() + wav8Suffix;
            var wave8FileRCH = Guid.NewGuid() + wav8Suffix;

            var workdir = Path.GetDirectoryName(srcFilePath);
            var srcName = Path.GetFileName(srcFilePath);

            var command = $"-i {srcName} -acodec pcm_s16le -ar 8000 -map_channel 0.0.0 {wave8FileLCH} -ar 8000 -map_channel 0.0.1 {wave8FileRCH}";

            FfmpegHelper.RunCommand(workdir, command);

            return (Path.Combine(workdir, wave8FileLCH), Path.Combine(workdir, wave8FileRCH));
        }

        public string TransformFromMp3ToWav(string srcFilePath, string tmpDir, int channelsCount = 1)
        {
            var workdir = Path.GetDirectoryName(srcFilePath);
            var srcName = Path.GetFileName(srcFilePath);
            var wave16File = Guid.NewGuid() + ".wav";
            var command = $"-i {srcName} -acodec pcm_s16le -ar 16000 -ac {channelsCount} {wave16File}";

            FfmpegHelper.RunCommand(workdir, command);
            return Path.Combine(workdir, wave16File);
        }

        public string TransformFromMp3ToWav8KHz(string srcFilePath, string tmpDir, int channelsCount = 1)
        {
            var workdir = Path.GetDirectoryName(srcFilePath);
            var srcName = Path.GetFileName(srcFilePath);
            var wave8File = Guid.NewGuid() + ".wav";
            var command = $"-i {srcName} -acodec pcm_s16le -ar 8000 -ac 1 {wave8File}";

            FfmpegHelper.RunCommand(workdir, command);
            return Path.Combine(workdir, wave8File);
        }

        public string TransformFromMp3ToWavMono16BothChannelsInOne(string srcFilePath, string tmpDir)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string tmpTransformDir, int takeFirstSecondsCount,
            int lengthThreshold)
        {
            throw new NotImplementedException();
        }

        public bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath,
            double maxAllowedOneTimeSpeechLengthInSeconds)
        {
            throw new NotImplementedException();
        }
    }
}

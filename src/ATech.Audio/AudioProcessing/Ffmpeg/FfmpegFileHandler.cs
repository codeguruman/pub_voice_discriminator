﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ATech.Audio.AudioProcessing.Ffmpeg
{
    public class FfmpegFileHandler : IAudioFileHandler
    {
        public void ConvertMp4ToMp3(string fromFilePath, string toFilePath)
        {
            throw new NotImplementedException();
        }

        public void ConvertMp3ToWav(string fromFilePath, string toFilePath)
        {
            throw new NotImplementedException();
        }

        public void ConvertWavToMono16Hz16000Format(string fromFilePath, string toFilePath, int channelsCount = 1)
        {
            throw new NotImplementedException();
        }

        public Task ConvertWavTwoChannelsToMonoOneChannel(string fromFilePath, string toFilePath, int idxChannelToSave)
        {
            throw new NotImplementedException();
        }

        public Task<int> CropPartFromMp3File(string fromFilePath, string toFilePath, TimeSpan? begin, TimeSpan? end)
        {
            throw new NotImplementedException();
        }

        public void CropPartFromFile(string fromFilePath, string toFilePath, int startSecond, int secondsCount)
        {
            var workdir = Path.GetDirectoryName(fromFilePath);
            var srcName = Path.GetFileName(fromFilePath);
            var dstName = Path.GetFileName(toFilePath);
            var command = $"-i {srcName} -ss {startSecond} -t {secondsCount} -c copy -y {dstName}";

            FfmpegHelper.RunCommand(workdir, command);
        }

        public async Task<double> GetAudioDurationInSeconds(string path)
        {
            var workdir = Path.GetDirectoryName(path);
            var srcName = Path.GetFileName(path);
            var command = $"-i {srcName}";

            var output = await FfmpegHelper.GetCommandOutput(workdir, command);

            var durationStringSplit =
                output.Split(new[] { "Duration: ", ", start:", ", bitrate: " }, StringSplitOptions.RemoveEmptyEntries);

            if (durationStringSplit.Length <= 1) 
                return 0;
            
            var durationString = durationStringSplit[1];
            
            if (TimeSpan.TryParse(durationString, out var time))
                return time.TotalSeconds;
            
            return 0;

        }

        public Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string croppedFile, int takeFirstSecondsCount,
            int lengthThreshold)
        {
            throw new NotImplementedException();
        }

        public bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath,
            double maxAllowedOneTimeSpeechLengthInSeconds)
        {
            throw new NotImplementedException();
        }
    }
}

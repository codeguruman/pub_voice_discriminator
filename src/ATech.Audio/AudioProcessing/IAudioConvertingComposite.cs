﻿using System.Threading.Tasks;

namespace ATech.Audio.AudioProcessing
{
    public interface IAudioConvertingComposite
    {
        Task<string> TransformFromCroppedMp3ToWavMono16OneChannel(string tmpDir, int idxChannel);

        string TransformFromCroppedMp3ToWavMono16BothChannelsInOne(string tmpDir);

        Task<string> TransformFromMp3ToWavMono16OneChannel(string srcFilePath, string tmpDir, int idxChannel);
        
        (string, string) TransformFromMp3ToTwoWavMonoChannels(string srcFilePath);

        (string, string) TransformFromMp3ToTwoWav8MonoChannels(string srcFilePath);

        string TransformFromMp3ToWav(string srcFilePath, string tmpDir, int channelsCount = 1);
        string TransformFromMp3ToWav8KHz(string srcFilePath, string tmpDir, int channelsCount = 1);

        string TransformFromMp3ToWavMono16BothChannelsInOne(string srcFilePath, string tmpDir);

        Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string tmpTransformDir, int takeFirstSecondsCount,
            int lengthThreshold);

        bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath,
            double maxAllowedOneTimeSpeechLengthInSeconds);
    }
}

﻿using System;
using System.Threading.Tasks;

namespace ATech.Audio.AudioProcessing
{
    public interface IAudioFileHandler
    {
        void ConvertMp4ToMp3(string fromFilePath, string toFilePath);
        void ConvertMp3ToWav(string fromFilePath, string toFilePath);

        void ConvertWavToMono16Hz16000Format(string fromFilePath, string toFilePath, int channelsCount = 1);

        Task ConvertWavTwoChannelsToMonoOneChannel(string fromFilePath, string toFilePath, int idxChannelToSave);

        Task<int> CropPartFromMp3File(string fromFilePath, string toFilePath, TimeSpan? begin, TimeSpan? end);

        void CropPartFromFile(string fromFilePath, string toFilePath, int startSecond, int secondsCount);

        Task<double> GetAudioDurationInSeconds(string path);

        Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string croppedFile, int takeFirstSecondsCount,
            int lengthThreshold);

        bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath,
            double maxAllowedOneTimeSpeechLengthInSeconds);
    }
}

﻿using System;
using System.IO;
using System.Threading.Tasks;
using NAudio.Wave;

namespace ATech.Audio.AudioProcessing.NAudio
{
    public class NaudioFileHandler : IAudioFileHandler
    {
        public void ConvertMp4ToMp3(string fromFilePath, string toFilePath)
        {
            if (!toFilePath.EndsWith(".mp3"))
            {
                throw new ArgumentException("toFilePath must end with .mp3");
            }

            using var reader = new MediaFoundationReader(fromFilePath);
            MediaFoundationEncoder.EncodeToMp3(reader, toFilePath);
        }

        public void ConvertMp3ToWav(string fromFilePath, string toFilePath)
        {
            using var reader = new Mp3FileReader(fromFilePath);
            using var pcmStream = WaveFormatConversionStream.CreatePcmStream(reader);
            WaveFileWriter.CreateWaveFile(toFilePath, pcmStream);
        }

        public void ConvertWavToMono16Hz16000Format(string fromFilePath, string toFilePath, int channelsCount = 1)
        {
            var monoFormat = new WaveFormat(16000, 16, channelsCount);
            using var waveFileReader = new WaveFileReader(fromFilePath);
            using var reSampler = new MediaFoundationResampler(waveFileReader, monoFormat);
            WaveFileWriter.CreateWaveFile(toFilePath, reSampler);
        }

        public async Task ConvertWavTwoChannelsToMonoOneChannel(string fromFilePath, string toFilePath, int idxChannelToSave)
        {
            await using var reader = new WaveFileReader(fromFilePath);
            var buffer = new byte[2 * reader.WaveFormat.SampleRate * reader.WaveFormat.Channels];
            var format = new WaveFormat(reader.WaveFormat.SampleRate, 16, 1);
            await using var writer = new WaveFileWriter(toFilePath, format);
            int bytesRead;
            while ((bytesRead = await reader.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false)) > 0)
            {
                var offset = 2 * idxChannelToSave;
                while (offset < bytesRead)
                {
                    await writer.WriteAsync(buffer, offset, 2).ConfigureAwait(false);
                    offset += 4;
                }
            }
        }

        public async Task<int> CropPartFromMp3File(string fromFilePath, string toFilePath, TimeSpan? begin, TimeSpan? end)
        {
            await using var reader = new Mp3FileReader(fromFilePath);
            await using var writer = File.Create(toFilePath);
            Mp3Frame frame;
            var time = (int)reader.TotalTime.TotalMilliseconds;
            while ((frame = reader.ReadNextFrame()) != null)
                if (reader.CurrentTime >= begin || !begin.HasValue)
                {
                    if (reader.CurrentTime <= end || !end.HasValue)
                        await writer.WriteAsync(frame.RawData, 0, frame.RawData.Length).ConfigureAwait(false);
                    else break;
                }

            return time;
        }

        public void CropPartFromFile(string fromFilePath, string toFilePath, int startSecond, int secondsCount)
        {
            throw new NotImplementedException();
        }

        public async Task<double> GetAudioDurationInSeconds(string path)
        {
            await using var reader = new Mp3FileReader(path);
            return reader.TotalTime.TotalSeconds;
        }

        public async Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string croppedFile, int takeFirstSecondsCount, int lengthThreshold)
        {
            var lengthMilliseconds = await CropPartFromMp3File(fragmentPath, croppedFile, new TimeSpan(0, 0, 0), new TimeSpan(0, 0, takeFirstSecondsCount));
            return lengthMilliseconds >= lengthThreshold;
        }

        public bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath, double maxAllowedOneTimeSpeechLengthInSeconds)
        {
            var lengthSecs = GetAudioDurationInSeconds(fragmentPath).Result;
            return lengthSecs > maxAllowedOneTimeSpeechLengthInSeconds;
        }
    }
}

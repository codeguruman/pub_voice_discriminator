﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace ATech.Audio.AudioProcessing.NAudio
{
    public class NaudioConvertingComposite : IAudioConvertingComposite
    {
        private readonly IAudioFileHandler _audioFileHandler;

        private readonly string _monoSuffix;

        private readonly string _wavSuffix;

        private readonly string _wav16Suffix;

        private readonly string _croppedSuffix;

        public NaudioConvertingComposite(IAudioFileHandler audioFileHandler)
        {
            _monoSuffix = "_mono.wav";
            _wavSuffix = "_wav.wav";
            _wav16Suffix = "_wav16.wav";
            _croppedSuffix = "_cropped.mp3";
            _audioFileHandler = audioFileHandler;
        }

        public async Task<string> TransformFromCroppedMp3ToWavMono16OneChannel(string tmpDir, int idxChannel) // UNUSED NOW
        {
            var croppedFile = Path.Combine(tmpDir, _croppedSuffix);
            var waveFile = Path.Combine(tmpDir, _wavSuffix);
            var monoWavFile = Path.Combine(tmpDir, _monoSuffix);
            var wave16File = Path.Combine(tmpDir, _wav16Suffix);
            
            _audioFileHandler.ConvertMp3ToWav(croppedFile, waveFile);
            await _audioFileHandler.ConvertWavTwoChannelsToMonoOneChannel(waveFile, monoWavFile, idxChannel);
            _audioFileHandler.ConvertWavToMono16Hz16000Format(monoWavFile, wave16File);
            return wave16File;
        }

        public string TransformFromCroppedMp3ToWavMono16BothChannelsInOne(string tmpDir)  // UNUSED NOW
        {
            var croppedFile = Path.Combine(tmpDir, _croppedSuffix);
            var waveFile = Path.Combine(tmpDir, _wavSuffix);
            var wave16File = Path.Combine(tmpDir, _wav16Suffix);

            _audioFileHandler.ConvertMp3ToWav(croppedFile, waveFile);
            _audioFileHandler.ConvertWavToMono16Hz16000Format(waveFile, wave16File);
            return wave16File;
        }

        public async Task<string> TransformFromMp3ToWavMono16OneChannel(string srcFilePath, string tmpDir, int idxChannel)
        {
            var waveFile = Path.Combine(tmpDir, _wavSuffix);
            var monoWavFile = Path.Combine(tmpDir, _monoSuffix);
            var wave16File = Path.Combine(tmpDir, _wav16Suffix);

            _audioFileHandler.ConvertMp3ToWav(srcFilePath, waveFile);
            await _audioFileHandler.ConvertWavTwoChannelsToMonoOneChannel(waveFile, monoWavFile, idxChannel);
            _audioFileHandler.ConvertWavToMono16Hz16000Format(monoWavFile, wave16File);
            return wave16File;
        }

        public (string, string) TransformFromMp3ToTwoWavMonoChannels(string srcFilePath)
        {
            throw new NotImplementedException();
        }

        public (string, string) TransformFromMp3ToTwoWav8MonoChannels(string srcFilePath)
        {
            throw new NotImplementedException();
        }

        public string TransformFromMp3ToWav(string srcFilePath, string tmpDir, int channelsCount = 1)
        {
            var waveFile = Path.Combine(tmpDir, _wavSuffix);
            var wave16File = Path.Combine(tmpDir, Guid.NewGuid() + _wav16Suffix);
            _audioFileHandler.ConvertMp3ToWav(srcFilePath, waveFile);
            _audioFileHandler.ConvertWavToMono16Hz16000Format(waveFile, wave16File, channelsCount);
            return wave16File;
        }

        public string TransformFromMp3ToWav8KHz(string srcFilePath, string tmpDir, int channelsCount = 1)
        {
            throw new NotImplementedException();
        }


        public string TransformFromMp3ToWavMono16BothChannelsInOne(string srcFilePath, string tmpDir) // UNUSED NOW
        {
            var waveFile = Path.Combine(tmpDir, _wavSuffix);
            var wave16File = Path.Combine(tmpDir, _wav16Suffix);

            _audioFileHandler.ConvertMp3ToWav(srcFilePath, waveFile);
            _audioFileHandler.ConvertWavToMono16Hz16000Format(waveFile, wave16File);
            return wave16File;
        }

        public async Task<bool> CheckCorrectFragmentMp3AndCrop(string fragmentPath, string tmpTransformDir, int takeFirstSecondsCount, int lengthThreshold)  // UNUSED NOW
        {
            var croppedFile = Path.Combine(tmpTransformDir, _croppedSuffix);
            return await _audioFileHandler.CheckCorrectFragmentMp3AndCrop(fragmentPath, croppedFile, takeFirstSecondsCount, lengthThreshold);
        }

        public bool IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(string fragmentPath, double maxAllowedOneTimeSpeechLengthInSeconds)  // UNUSED NOW
        {
            return _audioFileHandler.IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(fragmentPath,
                maxAllowedOneTimeSpeechLengthInSeconds);
        }
    }
}

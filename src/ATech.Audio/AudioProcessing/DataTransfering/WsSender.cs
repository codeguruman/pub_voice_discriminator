﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ATech.Configuration;
using Nest;
using Newtonsoft.Json.Linq;

namespace ATech.Audio.AudioProcessing.DataTransfering
{
    public class WsSender
    {
        private static readonly Uri _uri = new Uri(MainProcessingConfiguration.Instance.KaldiServiceWsUrl);

        private static readonly Uri _uriEn = new Uri(MainProcessingConfiguration.Instance.KaldiServiceWsUrlEn);

        private static readonly CancellationToken _timeOut = new CancellationTokenSource(10 * 6 * 10 * 1000).Token; // can use instead CancellationToken.None

        public static async Task<(List<JObject>, string)> Send(string filePath, string language)
        {
            var eof = Encoding.UTF8.GetBytes("{\"eof\" : 1}");
            var ending = new ArraySegment<byte>(eof);

            using var client = new ClientWebSocket();

            if (language == "en")
                await client.ConnectAsync(_uriEn, CancellationToken.None).ConfigureAwait(false);
            else
                await client.ConnectAsync(_uri, CancellationToken.None).ConfigureAwait(false);

            var temporalResults = new List<JObject>();
            var partialResult = "";
            var maxPartialLength = 0;

            await using Stream source = File.OpenRead(filePath);
            var buffer = new byte[8000];
            while (true)
            {
                var count = await source.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
                if (count == 0)
                    break;

                var msg = await ProcessData(client, buffer, count).ConfigureAwait(false);
                if (string.IsNullOrWhiteSpace(msg))
                {
                    continue;
                }

                if (!msg.Contains("result"))
                {
                    if (msg.Contains("partial"))
                    {
                        if (msg.Length > maxPartialLength)
                            maxPartialLength = msg.Length;

                        try
                        {
                            partialResult = Convert.ToString(JObject.Parse(msg)["partial"]);
                        }
                        catch
                        {
                            // ignored
                        }
                    }
                }
                else
                {
                    try
                    {
                        maxPartialLength = 0;
                        partialResult = "";
                        temporalResults.Add(JObject.Parse(msg));
                    }
                    catch
                    {
                        // ignored
                    }
                }
            }

            await ProcessFinalData(client, ending).ConfigureAwait(false);

            await client.CloseAsync(WebSocketCloseStatus.NormalClosure, "OK", CancellationToken.None).ConfigureAwait(false);

            return (temporalResults, partialResult);
        }

        private static async Task<string> RecieveResult(ClientWebSocket ws)
        {
            var result = new byte[4096 * 8];
            var receiveTask = ws.ReceiveAsync(new ArraySegment<byte>(result), CancellationToken.None);
            await receiveTask.ConfigureAwait(false);
            var receivedString = Encoding.UTF8.GetString(result, 0, receiveTask.Result.Count);

            return receivedString;
        }

        private static async Task<string> ProcessData(ClientWebSocket ws, byte[] data, int count)
        {
            await ws.SendAsync(new ArraySegment<byte>(data, 0, count), WebSocketMessageType.Binary, true, CancellationToken.None);
            return await RecieveResult(ws);
        }

        private static async Task ProcessFinalData(ClientWebSocket ws, ArraySegment<byte> ending)
        {
            await ws.SendAsync(ending, WebSocketMessageType.Text, true, CancellationToken.None);
            await RecieveResult(ws).ConfigureAwait(false);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ATech.Audio.AudioProcessing;
using ATech.Audio.AudioSegmentation;
using ATech.Audio.Interfaces;
using ATech.Audio.SpeechRecognition;
using ATech.Audio.VoiceSeparation;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.Common.Dal.Models;
using ATech.Configuration;
using ATech.Configuration.Types;
using ATech.Logging.Interfaces;
using ATech.TextAnalysis.Models;

namespace ATech.Audio
{
    [System.Runtime.InteropServices.Guid("386F900F-4C40-4E73-B8EB-FE985A5B9A04")]
    public class AudioDescriptor : IAudioDescriptor
    {
        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        private IAudioConvertingComposite _audioConvertingComposite;

        public string MainAudioFilePath { get; set; }
        public string SecondMainAudioFilePath { get; set; }
        public DateTime CreationDate { get; set; }
        public AudioChannelsType AudioChannelsType { get; set; }
        public string Name { get; set; }
        public string OperatorId { get; set; }
        public string ClientId { get; set; }

        public string TmpTransformForVoicesIdentifyFilesDir { get; set; }
        public string TmpTransformForRecognizingFilesDir { get; set; }

        public List<IAudioFragmentDescriptor> Fragments { get; set; }
        public IAudioDialogDetails AudioDialogDetails { get; set; }

        private AudioDialogPhrase[] _temporalPhrases;

        private List<string> _operatorNamesVocabulary;

        public int OperatorChannelNumber { get; set; }

        public void Create(IAudioConvertingComposite audioConvertingComposite, string filePath, string secondFilePath)
        {
            _audioConvertingComposite = audioConvertingComposite;
            AudioChannelsType = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.AudioChannelsType;
            CreationDate = File.GetLastWriteTime(filePath);

            MainAudioFilePath = filePath;
            SecondMainAudioFilePath = secondFilePath;
            AudioDialogDetails = new AudioDialogDetails { MainAudioFilePath = filePath };

            TmpTransformForVoicesIdentifyFilesDir = Path.Combine(StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.TmpTransformForVoicesIdentifyFilesDir, Guid.NewGuid().ToString());
            Directory.CreateDirectory(TmpTransformForVoicesIdentifyFilesDir);

            TmpTransformForRecognizingFilesDir = Path.Combine(StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.TmpTransformForRecognizingFilesDir, Guid.NewGuid().ToString());
            Directory.CreateDirectory(TmpTransformForRecognizingFilesDir);
        }

        public IAudioDescriptor Segment(IAudioSegmentor segmentor)
        {
            _logger.LogMessage("Audio file segmentation");
            return segmentor.CropBySilence(this);
        }

        public void CalculateRoleDuration(IAudioSegmentor segmentor)
        {
            if (segmentor.FramesCountByFragmentWithoutSilenceFirstChannel == null) return;
            var totalFramesSpeech = 0;
            var totalFirstChannelFramesSpeech = 0;
            var totalSecondChannelFramesSpeech = 0;

            if (AudioChannelsType == AudioChannelsType.BothVoicesInOneChannel)
            {
                for (var i = 0; i < Fragments.Count; i++)
                {
                    var framesCount = segmentor.FramesCountByFragmentWithoutSilenceFirstChannel[i];
                    if (string.IsNullOrEmpty(Fragments[i].RecognizedText)) continue;
                    if (Fragments[i].AuthorType == RoleType.Operator.ToString())
                        totalSecondChannelFramesSpeech += framesCount;
                    else
                        totalFirstChannelFramesSpeech += framesCount;
                    totalFramesSpeech += framesCount;
                }

                if (totalFramesSpeech == 0)
                    return;

                AudioDialogDetails.ClientSpeechDurationPercents = Math.Round(totalFirstChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
                AudioDialogDetails.OperatorSpeechDurationPercents = Math.Round(totalSecondChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
            }
            else
            {
                var firstChannelIdx = 0;
                var secondChannelIdx = 0;

                foreach (var fragment in Fragments)
                {
                    if (string.IsNullOrEmpty(fragment.AuthorId)) continue;

                    if (fragment.AuthorType == RoleType.Operator.ToString())
                    {
                        int framesCount;
                        if (OperatorChannelNumber == 0)
                        {
                            framesCount = segmentor.FramesCountByFragmentWithoutSilenceFirstChannel[firstChannelIdx];
                            firstChannelIdx++;
                            totalFirstChannelFramesSpeech += framesCount;
                        }
                        else
                        {
                            framesCount = segmentor.FramesCountByFragmentWithoutSilenceSecondChannel[secondChannelIdx];
                            secondChannelIdx++;
                            totalSecondChannelFramesSpeech += framesCount;
                        }
                        totalFramesSpeech += framesCount;
                    }
                    else
                    {
                        int framesCount;
                        if (OperatorChannelNumber == 1)
                        {
                            framesCount = segmentor.FramesCountByFragmentWithoutSilenceFirstChannel[firstChannelIdx];
                            firstChannelIdx++;
                            totalFirstChannelFramesSpeech += framesCount;
                        }
                        else
                        {
                            framesCount = segmentor.FramesCountByFragmentWithoutSilenceSecondChannel[secondChannelIdx];
                            secondChannelIdx++;
                            totalSecondChannelFramesSpeech += framesCount;
                        }
                        totalFramesSpeech += framesCount;
                    }
                }

                if (totalFramesSpeech == 0)
                    return;

                if (OperatorChannelNumber == 0)
                {
                    AudioDialogDetails.ClientSpeechDurationPercents = Math.Round(totalSecondChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
                    AudioDialogDetails.OperatorSpeechDurationPercents = Math.Round(totalFirstChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
                }
                else
                {
                    AudioDialogDetails.ClientSpeechDurationPercents = Math.Round(totalFirstChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
                    AudioDialogDetails.OperatorSpeechDurationPercents = Math.Round(totalSecondChannelFramesSpeech * 100.0 / totalFramesSpeech, 2);
                }
            }
        }

        public void GetTextAndRoleFromFragment(IVoicesSeparator voicesIdentifier, ISpeechRecognizingStrategy primarySpeechRecognizer,
            ISpeechRecognizingStrategy secondarySpeechRecognizer = null, List<string> operatorNamesVocabulary = null)
        {
            if (AudioChannelsType == AudioChannelsType.BothVoicesInOneChannel)
            {
                // recognize and identify async
                var taskList = new List<Task>();
                var recognize = Task.Run(() => Recognize(primarySpeechRecognizer, secondarySpeechRecognizer));
                var voiceIdentify = Task.Run(() => IdentifySpeakersBothVoicesInOnChannel(voicesIdentifier));
                taskList.Add(recognize);
                taskList.Add(voiceIdentify);
                Task.WaitAll(taskList.ToArray());
            }
            else if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.UseYandexLongAudioFilesRecognizingSystem)
            {
                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.NeedTakeOperatorNameFromContent ||
                    StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.NeedTakeOperatorNameFromFileName)
                {
                    // only recognize text, and an operator's will be taken from vocabulary
                    _operatorNamesVocabulary = operatorNamesVocabulary;
                    var recognize = Task.Run(() => Recognize(primarySpeechRecognizer, secondarySpeechRecognizer));
                    IdentifySpeakersOneVoiceInEveryChannelWhenOperatorNameFromVocabularyOrFileName();
                    recognize.Wait();
                }
                else
                {
                    // recognize and identify async
                    var taskList = new List<Task>();
                    var recognize = Task.Run(() => Recognize(primarySpeechRecognizer, secondarySpeechRecognizer));
                    var voiceIdentify = Task.Run(() => IdentifySpeakersOneVoiceInEveryChannel(voicesIdentifier));
                    taskList.Add(recognize);
                    taskList.Add(voiceIdentify);
                    Task.WaitAll(taskList.ToArray());
                }
            }
            else
            {
                // at first, we need to know what channel number has every role
                var voiceIdentify = Task.Run(() => IdentifySpeakersOneVoiceInEveryChannel(voicesIdentifier));
                voiceIdentify.Wait();
                var recognize = Task.Run(() => Recognize(primarySpeechRecognizer, secondarySpeechRecognizer));
                recognize.Wait();
            }
        }

        public async Task GetDialogFromFullFile(int channelsCount, RecognizerType recognizerType, string language, bool onlyRoles = false, bool useUsedeskKeys = true)
        {
            IdentifySpeakersOneVoiceInEveryChannelWhenOperatorNameFromVocabularyOrFileName();
            if (!onlyRoles)
            {
                if (recognizerType == RecognizerType.Kaldi)
                {
                    if (useUsedeskKeys)
                        await RecognizeWithYandex(channelsCount);
                    else
                        await RecognizeWithKaldi(channelsCount, language);
                }
                else
                {
                    if (useUsedeskKeys)
                        await RecognizeWithYandex(channelsCount);
                    else
                        await RecognizeWithYandexAlloka(channelsCount);
                }
            }
        }

        public async Task IdentifySpeakersOneVoiceInEveryChannel(IVoicesSeparator voicesIdentifier)
        {
            _logger.LogMessage("Audio file voices identification for one voice in every channel audio type");
            try
            {
                var takeFirstSecondsCount = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .TakeFirstSecondsCount;
                var minimumAllowAudioLengthMillisecs = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .MinimumAllowAudioLengthMillisecs;
                ClientId = Guid.NewGuid().ToString();
                AudioDialogDetails.ClientId = ClientId;

                // define what channel has operator
                var operatorIdxChannel = -1; // we don't know operator's channel
                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.OperatorChannel ==
                    OperatorChannel.Left)
                    operatorIdxChannel = 0;
                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.OperatorChannel ==
                    OperatorChannel.Right)
                    operatorIdxChannel = 1;

                var attemptsToMakeDecision = 3;
                //var startOperatorProfileDefinitionFragment = Math.Max(0, Math.Min(Fragments.Count - 5, StatisticsAnalysisConfiguration.Instance
                //    .AudioProcessingSettings.StartDefinitionOperatorFragmentIdx - 5));

                var voicesVoter = new VoicesVoter(attemptsToMakeDecision);
                if (operatorIdxChannel == 0 || operatorIdxChannel == -1)
                {

                    for (var i = Fragments.Count - 1; i >= 0; i--) // we start from tail of record to exclude IVR influence for correct voice detection
                    {
                        if (voicesVoter.GetFinalProfile() != null)
                            break;

                        _logger.LogMessage(
                            $"Identification speaker of {i + 1} fragment of {Fragments.Count} channel {0}");
                        var audioFragmentDescriptor = Fragments[i];
                        if (!audioFragmentDescriptor.SrcFilePath.EndsWith("_first.mp3"))
                            continue;

                        var isCorrect = await _audioConvertingComposite.CheckCorrectFragmentMp3AndCrop(
                            audioFragmentDescriptor.SrcFilePath, TmpTransformForVoicesIdentifyFilesDir,
                            takeFirstSecondsCount,
                            minimumAllowAudioLengthMillisecs).ConfigureAwait(false);
                        if (!isCorrect)
                            continue;

                        var transformedFilePath =
                            await _audioConvertingComposite.TransformFromCroppedMp3ToWavMono16OneChannel(
                                TmpTransformForVoicesIdentifyFilesDir, 0);

                        var profile = await voicesIdentifier
                            .GetTargetVoiceProfile(transformedFilePath)
                            .ConfigureAwait(false); // check match profile with base

                        if (profile == null)
                            continue;

                        var operatorId = profile.ProfileId;
                        if (!string.IsNullOrEmpty(profile.Name))
                        {
                            operatorId = $"{profile.Name}";
                            //operatorProfile = $"{profile.Name}-{profile.ProfileId}";
                        }

                        voicesVoter.AddProfile(operatorId);

                        var finalProfile = voicesVoter.GetFinalProfile();

                        if (finalProfile != null)
                        {
                            OperatorId = finalProfile;
                            break;
                        }
                    }
                }

                if (operatorIdxChannel == 0)
                {
                    OperatorChannelNumber = 0;
                    AudioDialogDetails.OperatorId = OperatorId;
                }

                if (operatorIdxChannel == -1)
                {
                    if (!string.IsNullOrEmpty(OperatorId))
                    {
                        OperatorChannelNumber = 0;
                        AudioDialogDetails.OperatorId = OperatorId;
                    }
                }

                if (string.IsNullOrEmpty(OperatorId))
                {
                    // seems like operator has second channel
                    // check it
                    for (var i = Fragments.Count - 1; i >= 0; i--) // we start from tail of record to exclude IVR influence for correct voice detection
                    {
                        if (voicesVoter.GetFinalProfile() != null)
                            break;

                        _logger.LogMessage(
                            $"Identification speaker of {i + 1} fragment of {Fragments.Count} channel 1");
                        var audioFragmentDescriptor = Fragments[i];
                        if (!audioFragmentDescriptor.SrcFilePath.EndsWith("_second.mp3"))
                            continue;

                        var isCorrect = await _audioConvertingComposite.CheckCorrectFragmentMp3AndCrop(
                            audioFragmentDescriptor.SrcFilePath, TmpTransformForVoicesIdentifyFilesDir,
                            takeFirstSecondsCount,
                            minimumAllowAudioLengthMillisecs).ConfigureAwait(false);
                        if (!isCorrect)
                            continue;

                        var transformedFilePath =
                            await _audioConvertingComposite.TransformFromCroppedMp3ToWavMono16OneChannel(
                                TmpTransformForVoicesIdentifyFilesDir, 1);

                        var profile = await voicesIdentifier
                            .GetTargetVoiceProfile(transformedFilePath)
                            .ConfigureAwait(false); // check match profile with base

                        if (profile == null)
                            continue;

                        var operatorId = profile.ProfileId;
                        if (!string.IsNullOrEmpty(profile.Name))
                        {
                            operatorId = $"{profile.Name}";
                            //operatorProfile = $"{profile.Name}-{profile.ProfileId}";
                        }

                        voicesVoter.AddProfile(operatorId);

                        var finalProfile = voicesVoter.GetFinalProfile();

                        if (finalProfile != null)
                        {
                            OperatorId = finalProfile;
                            break;
                        }
                    }

                    if (operatorIdxChannel == 1)
                    {
                        OperatorChannelNumber = 1;
                        AudioDialogDetails.OperatorId = OperatorId;
                    }

                    if (operatorIdxChannel == -1)
                    {
                        if (!string.IsNullOrEmpty(OperatorId))
                        {
                            OperatorChannelNumber = 1;
                            AudioDialogDetails.OperatorId = OperatorId;
                        }
                    }
                }

                if (OperatorId == null)
                {
                    OperatorId = voicesVoter.GetMostProbableProfile();
                    AudioDialogDetails.OperatorId = OperatorId;
                }

                _logger.LogMessage("Assign roles for every fragment");
                // Now we can assign role every fragment
                foreach (var audioFragmentDescriptor in Fragments)
                {
                    if (audioFragmentDescriptor.SrcFilePath.EndsWith("_first.mp3"))
                    {
                        if (OperatorChannelNumber == 0)
                        {
                            audioFragmentDescriptor.AuthorId = OperatorId;
                            audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        }
                        else
                        {
                            audioFragmentDescriptor.AuthorId = ClientId;
                            audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                        }
                    }
                    else
                    {
                        if (OperatorChannelNumber == 1)
                        {
                            audioFragmentDescriptor.AuthorId = OperatorId;
                            audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        }
                        else
                        {
                            audioFragmentDescriptor.AuthorId = ClientId;
                            audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor IdentifySpeakersOneVoiceInEveryChannel");
            }
        }

        public void IdentifySpeakersOneVoiceInEveryChannelWhenOperatorNameFromVocabularyOrFileName()
        {
            _logger.LogMessage("Audio file voices identification for one voice in every channel audio type when operator name will be taken from vocabulary or filename");
            try
            {
                ClientId = Guid.NewGuid().ToString();
                AudioDialogDetails.ClientId = ClientId;

                // define what channel has operator
                var operatorIdxChannel = 0;
                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.OperatorChannel ==
                    OperatorChannel.Left)
                    operatorIdxChannel = 0;
                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.OperatorChannel ==
                    OperatorChannel.Right)
                    operatorIdxChannel = 1;

                if (operatorIdxChannel == 0)
                {
                    OperatorChannelNumber = 0;
                    OperatorId = "Operator";
                    AudioDialogDetails.OperatorId = OperatorId;
                }

                if (operatorIdxChannel == 1)
                {
                    OperatorChannelNumber = 1;
                    OperatorId = "Operator";
                    AudioDialogDetails.OperatorId = OperatorId;
                }

                _logger.LogMessage("Assign roles for every fragment");
                // Now we can assign role every fragment
                if (Fragments == null)
                    return;

                foreach (var audioFragmentDescriptor in Fragments)
                {
                    if (audioFragmentDescriptor.SrcFilePath.EndsWith("_first.mp3"))
                    {
                        if (OperatorChannelNumber == 0)
                        {
                            audioFragmentDescriptor.AuthorId = OperatorId;
                            audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        }
                        else
                        {
                            audioFragmentDescriptor.AuthorId = ClientId;
                            audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                        }
                    }
                    else
                    {
                        if (OperatorChannelNumber == 1)
                        {
                            audioFragmentDescriptor.AuthorId = OperatorId;
                            audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        }
                        else
                        {
                            audioFragmentDescriptor.AuthorId = ClientId;
                            audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor IdentifySpeakersOneVoiceInEveryChannelWhenOperatorNameFromVocabularyOrFileName");
            }
        }


        public async Task IdentifySpeakersBothVoicesInOnChannel(IVoicesSeparator voicesIdentifier)
        {
            _logger.LogMessage("Audio file voices identification for both voices in on channel audio type");
            try
            {
                var operatorAssigned = false;
                var operatorProfile = "";
                var operatorGuid = "";
                var clientProfile = Guid.NewGuid().ToString();
                var takeFirstSecondsCount = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.TakeFirstSecondsCount;
                var minimumAllowAudioLengthMillisecs = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.MinimumAllowAudioLengthMillisecs;
                var maxAllowOneTimeSpeechLengthInSeconds = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .MaxAllowedOneTimeSpeechLengthInSeconds;
                var assignOperatorsAutomatically = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .AssignOperatorsAutomatically;
                var getOperatorFromFileName = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.GetOperatorFromFileName;

                if (getOperatorFromFileName)
                {
                    operatorAssigned = true;
                    var assignedProfile = voicesIdentifier.GetProfiles().Find(op => Fragments[0].SrcFilePath.Contains(op.Name));
                    operatorProfile = assignedProfile.Name;
                    operatorGuid = assignedProfile.ProfileId;
                    OperatorId = operatorProfile;
                    AudioDialogDetails.OperatorId = operatorProfile;
                }

                ClientId = clientProfile;
                AudioDialogDetails.ClientId = clientProfile;


                for (var i = 0; i < Fragments.Count; i++)
                {
                    _logger.LogMessage($"Identification speaker of {i + 1} fragment of {Fragments.Count}");
                    var audioFragmentDescriptor = Fragments[i];
                    var isCorrect = await _audioConvertingComposite.CheckCorrectFragmentMp3AndCrop(
                        audioFragmentDescriptor.SrcFilePath, TmpTransformForVoicesIdentifyFilesDir, takeFirstSecondsCount,
                        minimumAllowAudioLengthMillisecs).ConfigureAwait(false);
                    if (!isCorrect)
                        continue;

                    var calcOneTimeSpeech = _audioConvertingComposite.IsFragmentMp3LengthMoreAllowMaxOneTimeSpeechDurationInSeconds(
                        audioFragmentDescriptor.SrcFilePath, maxAllowOneTimeSpeechLengthInSeconds);

                    var transformedFilePath = _audioConvertingComposite.TransformFromCroppedMp3ToWavMono16BothChannelsInOne(TmpTransformForVoicesIdentifyFilesDir);

                    if (!operatorAssigned)
                    {
                        var profile = await voicesIdentifier
                            .GetTargetVoiceProfile(transformedFilePath)
                            .ConfigureAwait(false); // operator already exists in base
                        if (profile == null)
                        {
                            if (assignOperatorsAutomatically)
                            {
                                operatorProfile = await voicesIdentifier
                                    .CreateTargetProfile(transformedFilePath)
                                    .ConfigureAwait(false); // first speech has operator
                                if (string.IsNullOrEmpty(operatorProfile))
                                    continue;

                                operatorAssigned = true;
                            }
                            else
                            {
                                audioFragmentDescriptor.AuthorId = clientProfile;
                                audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                            }
                        }
                        else
                        {
                            operatorGuid = profile.ProfileId;
                            if (!string.IsNullOrEmpty(profile.Name))
                            {
                                operatorProfile = $"{profile.Name}";
                                //operatorProfile = $"{profile.Name}-{profile.ProfileId}";
                            }

                            operatorAssigned = true;
                            OperatorId = operatorProfile;
                            AudioDialogDetails.OperatorId = operatorProfile;
                            audioFragmentDescriptor.AuthorId = operatorProfile;
                            audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        }
                        continue;
                    }

                    var isOperatorProfileResultTuple = await voicesIdentifier.IsTargetVoiceProfile(transformedFilePath, operatorGuid).ConfigureAwait(false);
                    if (isOperatorProfileResultTuple.Item1)
                    {
                        audioFragmentDescriptor.AuthorId = operatorProfile;
                        audioFragmentDescriptor.AuthorType = RoleType.Operator.ToString();
                        if (calcOneTimeSpeech && isOperatorProfileResultTuple.Item2)
                            AudioDialogDetails.InterruptsCount++;
                    }
                    else
                    {
                        audioFragmentDescriptor.AuthorId = clientProfile;
                        audioFragmentDescriptor.AuthorType = RoleType.Client.ToString();
                    }
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor IdentifySpeakersBothVoicesInOnChannel");
            }
        }

        public async Task Recognize(ISpeechRecognizingStrategy primarySpeechRecognizer, ISpeechRecognizingStrategy secondarySpeechRecognizer = null)
        {
            _logger.LogMessage("Audio file Speech Recognition");
            try
            {
                var useYandexLongAudioRecognizing = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.UseYandexLongAudioFilesRecognizingSystem;

                if (useYandexLongAudioRecognizing && AudioChannelsType == AudioChannelsType.OneVoiceInEveryChannel)
                {
                    string srcRecognizingFilePath = "";
                    try
                    {
                        var channelsCount = AudioChannelsType == AudioChannelsType.OneVoiceInEveryChannel ? 2 : 1;
                        srcRecognizingFilePath = _audioConvertingComposite.TransformFromMp3ToWav(MainAudioFilePath,
                            TmpTransformForRecognizingFilesDir, channelsCount);
                        var urlOp = await YandexRecognizer.UploadToBucket(srcRecognizingFilePath).ConfigureAwait(false);

                        _temporalPhrases = await YandexRecognizer.RecognizeDialog(urlOp, channelsCount, OperatorChannelNumber == 0).ConfigureAwait(false);
                        if (_temporalPhrases != null)
                        {
                            _logger.LogMessage($"Recognized {MainAudioFilePath} using yandex");
                            return;
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Yandex Long Audio Recognition Error");
                    }
                    finally
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(srcRecognizingFilePath))
                                await YandexRecognizer.DeleteFromBucket(srcRecognizingFilePath).ConfigureAwait(false);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(e, "Yandex Long Audio Recognition | Delete file from bucket Error");
                        }

                    }
                }

                for (var i = 0; i < Fragments.Count; i++)
                {

                    var audioFragmentDescriptor = Fragments[i];

                    string transformedFilePath;
                    if (AudioChannelsType == AudioChannelsType.BothVoicesInOneChannel)
                        transformedFilePath =
                            _audioConvertingComposite.TransformFromMp3ToWavMono16BothChannelsInOne(
                                audioFragmentDescriptor.SrcFilePath, TmpTransformForRecognizingFilesDir);
                    else
                    {
                        // getting channel with speech, that need recognize
                        int channelIdx;
                        var isOperatorSpeech = audioFragmentDescriptor.AuthorType == RoleType.Operator.ToString();
                        var isOperatorInFirstChannel = OperatorChannelNumber == 0;
                        if (isOperatorSpeech && isOperatorInFirstChannel)
                            channelIdx = 0;
                        else if (isOperatorSpeech)
                            channelIdx = 1;
                        else if (isOperatorInFirstChannel)
                            channelIdx = 1;
                        else
                            channelIdx = 0;

                        transformedFilePath = await _audioConvertingComposite.TransformFromMp3ToWavMono16OneChannel(audioFragmentDescriptor.SrcFilePath, TmpTransformForRecognizingFilesDir, channelIdx);
                    }

                    var recognizedUsedYa = true;
                    var text = await primarySpeechRecognizer.RecognizeAudioSpeech(transformedFilePath).ConfigureAwait(false);
                    if (string.IsNullOrEmpty(text) && secondarySpeechRecognizer != null)
                    {
                        text = await secondarySpeechRecognizer.RecognizeAudioSpeech(transformedFilePath)
                            .ConfigureAwait(false);
                        recognizedUsedYa = false;
                    }

                    audioFragmentDescriptor.RecognizedText = text;
                    var recognizer = recognizedUsedYa ? "ya" : "wit";
                    _logger.LogMessage($"Recognized {i + 1} fragment of {Fragments.Count} using {recognizer}");
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor RecognizeWithYandex");
            }
        }

        public async Task RecognizeWithYandex(int channelsCount)
        {
            var srcRecognizingFilePath = "";
            try
            {
                srcRecognizingFilePath = _audioConvertingComposite.TransformFromMp3ToWav(MainAudioFilePath, TmpTransformForRecognizingFilesDir, channelsCount);

                var urlOp = await YandexRecognizer.UploadToBucket(srcRecognizingFilePath).ConfigureAwait(false);
                _temporalPhrases = await YandexRecognizer.RecognizeDialog(urlOp, channelsCount, OperatorChannelNumber == 0).ConfigureAwait(false);

                if (_temporalPhrases != null)
                {
                    _logger.LogMessage($"Recognized {MainAudioFilePath} using yandex");

                    if (channelsCount == 1)
                        _temporalPhrases = _temporalPhrases.Where(audioDialogPhrase => audioDialogPhrase.AuthorId == "Client").ToArray();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Yandex Long Audio Recognition Error");
            }
            finally
            {
                try
                {
                    if (!string.IsNullOrEmpty(srcRecognizingFilePath))
                    {
                        await YandexRecognizer.DeleteFromBucket(srcRecognizingFilePath).ConfigureAwait(false);
                        if (File.Exists(srcRecognizingFilePath))
                            File.Delete(srcRecognizingFilePath);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Yandex Long Audio Recognition | Delete file from bucket Error");
                }
            }
        }

        public async Task RecognizeWithYandexAlloka(int channelsCount)
        {
            var srcRecognizingFilePath = "";
            try
            {
                srcRecognizingFilePath = _audioConvertingComposite.TransformFromMp3ToWav(MainAudioFilePath, TmpTransformForRecognizingFilesDir, channelsCount);

                var urlOp = await YandexRecognizerAlloka.UploadToBucket(srcRecognizingFilePath).ConfigureAwait(false);
                _temporalPhrases = await YandexRecognizerAlloka.RecognizeDialog(urlOp, channelsCount, OperatorChannelNumber == 0).ConfigureAwait(false);

                if (_temporalPhrases != null)
                {
                    _logger.LogMessage($"Recognized {MainAudioFilePath} using yandex");

                    if (channelsCount == 1)
                        _temporalPhrases = _temporalPhrases.Where(audioDialogPhrase => audioDialogPhrase.AuthorId == "Client").ToArray();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "YandexRecognizerAlloka Long Audio Recognition Error");
            }
            finally
            {
                try
                {
                    if (!string.IsNullOrEmpty(srcRecognizingFilePath))
                    {
                        await YandexRecognizerAlloka.DeleteFromBucket(srcRecognizingFilePath).ConfigureAwait(false);
                        if (File.Exists(srcRecognizingFilePath))
                            File.Delete(srcRecognizingFilePath);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "YandexRecognizerAlloka Long Audio Recognition | Delete file from bucket Error");
                }
            }
        }

        public async Task RecognizeWithKaldi(int channelsCount, string language)
        {
            var lcFilePath = "";
            var rcFilePath = "";
            try
            {
                if (channelsCount == 1)
                {
                    string partialResult;
                    lcFilePath = _audioConvertingComposite.TransformFromMp3ToWav8KHz(MainAudioFilePath,
                        TmpTransformForRecognizingFilesDir, 1);
                    if (!File.Exists(lcFilePath))
                        return;

                    var author = OperatorChannelNumber == 0 ? PhraseAuthors.Operator : PhraseAuthors.Client;
                    (_temporalPhrases, partialResult) = await KaldiRecognizer.RecognizeChannel(lcFilePath, author, language).ConfigureAwait(false);
                    if (_temporalPhrases?.Length == 0 && !string.IsNullOrEmpty(partialResult))
                    {
                        _temporalPhrases = new[] { new AudioDialogPhrase
                        {
                            AuthorId = author.ToString(),
                            StartingSecond = 0,
                            EndingSecond = 999999,
                            Text = partialResult,
                            TextId = Guid.NewGuid().ToString(),
                            Confidence = "1.0"
                        } };
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(SecondMainAudioFilePath))
                    {
                        if (MainProcessingConfiguration.Instance.ClientInLeftChannel)
                        {
                            (lcFilePath, rcFilePath) = _audioConvertingComposite.TransformFromMp3ToTwoWav8MonoChannels(MainAudioFilePath);
                        }
                        else
                        {
                            (rcFilePath, lcFilePath) = _audioConvertingComposite.TransformFromMp3ToTwoWav8MonoChannels(MainAudioFilePath);
                        }
                    }
                    else
                    {
                        if (MainProcessingConfiguration.Instance.ClientInLeftChannel)
                        {
                            (lcFilePath, rcFilePath) =
                                (_audioConvertingComposite.TransformFromMp3ToWav8KHz(MainAudioFilePath,
                                        TmpTransformForRecognizingFilesDir, 1),
                                    _audioConvertingComposite.TransformFromMp3ToWav8KHz(SecondMainAudioFilePath,
                                        TmpTransformForRecognizingFilesDir, 1)
                                );
                        }
                        else
                        {
                            (rcFilePath, lcFilePath) =
                                (_audioConvertingComposite.TransformFromMp3ToWav8KHz(MainAudioFilePath,
                                        TmpTransformForRecognizingFilesDir, 1),
                                    _audioConvertingComposite.TransformFromMp3ToWav8KHz(SecondMainAudioFilePath,
                                        TmpTransformForRecognizingFilesDir, 1)
                                );
                        }
                    }

                    if (!File.Exists(lcFilePath) || !File.Exists(rcFilePath))
                        return;

                    var phrasesLc = await KaldiRecognizer.RecognizeChannel(lcFilePath, PhraseAuthors.Client, language).ConfigureAwait(false);
                    var phrasesRc = await KaldiRecognizer.RecognizeChannel(rcFilePath, PhraseAuthors.Operator, language).ConfigureAwait(false);

                    if (phrasesLc.Item1?.Length == 0 && !string.IsNullOrEmpty(phrasesLc.Item2))
                    {
                        phrasesLc.Item1 = new[] { new AudioDialogPhrase
                        {
                            AuthorId = PhraseAuthors.Client.ToString(),
                            StartingSecond = 0,
                            EndingSecond = 999999,
                            Confidence = "1.0",
                            Text = phrasesLc.Item2,
                            TextId = Guid.NewGuid().ToString()
                        } };
                    }

                    if (_temporalPhrases?.Length == 0 && !string.IsNullOrEmpty(phrasesRc.Item2))
                    {
                        phrasesRc.Item1 = new[] { new AudioDialogPhrase
                        {
                            AuthorId = PhraseAuthors.Operator.ToString(),
                            StartingSecond = 0,
                            EndingSecond = 999999,
                            Confidence = "1.0",
                            Text = phrasesRc.Item2,
                            TextId = Guid.NewGuid().ToString()
                        } };
                    }

                    _temporalPhrases = KaldiRecognizer.MergeChannels(phrasesLc.Item1, phrasesRc.Item1);
                }

                if (_temporalPhrases != null)
                {
                    _logger.LogMessage($"Recognized {MainAudioFilePath} using kaldi");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Kaldi ws Audio Recognition Error");
            }
            finally
            {
                try
                {
                    if (File.Exists(lcFilePath))
                        File.Delete(lcFilePath);
                    if (File.Exists(rcFilePath))
                        File.Delete(rcFilePath);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Kaldi ws Audio Recognition | Delete file Error");
                }
            }
        }

        public IDialogDescriptor GenerateDialog()
        {
            _logger.LogMessage("Generating text dialog from audio");
            try
            {
                var result = new AudioDialogDescriptor
                {
                    Date = CreationDate,
                    DialogId = Guid.NewGuid().ToString(),
                    Channel = DialogChannel.Audio.ToString(),
                    Phrases = new List<AudioDialogPhrase>(),
                    AudioFileName = Path.GetFileName(MainAudioFilePath),
                    SecondAudioFileName = Path.GetFileName(SecondMainAudioFilePath)
                };

                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.NeedTakeOperatorNameFromContent)
                {
                    OperatorId = GetOperatorNameFromRecognizedText();
                    ClientId = Guid.NewGuid().ToString();
                    AudioDialogDetails.OperatorId = OperatorId;
                    AudioDialogDetails.ClientId = ClientId;
                }

                if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.NeedTakeOperatorNameFromFileName)
                {
                    OperatorId = GetOperatorNameFromFileName();
                    ClientId = Guid.NewGuid().ToString();
                    AudioDialogDetails.OperatorId = OperatorId;
                    AudioDialogDetails.ClientId = ClientId;
                }
                else
                {
                    OperatorId = "Operator";
                    ClientId = "Client";
                    AudioDialogDetails.OperatorId = OperatorId;
                    AudioDialogDetails.ClientId = ClientId;
                }

                result.OperatorId = OperatorId;
                result.ClientId = ClientId;



                if (_temporalPhrases != null)
                {
                    foreach (var audioDialogPhrase in _temporalPhrases)
                    {
                        if (string.IsNullOrEmpty(audioDialogPhrase.Text))
                            continue;

                        var authorId = audioDialogPhrase.AuthorId == "Operator" ? OperatorId : ClientId;

                        result.Phrases.Add(new AudioDialogPhrase
                        {
                            AuthorId = authorId,
                            Text = audioDialogPhrase.Text,
                            TextId = Guid.NewGuid().ToString(),
                            StartingSecond = audioDialogPhrase.StartingSecond,
                            EndingSecond = audioDialogPhrase.EndingSecond,
                            Confidence = audioDialogPhrase.Confidence
                        });
                        result.FullText += audioDialogPhrase.Text + "\n";
                    }
                }
                else
                {
                    if (Fragments == null)
                        return result;

                    foreach (var audioFragmentDescriptor in Fragments)
                    {
                        if (string.IsNullOrEmpty(audioFragmentDescriptor.RecognizedText))
                            continue;

                        if (string.IsNullOrEmpty(audioFragmentDescriptor.AuthorId))
                            continue;

                        result.Phrases.Add(new AudioDialogPhrase
                        {
                            AuthorId = audioFragmentDescriptor.AuthorId,
                            Text = audioFragmentDescriptor.RecognizedText,
                            TextId = Guid.NewGuid().ToString()
                        });
                        result.FullText += audioFragmentDescriptor.RecognizedText + "\n";
                    }
                }

                if (result.Phrases == null || result.Phrases.Count == 0)
                    return null;

                return result;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor RecognizeWithYandex");
                return null;
            }
        }

        public void BackupAudioFile(bool forseIntoCorrupted = false)
        {
            var backupDir = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings.AudioDataBackupPath;
            if (!Directory.Exists(backupDir))
                Directory.CreateDirectory(backupDir);

            if (OperatorId == null || forseIntoCorrupted)
            {
                backupDir = StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .CorruptedAudioFilesBackupPath;
                if (!Directory.Exists(backupDir))
                    Directory.CreateDirectory(backupDir);
            }

            try
            {
                if (File.Exists(MainAudioFilePath))
                {
                    var name = Path.GetFileName(MainAudioFilePath);
                    File.Move(MainAudioFilePath, Path.Combine(backupDir, name));
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "AudioDescriptor BackupAudioFile");
            }
        }

        public void ClearTmpDirs()
        {
            if (Directory.Exists(TmpTransformForVoicesIdentifyFilesDir))
                Directory.Delete(TmpTransformForVoicesIdentifyFilesDir, true);

            if (Directory.Exists(TmpTransformForRecognizingFilesDir))
                Directory.Delete(TmpTransformForRecognizingFilesDir, true);
        }

        private string GetOperatorNameFromRecognizedText()
        {
            var operatorName = "Unknown";
            if (_temporalPhrases == null) return operatorName;
            var maxCountPhrasesToSearchName = 5;
            var currentCheckedPhrases = 0;
            foreach (var audioDialogPhrase in _temporalPhrases)
            {
                if (currentCheckedPhrases >= maxCountPhrasesToSearchName)
                    break;

                if (audioDialogPhrase.AuthorId != "Operator") continue;
                var name = GetOperatorNameFromPhrase(audioDialogPhrase.Text);
                if (string.IsNullOrEmpty(name))
                {
                    currentCheckedPhrases++;
                    continue;
                }
                operatorName = name;
                break;
            }

            return operatorName;
        }

        private string GetOperatorNameFromFileName()
        {
            var fileName = Path.GetFileNameWithoutExtension(MainAudioFilePath);
            if (StatisticsAnalysisConfiguration.Instance.AudioProcessingSettings
                    .TakingOperatorNameFromFileNameRuleNumber == 0)
                return fileName.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries)[0];
            return "UnknownOperator";
        }

        private string GetOperatorNameFromPhrase(string phrase)
        {
            return _operatorNamesVocabulary.FirstOrDefault(name => IsNameInPhrase(phrase, name));
        }

        private static bool IsNameInPhrase(string phrase, string name)
        {
            var template = $"[ .,?]{name.ToLower()}[ ,.?]";
            return Regex.Matches($" {phrase.ToLower()} ", template).Count > 0;
        }

    }
}

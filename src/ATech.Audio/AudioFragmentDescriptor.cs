﻿using ATech.Audio.Interfaces;

namespace ATech.Audio
{
    public class AudioFragmentDescriptor : IAudioFragmentDescriptor
    {
        public string SrcFilePath { get; set; }
        public string RecognizedText { get; set; }
        public string AuthorId { get; set; }
        public string AuthorType { get; set; }
        public bool IsOneTimeSpeech { get; set; }
    }
}

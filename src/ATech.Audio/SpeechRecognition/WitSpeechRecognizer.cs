﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json.Linq;

namespace ATech.Audio.SpeechRecognition
{
    public class WitSpeechRecognizer : ISpeechRecognizingStrategy
    {
        private readonly string _url = MainProcessingConfiguration.Instance.WitAiApiData.Url;

        private readonly string _apiKey;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public WitSpeechRecognizer(string apiKey)
        {
            _apiKey = apiKey;
        }

        public async Task<string> RecognizeAudioSpeech(string audioPath)
        {
            var bytes = await File.ReadAllBytesAsync(audioPath).ConfigureAwait(false);
            var jsonAnswer = await Post(bytes).ConfigureAwait(false);
            await Task.Delay(500);
            return jsonAnswer == null ? null : GetTextFromAnswer(jsonAnswer);
        }

        private string GetTextFromAnswer(string jsonAnswer)
        {
            var jObj = JObject.Parse(jsonAnswer);
            if (jObj["_text"] != null)
                return jObj["_text"].ToString();
            _logger.LogMessage("Error in recognizing", "WitSpeechRecognizer GetTextFromAnswer");
            return "";
        }

        private async Task<string> Post(byte[] audioBytes)
        {
            var request = (HttpWebRequest)WebRequest.Create(_url);

            request.Method = "POST";
            request.Accept = "audio/x-wav";
            request.ContentType = "audio/wav";
            request.ContentLength = audioBytes.Length;
            request.Headers.Add("Authorization", $"Bearer {_apiKey}");

            try
            {
                var stream = await request.GetRequestStreamAsync().ConfigureAwait(false);
                await stream.WriteAsync(audioBytes, 0, audioBytes.Length).ConfigureAwait(false);
                var response = await request.GetResponseAsync().ConfigureAwait(false);

                string result;
                using (var rdr = new StreamReader(response.GetResponseStream()))
                    result = await rdr.ReadToEndAsync();
                return result;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "WitSpeechRecognizer Post");
                return null;
            }
        }
    }
}

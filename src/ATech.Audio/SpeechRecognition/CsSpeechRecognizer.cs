﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json.Linq;

namespace ATech.Audio.SpeechRecognition
{
    public class CsSpeechRecognizer : ISpeechRecognizingStrategy
    {
        private readonly string _url = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.SpeechRecognitionUrl;

        private readonly string _apiKey;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public CsSpeechRecognizer(string apiKey)
        {
            _apiKey = apiKey;
        }

        public async Task<string> RecognizeAudioSpeech(string audioPath)
        {
            try
            {
                var result = await Post(audioPath).ConfigureAwait(false);
                return GetTextFromAnswer(result);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "CsSpeechRecognizer RecognizeAudioSpeech");
                return "";
            }
        }

        private string GetTextFromAnswer(string jsonAnswer)
        {
            var jObj = JObject.Parse(jsonAnswer);
            if (jObj["RecognitionStatus"] != null)
            {
                var status = jObj["RecognitionStatus"].ToString();
                if (status != "Success")
                    return "";

                if (jObj["DisplayText"] != null)
                {
                    return jObj["DisplayText"].ToString();
                }
            }
                
            _logger.LogMessage("Error in recognizing", "CsSpeechRecognizer GetTextFromAnswer");
            return "";
        }

        private async Task<string> Post(string audioFile)
        {
            var request = (HttpWebRequest)WebRequest.Create(_url);
            request.SendChunked = true;
            request.Accept = @"application/json;text/xml";
            request.Method = "POST";
            request.Host = "westeurope.stt.speech.microsoft.com";
            request.ProtocolVersion = HttpVersion.Version11;
            request.ContentType = @"audio/wav; codecs=audio/pcm; samplerate=16000";
            request.Headers["Ocp-Apim-Subscription-Key"] = _apiKey;
            request.AllowWriteStreamBuffering = false;

            using (var fs = new FileStream(audioFile, FileMode.Open, FileAccess.Read))
            {
                using (var requestStream = await request.GetRequestStreamAsync().ConfigureAwait(false))
                {
                    var buffer = new byte[checked((uint)Math.Min(1024, (int)fs.Length))];
                    int bytesRead;
                    while ((bytesRead = await fs.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false)) != 0)
                        await requestStream.WriteAsync(buffer, 0, bytesRead);

                    await requestStream.FlushAsync().ConfigureAwait(false);
                }
            }

            var response = await request.GetResponseAsync().ConfigureAwait(false);
            string result;
            using (var rdr = new StreamReader(response.GetResponseStream()))
                result = await rdr.ReadToEndAsync().ConfigureAwait(false);
            return result;
        }
    }
}

﻿using System.Threading.Tasks;

namespace ATech.Audio.SpeechRecognition
{
    public interface ISpeechRecognizingStrategy
    {
        Task<string> RecognizeAudioSpeech(string audioPath);
    }
}

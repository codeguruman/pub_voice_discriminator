﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ATech.Audio.AudioProcessing.DataTransfering;
using ATech.Common.Dal.Models;
using Newtonsoft.Json.Linq;

namespace ATech.Audio.SpeechRecognition
{
    public static class KaldiRecognizer
    {
        public static async Task<(AudioDialogPhrase[], string)> RecognizeChannel(string filePath, PhraseAuthors author, string language)
        {
            var (temporalPhrases, partialResult) = await WsSender.Send(filePath, language);
            var phrasesRes = ExtractPhrasesFromChannel(temporalPhrases, author);
            return (phrasesRes, partialResult);
        }

        public static AudioDialogPhrase[] MergeChannels(AudioDialogPhrase[] lcPhrases, AudioDialogPhrase[] rcPhrases)
        {
            var tmpList = lcPhrases.ToList();
            if (rcPhrases != null)
                tmpList.AddRange(rcPhrases);

            tmpList = tmpList.OrderBy(x => x.StartingSecond).ToList();

            var result = new List<AudioDialogPhrase>();
            AudioDialogPhrase prev = null;
            var needToAddLastPhrase = false;
            var partsOneByOneCount = 1;
            var currentSumConfidence = 1.0;
            foreach (var phrase in tmpList)
            {
                if (prev == null)
                {
                    prev = phrase;
                    needToAddLastPhrase = true;
                    currentSumConfidence = Convert.ToDouble(prev.Confidence, CultureInfo.InvariantCulture);
                    partsOneByOneCount = 1;
                }
                else
                {
                    if (prev.AuthorId == phrase.AuthorId)
                    {
                        prev.Text += " " + phrase.Text;
                        prev.EndingSecond = phrase.EndingSecond;
                        needToAddLastPhrase = true;
                        currentSumConfidence += Convert.ToDouble(phrase.Confidence, CultureInfo.InvariantCulture);
                        partsOneByOneCount++;
                    }
                    else
                    {
                        prev.Confidence = Convert.ToString(currentSumConfidence / partsOneByOneCount, CultureInfo.InvariantCulture);
                        result.Add(prev);
                        prev = phrase;
                        needToAddLastPhrase = false;
                        currentSumConfidence = Convert.ToDouble(prev.Confidence, CultureInfo.InvariantCulture);
                        partsOneByOneCount = 1;
                    }
                }
            }

            if (needToAddLastPhrase)
                result.Add(prev);

            return result.ToArray();
        }

        private static AudioDialogPhrase[] ExtractPhrasesFromChannel(IEnumerable<JObject> temporalResults, PhraseAuthors author)
        {
            var result = new List<AudioDialogPhrase>();

            foreach (var item in temporalResults)
            {
                foreach (var token in item["result"]!)
                {
                    var phrase = new AudioDialogPhrase()
                    {
                        Text = token["word"]!.ToString(),
                        Confidence = token["conf"]!.Value<string>(),
                        StartingSecond =
                            Convert.ToDouble(token["start"]!.Value<string>(), CultureInfo.InvariantCulture),
                        EndingSecond =
                            Convert.ToDouble(token["end"]!.Value<string>(), CultureInfo.InvariantCulture),
                        AuthorId = author.ToString()
                    };
                    result.Add(phrase);
                }
            }

            return result.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ATech.Audio.SourceDownloading
{
    public interface IAudioSourcesLoader
    {
        Task<List<string>> Download(string dirToSave, DateTime @from, DateTime to, List<string> operatorIds);
    }
}

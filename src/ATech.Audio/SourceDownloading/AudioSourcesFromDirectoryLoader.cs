﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ATech.Configuration;
using ATech.Logging.Interfaces;

namespace ATech.Audio.SourceDownloading
{
    public class AudioSourcesFromDirectoryLoader : IAudioSourcesLoader
    {
        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public async Task<List<string>> Download(string dirToSave, DateTime @from, DateTime to, List<string> operatorIds)
        {
            _logger.LogMessage("Audio files loading");

            if (!Directory.Exists(dirToSave))
            {
                _logger.LogMessage("Audio sources do not exist", "AudioSourcesFromDirectoryLoader DownLoad");
                return null;
            }

            await Task.Delay(10).ConfigureAwait(false);

            return Directory.GetFiles(dirToSave).ToList();
        }
    }
}

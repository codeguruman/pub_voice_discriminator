﻿namespace CallProcessor.Models
{
    public class SttAnswer
    {
        public string token { get; set; }

        public string transcription_text { get; set; }

        public string secret { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using ATech.AiServices.Contract;
using ATech.Audio.AudioProcessing;
using ATech.Common.Dal.Models;
using ATech.Common.Dal.Models.VoiceFeatures;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json;

namespace CallProcessor.Models
{
    public class AudioFeatureExtractingDescriptor
    {
        public IAudioFileHandler AudioFileHandler { get; set; }

        public IAudioConvertingComposite AudioConvertingComposite { get; set; }

        public string AudioFileName { get; set; }

        public string SecondAudioFileName { get; set; }

        public List<FragmentFeaturesData> FragmentFeatures { get; set; }

        public InputSettings Settings { get; set; }

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public ISpeechAnalysisAiService SpeechAnalysisAiService { get; set; }

        public async void GetVoiceFeaturesFromAudio()
        {
            var filePath = Path.Combine("InputFiles", AudioFileName);
            var fileName = Path.GetFileName(filePath);
            var filePathSecond = "";
            var fileNameSecond = "";
            var newFileNameLc = Guid.NewGuid().ToString();
            var newFileNameRc = Guid.NewGuid().ToString();
            var newFilePathLc = filePath.Replace(fileName, $"{newFileNameLc}.wav");
            var newFilePathRc = "";
            var fragmentFeaturesLeftFilePath = "";
            var fragmentFeaturesRightFilePath = "";


            try
            {
                if (SecondAudioFileName != null)
                {
                    filePathSecond = SecondAudioFileName;
                    fileNameSecond = Path.GetFileName(filePathSecond);
                }

                newFilePathRc = !string.IsNullOrEmpty(filePathSecond)
                    ? filePathSecond.Replace(fileNameSecond!, $"{newFileNameRc}.wav")
                    : filePath.Replace(fileName, $"{newFileNameRc}.wav");

                var duration = await AudioFileHandler.GetAudioDurationInSeconds(filePath);
                var stepSeconds = 3;

                var currentStepSeconds = 0;
                FragmentFeatures = new List<FragmentFeaturesData>();

                if (Settings.ChannelsCount == 2)
                {
                    if (!string.IsNullOrEmpty(filePathSecond))
                    {
                        if (MainProcessingConfiguration.Instance.ClientInLeftChannel)
                        {
                            (fragmentFeaturesLeftFilePath, fragmentFeaturesRightFilePath) = (
                                AudioConvertingComposite.TransformFromMp3ToWav(filePath, null),
                                AudioConvertingComposite.TransformFromMp3ToWav(filePathSecond, null));
                        }
                        else
                        {
                            (fragmentFeaturesRightFilePath, fragmentFeaturesLeftFilePath) = (
                                AudioConvertingComposite.TransformFromMp3ToWav(filePath, null),
                                AudioConvertingComposite.TransformFromMp3ToWav(filePathSecond, null));
                        }
                    }
                    else
                    {
                        if (MainProcessingConfiguration.Instance.ClientInLeftChannel)
                        {
                            (fragmentFeaturesLeftFilePath, fragmentFeaturesRightFilePath) =
                                AudioConvertingComposite.TransformFromMp3ToTwoWavMonoChannels(filePath);
                        }
                        else
                        {
                            (fragmentFeaturesRightFilePath, fragmentFeaturesLeftFilePath) =
                                AudioConvertingComposite.TransformFromMp3ToTwoWavMonoChannels(filePath);
                        }
                    }
                }
                else
                    fragmentFeaturesLeftFilePath = AudioConvertingComposite.TransformFromMp3ToWav(filePath, null);

                while (currentStepSeconds < duration)
                {
                    AudioFileHandler.CropPartFromFile(fragmentFeaturesLeftFilePath, newFilePathLc, currentStepSeconds, stepSeconds);

                    if (File.Exists(newFilePathLc))
                    {
                        var fragmentFeaturesLeft = Settings.GiveMainVoiceFeatures
                            ? await SpeechAnalysisAiService.GetSimpleVoiceFeaturesFromFile(newFilePathLc)
                                .ConfigureAwait(false)
                            : await SpeechAnalysisAiService.GetVoiceFeaturesFromFile(newFilePathLc)
                                .ConfigureAwait(false);

                        var clientFeatures = new FragmentFeaturesData
                        {
                            Features = JsonConvert.DeserializeObject<FragmentFeatures>(fragmentFeaturesLeft),
                            FromSecond = currentStepSeconds,
                            ToSecond = currentStepSeconds + stepSeconds,
                            Owner = "Client"
                        };

                        FragmentFeatures.Add(clientFeatures);
                    }

                    if (Settings.ChannelsCount > 1)
                    {
                        AudioFileHandler.CropPartFromFile(fragmentFeaturesRightFilePath, newFilePathRc, currentStepSeconds, stepSeconds);

                        if (File.Exists(newFilePathRc))
                        {
                            var fragmentFeaturesRight = Settings.GiveMainVoiceFeatures
                                ? await SpeechAnalysisAiService.GetSimpleVoiceFeaturesFromFile(newFilePathRc)
                                    .ConfigureAwait(false)
                                : await SpeechAnalysisAiService.GetVoiceFeaturesFromFile(newFilePathRc)
                                    .ConfigureAwait(false);

                            var operatorFeatures = new FragmentFeaturesData
                            {
                                Features = JsonConvert.DeserializeObject<FragmentFeatures>(fragmentFeaturesRight),
                                FromSecond = currentStepSeconds,
                                ToSecond = currentStepSeconds + stepSeconds,
                                Owner = "Operator"
                            };

                            FragmentFeatures.Add(operatorFeatures);
                        }
                    }

                    currentStepSeconds += stepSeconds;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"AudioDialogProcessing {filePath}");
            }
            finally
            {
                if (File.Exists(filePath))
                    File.Delete(filePath);
                if (File.Exists(filePathSecond))
                    File.Delete(filePathSecond);

                if (File.Exists(newFilePathRc))
                    File.Delete(newFilePathRc);
                if (File.Exists(newFilePathLc))
                    File.Delete(newFilePathLc);

                if (File.Exists(fragmentFeaturesLeftFilePath))
                    File.Delete(fragmentFeaturesLeftFilePath);
                if (File.Exists(fragmentFeaturesRightFilePath))
                    File.Delete(fragmentFeaturesRightFilePath);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;

namespace CallProcessor.Contract
{
    public interface INotificationService
    {
        Task SendNotification(AnswerModel model, string apikey, string taskId);
    }
}

﻿using System.Threading.Tasks;
using ATech.Common.Dal.Models;

namespace CallProcessor.Contract
{
    public interface IAudioProcessingService
    {
        void Init();
        Task<AnswerModel> ProcessAudio(string filePath, InputSettings settings, ApiType apiType, string secondFilePath = "");
    }
}

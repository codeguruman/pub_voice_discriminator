﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ATech.Common.Licensing;
using ATech.Configuration;
using Newtonsoft.Json;

namespace CallProcessor.Collectors
{
    public class UsersCollector
    {
        private readonly ConcurrentDictionary<string, int> handleItemsCount;

        private readonly string statPath;

        #region Singleton

        private static readonly Lazy<UsersCollector> _instance =
            new Lazy<UsersCollector>(() => new UsersCollector());

        public static UsersCollector Instance => _instance.Value;

        private UsersCollector()
        {
            handleItemsCount = new ConcurrentDictionary<string, int>();
            statPath = MainProcessingConfiguration.UsersStatisticsFilePath;
            if (File.Exists(statPath))
                handleItemsCount = JsonConvert.DeserializeObject<ConcurrentDictionary<string, int>>(File.ReadAllText(statPath));
            foreach (var item in handleItemsCount)
            {
                new LicenseClient(item.Key);
            }

            Task.Run(StatisticsSaver);
        }

        #endregion


        public List<string> GetUsers()
        {
            return handleItemsCount.Keys.ToList();
        }

        public bool CheckUserAndUpdateStat(string apiKey)
        {
            if (string.IsNullOrEmpty(apiKey))
                return false;

            if (!handleItemsCount.TryGetValue(apiKey, out var val))
                return false;

            handleItemsCount.AddOrUpdate(apiKey, val, (k, t) => t + 1);
            return true;
        }

        private async void StatisticsSaver()
        {
            while (true)
            {
                var data = JsonConvert.SerializeObject(handleItemsCount, Formatting.Indented);
                await File.WriteAllTextAsync(statPath, data);

                for (var i = 0; i < 300; i++)
                    await Task.Delay(1000);
            }
        }
    }
}

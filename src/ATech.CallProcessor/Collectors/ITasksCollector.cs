﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;
using CallProcessor.Contract;

namespace CallProcessor.Collectors
{
    public interface ITasksCollector
    {
        void Init(IAudioProcessingService processingService);

        string AddTask(ProcessingTask task);

        Task<bool> ClearErrorTasks();

        Task<AnswerModel> GetTaskResult(string taskId);

        Task<ProcessingTaskState []> GetTasksStates(string userId);

        Task<List<ProcessingTask>> GetTaskResults();

        Task<bool> SendNotification(string apikey, string taskId);
    }
}

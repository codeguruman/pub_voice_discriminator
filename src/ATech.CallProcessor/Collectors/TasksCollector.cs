﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;
using ATech.Common.Dal.Repositiory;
using ATech.Logging.Interfaces;
using ATech.Configuration;
using CallProcessor.Contract;
using CallProcessor.Services;

namespace CallProcessor.Collectors
{
    public class TasksCollector : ITasksCollector
    {
        private readonly ConcurrentDictionary<string, ProcessingTask> _tasks;

        private IAudioProcessingService _audioProcessingService;

        private readonly UseDeskAnswerSendingService _useDeskAnswerSendingService;

        private readonly TimeSpan _liveRecognitionTaskHours;

        private readonly IProcessingTaskRepository _repository;

        private readonly INotificationService _notificationService;

        private readonly string _tmpDir;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public TasksCollector(IProcessingTaskRepository repository, INotificationService notificationService)
        {
            _repository = repository;
            _notificationService = notificationService;
            _tasks = new ConcurrentDictionary<string, ProcessingTask>();
            _useDeskAnswerSendingService = new UseDeskAnswerSendingService();
            _liveRecognitionTaskHours = new TimeSpan(MainProcessingConfiguration.LiveRecognitionTaskHours, 0, 0);
            _tmpDir = MainProcessingConfiguration.TmpAudioDir;
            Task.Run(TasksCleaner);
            Task.Run(TaskRunner);
            Task.Run(ErrorTasksCleaner);
        }


        public void Init(IAudioProcessingService processingService)
        {
            _audioProcessingService ??= processingService;
        }

        public string AddTask(ProcessingTask task)
        {
            var key = task.RecognitionTask.Ticket_id ?? Guid.NewGuid().ToString();
            task.RecognitionTask.Ticket_id = key;
            task.CreateDate = DateTime.Now;
            task.TaskId = key;
            _tasks.AddOrUpdate(key, task, (k, t) => t);
            Task.Run(() => ProcessTask(task));
            return key;
        }

        public async Task<bool> ClearErrorTasks()
        {
            return await _repository.RemoveTasksWithError();
        }

        public async Task<ProcessingTaskState []> GetTasksStates(string userId)
        {
            var result = new List<ProcessingTaskState>();
            var dir = MainProcessingConfiguration.MainLocalAudioDir;
            if (!Directory.Exists(dir))
                return null;

            var path = Path.Combine(dir, userId);
            if (!Directory.Exists(path))
                return null;

            var files = Directory.GetFiles(path);
            foreach (var file in files)
            {
                try
                {
                    var name = GetNameByUserAndPath(userId, file);
                    var taskResult = await GetTaskId(name);
                    if (taskResult != null)
                    {
                        result.Add(taskResult);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "TasksCollector GetTasksStates");
                }
            }

            return result.ToArray();
        }

        public async Task<List<ProcessingTask>> GetTaskResults()
        {
            try
            {
                return await _repository.GetAllProcessingTasks();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "TasksCollector GetTaskResults");
                return null;
            }
        }

        public async Task<bool> SendNotification(string apikey, string taskId)
        {
            try
            {
                var taskFromDb = await _repository.GetProcessingTaskByName(taskId);
                if (taskFromDb?.AnswerModel == null)
                    return false;

                return await SendNotification(taskFromDb);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "TasksCollector SendNotification");
                return false;
            }
        }

        private async Task<bool> SendNotification(ProcessingTask processingTask)
        {
            try
            {
                await _notificationService.
                    SendNotification(processingTask.AnswerModel, processingTask.UserKey, processingTask.TaskId);

                processingTask.NotificationSent = true;
                await _repository.UpdateProcessingTask(processingTask);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "TasksCollector SendNotification");
                return false;
            }

            return true;
        }

        public async Task<AnswerModel> GetTaskResult(string taskId)
        {
            if (!_tasks.TryGetValue(taskId, out var task))
            {
                ProcessingTask taskFromDb;
                try
                {
                    taskFromDb = await _repository.GetProcessingTaskByName(taskId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "TasksCollector GetTaskResult");
                    return new AnswerModel("Task_is_not_found");
                }

                if (taskFromDb != null)
                {
                    taskFromDb.AnswerModel.FilterFeatureIntervals();
                    return taskFromDb.AnswerModel;
                }
                return new AnswerModel("Task_is_not_found");
            }
            return task.AnswerModel ?? new AnswerModel("Task_is_not_ready");
        }

        private async Task<ProcessingTaskState> GetTaskId(string filename)
        {
            try
            {
                var taskFromDb = await _repository.GetProcessingTaskByFilename(filename);
                if (taskFromDb?.AnswerModel != null)
                {
                    if (taskFromDb?.AnswerModel?.Status == "Ok")
                    {
                        var text = taskFromDb.AnswerModel?.AudioDialogDescriptor?.FullText;
                        if (string.IsNullOrWhiteSpace(text))
                            return null;

                        if (taskFromDb.AnswerModel?.AudioDialogDescriptor.GetPhrasesCount(true) < 1 ||
                            taskFromDb.AnswerModel?.AudioDialogDescriptor.GetPhrasesCount(false) < 1)
                            return null;

                        var moodMetrics = taskFromDb.AnswerModel?.CalcMoodMetrics();
                        if (moodMetrics?.AvgMoodLevel == null ||
                            Math.Abs(moodMetrics.AvgMoodLevel) < 0.01)
                        {
                            await _repository.RemoveTaskById(taskFromDb.TaskId);
                            return null;
                        }

                        var badPhrasesMetrics = taskFromDb.AnswerModel?.CalcBadPhrasesMetrics();

                        return new ProcessingTaskState
                        {
                            CreateDate = taskFromDb.AnswerModel.AudioDialogDescriptor.Date,
                            Error = "",
                            FileName = filename,
                            RecognizedText = text,
                            MoodMetrics = moodMetrics,
                            BadWordsMetrics = badPhrasesMetrics,
                            TaskId = taskFromDb.TaskId,
                            NotificationSent = taskFromDb.NotificationSent
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "TasksCollector GetTaskId");
            }

            return null;
        }

        private async Task ProcessTask(ProcessingTask task)
        {
            var answerModel = await _audioProcessingService.ProcessAudio(task.FilePath, task.InputSettings, task.ApiType, task.SecondFilePath)
                .ConfigureAwait(false);
            if (_tasks.TryGetValue(task.RecognitionTask.Ticket_id, out task))
            {
                task.AnswerModel = answerModel;

                if (File.Exists(task.FilePath))
                    File.Delete(task.FilePath);
                if (!string.IsNullOrEmpty(task.SecondFilePath))
                    if (File.Exists(task.SecondFilePath))
                        File.Delete(task.SecondFilePath);

                _tasks.AddOrUpdate(task.RecognitionTask.Ticket_id, task, (k, t) => task);

                if (task.ApiType == ApiType.UseDesk)
                {
                    var sttAnswer = _useDeskAnswerSendingService.GetSttAnswerModelFromDefaultModel(task);

                    if (!string.IsNullOrEmpty(task.RecognitionTask.callback_url))
                    {
                        await _useDeskAnswerSendingService.SendAnswer(sttAnswer, task.RecognitionTask.callback_url);
                    }
                }

                if (task.AnswerModel != null && task.AnswerModel.Status != "Error")
                {
                    try
                    {
                        await _repository.InsertProcessingTask(task);
                        await GenerateAutoNotification(task);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "TasksCollector ProcessTask");
                    }
                }
            }
        }

        private async Task GenerateAutoNotification(ProcessingTask task)
        {
            if (MainProcessingConfiguration.Instance.SendNotificationsAutomatically)
            {
                if (task.AnswerModel.NeedToSendNotificationAutomatically(
                    MainProcessingConfiguration.Instance.NotificationThreshold))
                {
                    await SendNotification(task);
                }
            }
        }

        private async void TasksCleaner()
        {
            while (true)
            {
                var deleteKeysList = 
                    (from processingTask in _tasks
                    where DateTime.Now - processingTask.Value.CreateDate > _liveRecognitionTaskHours
                    select processingTask.Key).ToList();

                foreach (var key in deleteKeysList)
                    _tasks.TryRemove(key, out _);

                for (var i = 0; i < 300; i++)
                    await Task.Delay(1000).ConfigureAwait(false);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private async void ErrorTasksCleaner()
        {
            while (true)
            {
                await _repository.RemoveTasksWithError().ConfigureAwait(false);

                for (var i = 0; i < 600; i++)
                    await Task.Delay(1000).ConfigureAwait(false);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private async void TaskRunner()
        {
            var settings = new InputSettings
            {
                ChannelsCount = 2,
                GiveStatRegulation = false,
                GiveMainVoiceFeatures = true,
                GiveTextFromAudio = true,
                ProcessingFromFile = true,
                Language = "ru",
                RecognizerType = RecognizerType.Kaldi
            };

            var dir = MainProcessingConfiguration.MainLocalAudioDir;
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);


            var usersDirNames = UsersCollector.Instance.GetUsers();
            foreach (var usersDirName in usersDirNames)
            {
                var path = Path.Combine(dir, usersDirName);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }

            while (true)
            {
                foreach (var userDirName in usersDirNames)
                {
                    var path = Path.Combine(dir, userDirName);
                    var files = Directory.GetFiles(path);
                    foreach (var file in files)
                    {
                        try
                        {
                            var name = GetNameByUserAndPath(userDirName, file);
                            var taskResult = await GetTaskId(name);
                            if (taskResult == null)
                            {
                                var tmpFilePath = Path.Combine(_tmpDir, name);
                                if (!File.Exists(tmpFilePath))
                                    File.Copy(file, tmpFilePath);
                                var key = Guid.NewGuid().ToString();
                                var task = new RecognitionTask
                                {
                                    Ticket_id = key
                                };

                                var processingTask = new ProcessingTask
                                {
                                    RecognitionTask = task,
                                    InputSettings = settings,
                                    FilePath = tmpFilePath,
                                    FileName = name,
                                    UserKey = userDirName,
                                    ApiType = ApiType.Default,
                                    TaskId = key,
                                    CreateDate = DateTime.Now,
                                    NotificationSent = false
                                };

                                _tasks.AddOrUpdate(key, processingTask, (k, t) => t);
                                await ProcessTask(processingTask);
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "TasksCollector TaskRunner");
                        }
                    }
                }

                for (var i = 0; i < 60*60; i++)
                    await Task.Delay(1000);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private static string GetNameByUserAndPath(string userId, string path)
        {
            return $"{userId}__{Path.GetFileName(path)}";
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ATech.AiServices.Contract;
using ATech.Audio;
using ATech.Audio.AudioProcessing;
using ATech.Audio.AudioProcessing.Ffmpeg;
using ATech.Audio.AudioSegmentation;
using ATech.Audio.Interfaces;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.Common.Dal.Models;
using ATech.Common.DialogMetrics;
using ATech.Configuration;
using ATech.DialogProcessing.DialogProcessing;
using ATech.Logging.Interfaces;
using ATech.Statistics.Metrics;
using ATech.TextAnalysis.TextDataAnalysisDownloading;
using CallProcessor.Contract;
using CallProcessor.Models;

namespace CallProcessor.Services
{
    public class AudioProcessingService : IAudioProcessingService
    {
        private ILogger _logger;

        private readonly ISpeechAnalysisAiService _speechAnalysisAiService;

        public AudioProcessingService(ISpeechAnalysisAiService speechAnalysisAiService)
        {
            this._speechAnalysisAiService = speechAnalysisAiService;
        }

        public void Init()
        {
            if (_logger != null) return;
            _logger = MainProcessingConfiguration.Instance.Logger;
            _logger.LogMessage("Init AudioProcessingService");
        }

        public async Task<AnswerModel> ProcessAudio(string filePath, InputSettings settings, ApiType apiType, string secondFilePath)
        {
            IAudioSegmentor segmentor = null;
            IAudioDescriptor audioDescriptor = null;
            AudioDialogDescriptor dialog = null;
            AudioDialogDetails dialogDetails = null;
            AudioFeatureExtractingDescriptor audioFeatureExtractingDescriptor = null;
            TextCalculatedMetrics calculatedMetrics = null;
            IAudioFileHandler audioFileHandler = new FfmpegFileHandler();
            IAudioConvertingComposite audioConvertingComposite = new FfmpegConvertingComposite(audioFileHandler);
            string clientSpeechCategory = null;
            var processed = false;

            var sw = new Stopwatch();
            sw.Start();

            try
            {
                var configsAndVocabsStorage = new DialogsSourceDataDownloaderFromJsonStorage();
                configsAndVocabsStorage.DownloadConfigs();
                var textDialogProcessing = new TextDialogProcessing(null);
                textDialogProcessing.SetStorage(configsAndVocabsStorage);
                
                _logger.LogMessage("Start Processing");

                
                audioDescriptor = new AudioDescriptor();
                audioDescriptor.Create(audioConvertingComposite, filePath, secondFilePath);

                if (settings.GiveStatRegulation)
                {
                    _logger.LogMessage("Segmentation");
                    segmentor = new CropAudioSegmentor();
                    audioDescriptor = audioDescriptor.Segment(segmentor);
                }

                if (settings.GiveTextFromAudio)
                {
                    var duration = await audioFileHandler.GetAudioDurationInSeconds(filePath);
                    if (duration < 5)
                        return new AnswerModel("Error file too short");
                }

                Task gettingVoiceFeature = null;

                if (settings.GiveMainVoiceFeatures)
                {
                    audioFeatureExtractingDescriptor = new AudioFeatureExtractingDescriptor
                    {
                        AudioConvertingComposite = audioConvertingComposite,
                        AudioFileHandler = audioFileHandler,
                        AudioFileName = Path.GetFileName(audioDescriptor.MainAudioFilePath),
                        SecondAudioFileName = audioDescriptor.SecondMainAudioFilePath == null
                            ? null
                            : Path.GetFileName(audioDescriptor.SecondMainAudioFilePath),
                        Settings = settings,
                        SpeechAnalysisAiService = _speechAnalysisAiService
                    };

                    gettingVoiceFeature = Task.Run(() => audioFeatureExtractingDescriptor.GetVoiceFeaturesFromAudio());
                }

                await audioDescriptor.GetDialogFromFullFile(settings.ChannelsCount, settings.RecognizerType, settings.Language, !settings.GiveTextFromAudio, apiType == ApiType.UseDesk).ConfigureAwait(false);


                if (settings.GiveStatRegulation)
                    audioDescriptor.CalculateRoleDuration(segmentor);

                dialog = (AudioDialogDescriptor) audioDescriptor.GenerateDialog();

                if (dialog == null && !settings.GiveMainVoiceFeatures)
                    return new AnswerModel("Error speech recognition");

                if (settings.GiveStatRegulation)
                    dialogDetails = (AudioDialogDetails) audioDescriptor.AudioDialogDetails;

                if (dialog != null)
                    calculatedMetrics = (TextCalculatedMetrics) textDialogProcessing.ProcessDialog(dialog, configsAndVocabsStorage);

                if (settings.GiveTextCategory)
                    clientSpeechCategory = await GetClientSpeechCategory(dialog).ConfigureAwait(false);

                if (settings.GiveMainVoiceFeatures)
                {
                    gettingVoiceFeature?.Wait();
                }

                processed = true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"AudioDialogProcessing {filePath}");
            }
            finally
            {
                try
                {
                    segmentor?.RemoveFragmentsDir();
                    audioDescriptor?.ClearTmpDirs();
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "super-try-catch-fucked-up");
                }
            }

            if (!processed)
                return new AnswerModel("Error text processing");

            var result = new AnswerModel
            {
                AudioDialogDetails = dialogDetails,
                AudioDialogDescriptor = dialog,
                TextCalculatedMetrics = calculatedMetrics,
                FragmentFeatures = audioFeatureExtractingDescriptor?.FragmentFeatures,
                TextCategory = clientSpeechCategory
            };

            sw.Stop();

            result.ProcessingTimeInSeconds = (int) sw.ElapsedMilliseconds / 1000;

            return result;
        }

        private async Task<string> GetClientSpeechCategory(IDialogDescriptor descriptor)
        {
            var phrases = (from descriptorPhrase in descriptor.Phrases where descriptorPhrase.AuthorId == "Client" select descriptorPhrase.Text).ToList();
            var fullText = string.Join("", phrases);
            return await _speechAnalysisAiService.ClassifyTextFromSpeech(fullText).ConfigureAwait(false);
        }
    }
}

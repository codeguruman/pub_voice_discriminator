﻿using System;
using System.IO;
using System.Web;
using System.Net.Http;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using CallProcessor.Contract;

namespace CallProcessor.Services
{
    public class TgNotificationService : INotificationService
    {
        private static HttpClient _httpClient;

        private readonly ILogger _logger;

        public TgNotificationService()
        {
            _logger = MainProcessingConfiguration.Instance.Logger;
            _httpClient ??= new HttpClient { BaseAddress = new Uri(MainProcessingConfiguration.TelegramBotUrl) };
        }

        public async Task SendNotification(AnswerModel model, string apikey, string taskId)
        {
            try
            {
                var alarmerKey = MainProcessingConfiguration.Instance.TgAlarmerKey;
                if (string.IsNullOrEmpty(alarmerKey))
                {
                    return;
                }

                var message = GenerateMessage(model, apikey, taskId);
                if (string.IsNullOrEmpty(message))
                    return;

                var answer = await _httpClient
                    .GetAsync($"?key={MainProcessingConfiguration.Instance.TgAlarmerKey}&message={message}");
                await answer.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "TgNotificationService SendNotification");
            }
        }

        private string GenerateMessage(AnswerModel model, string apikey, string taskId)
        {
            var modelsMap = MainProcessingConfiguration.Instance.CallTouchObjectsMap;
            if (modelsMap == null)
            {
                return null;
            }

            var filename = Path.GetFileName(model.AudioDialogDescriptor.AudioFileName);
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.AudioDialogDescriptor.AudioFileName);

            _logger.LogMessage($"Generate notification about {filename}");
            if (filename != null && fileNameWithoutExtension != null)
            {
                filename = filename.Replace(apikey + "__", "");
                fileNameWithoutExtension = fileNameWithoutExtension.Replace(apikey + "__", "");
                var parts = fileNameWithoutExtension.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length >= 4)
                {
                    var id = parts[0];
                    var from = parts[1];
                    var to = parts[2];
                    var modelCode = parts[3];

                    var modelName = modelsMap.ContainsKey(modelCode) ? modelsMap[modelCode] : "";
                    var taskViewLink = CreateTaskViewLink(taskId, filename, apikey);

                    var message =
                        $"Suspected conflict in a phone call with id {id} from {from} to {to}, brand {modelName}. " + HttpUtility.UrlEncode(taskViewLink);
                    return message;
                }
            }
            
            return "";
        }

        private static string CreateTaskViewLink(string taskId, string fileName, string apiKey)
        {
            return $"{MainProcessingConfiguration.Instance.NotificationCallUrl}{taskId}?audiolink=" +
                   HttpUtility.UrlEncode($"{MainProcessingConfiguration.Instance.FileLoadingControllerUrl}?filename={fileName}&apikey={apiKey}");
        }
    }
}

using System;
using System.IO;
using System.Reflection;
using ATech.AiServices.Contract;
using ATech.AiServices.Services;
using ATech.Common.Dal;
using ATech.Common.Dal.Repositiory;
using ATech.Common.ElasticSearch.Extensions;
using ATech.Common.ElasticSearch.Repository;
using ATech.Configuration;
using CallProcessor.Collectors;
using CallProcessor.Contract;
using CallProcessor.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace CallProcessor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            MainProcessingConfiguration.Instance.InitFromAppSettings(Configuration);
            StatisticsAnalysisConfiguration.Instance.InitFromCode();

            services.AddCors();
            services.AddControllers();
            services.AddMvc();
            services.AddElasticsearch(Configuration);
            services.Configure<Settings>(options =>
            {
                options.ConnectionString = MainProcessingConfiguration.Instance.MongoDbConnectionString;
                options.Database = MainProcessingConfiguration.Instance.MongoDbName;
            });

            services.AddTransient<IElasticSearchRepository, ElasticSearchRepository>();
            services.AddTransient<IProcessingTaskRepository, ProcessingTaskRepository>();
            services.AddSingleton<IAudioProcessingService, AudioProcessingService>();
            services.AddSingleton<INotificationService, TgNotificationService>();
            services.AddTransient<ISpeechAnalysisAiService, SpeechAnalysisAiService>();
            services.AddSingleton<ITasksCollector, TasksCollector>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Speech Works API",
                    Description = "Web API for audio files processing. Supports mono and stereo channel files in mp3 and wav format.",
                    Contact = new OpenApiContact
                    {
                        Name = "Alexandr Smirnov",
                        Email = "alessander.smirnov@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/alessander-smirnov/"),
                    },
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Speech Works API");
                c.RoutePrefix = string.Empty;
                c.DocumentTitle = "Speech Works API";
            });

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCors(
                options => options.SetIsOriginAllowed(x => _ = true).AllowAnyMethod().AllowAnyHeader().AllowCredentials()
            );

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

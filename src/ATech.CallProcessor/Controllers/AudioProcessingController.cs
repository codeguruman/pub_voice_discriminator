﻿#nullable enable
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;
using ATech.Common.ElasticSearch.Repository;
using ATech.Common.Licensing;
using ATech.Configuration;
using ATech.Logging;
using ATech.Logging.Interfaces;
using ATech.Logging.Loggers;
using CallProcessor.Collectors;
using CallProcessor.Contract;
using CallProcessor.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CallProcessor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AudioProcessingController : ControllerBase
    {
        private readonly IAudioProcessingService audioProcessingService;

        private static string? _tmpDir;

        private static ILogger _logger = null!;

        private readonly ITasksCollector _collector;

        private readonly IElasticSearchRepository _elasticSearchRepository;

        public AudioProcessingController(IAudioProcessingService service, ITasksCollector collector, IElasticSearchRepository elasticSearchRepository, INotificationService notificationService)
        {
            if (_logger == null)
            {
                
                _logger = LoggerFactory.GetLogger(typeof(FileLogger));
                _tmpDir = MainProcessingConfiguration.TmpAudioDir;
                if (!Directory.Exists(_tmpDir))
                    Directory.CreateDirectory(_tmpDir);
            }

            audioProcessingService = service;
            audioProcessingService.Init();
            _collector = collector;
            _collector.Init(audioProcessingService);
            _elasticSearchRepository = elasticSearchRepository;
        }

        /// <summary>
        /// Get text from audio file inside body
        /// </summary>
        /// <param name="file">sound file</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <returns>List of phrases</returns>
        [Route("GetTextFromSpeech")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel[]>> GetTextFromSpeech(List<IFormFile> file, string apikey, string? channelsCount)
        {
            _logger.LogMessage($"Using controller GetTextFromSpeech {DateTime.Now}");
            var settings = new InputSettings();
            if (int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = false;
            settings.GiveTextFromAudio = true;

            var files = HttpContext.Request.Form.Files;
            if (files == null)
                return new NotFoundResult();

            if (files.Count == 0)
                return new NotFoundResult();

            var answerModels = new List<AnswerModel>();
            var filesCollection = await GetFilesCollectionFromContent(files).ConfigureAwait(false);
            foreach (var path in filesCollection)
            {
                var answerModel = await audioProcessingService.ProcessAudio(path, settings, ApiType.UseDesk).ConfigureAwait(false);
                answerModels.Add(answerModel);
            }

            ClearFiles(filesCollection);

            return GetVoiceFeaturesObject(answerModels);
        }

        /// <summary>
        /// Get voice features as tempo, emotions, etc from audio file inside body 
        /// </summary>
        /// <param name="file">sound file</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <returns>List of fragments with features </returns>
        [Route("GetVoiceFeatures")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel[]>> GetVoiceFeatures(List<IFormFile> file, string apikey, string? channelsCount)
        {
            _logger.LogMessage($"Using controller GetVoiceFeatures {DateTime.Now}");
            var settings = new InputSettings();
            if(int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;
            
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();


            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = true;
            settings.GiveTextFromAudio = false;

            var files = HttpContext.Request.Form.Files;
            if (files == null)
                return new BadRequestResult();

            if (files.Count == 0)
                return new BadRequestResult();

            var answerModels = new List<AnswerModel>();
            var filesCollection = await GetFilesCollectionFromContent(files).ConfigureAwait(false);
            foreach (var path in filesCollection)
            {
                var answerModel = await audioProcessingService.ProcessAudio(path, settings, ApiType.UseDesk).ConfigureAwait(false);
                answerModels.Add(answerModel);
            }

            ClearFiles(filesCollection);

            return GetVoiceFeaturesObject(answerModels);
        }

        /// <summary>
        /// Get voice features as tempo, emotions, etc and recognized text from audio file inside body 
        /// </summary>
        /// <param name="file">sound file</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <param name="language">language of a speaker</param>
        /// <returns>List of fragments with features and recognized text </returns>
        [Route("GetTextAndVoiceFeatures")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel[]>> GetTextAndVoiceFeatures(List<IFormFile> file, string apikey, string? channelsCount, string? language)
        {
            _logger.LogMessage($"Using controller GetVoiceFeatures {DateTime.Now}");
            var settings = new InputSettings();
            if (int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = true;
            settings.GiveTextFromAudio = true;

            if (language != null && language == "en")
                settings.Language = "en";

            var apiType = settings.Language == "en" ? ApiType.Default : ApiType.UseDesk;

            var files = HttpContext.Request.Form.Files;
            if (files == null)
                return new BadRequestResult();

            if (files.Count == 0)
                return new BadRequestResult();

            var answerModels = new List<AnswerModel>();
            var filesCollection = await GetFilesCollectionFromContent(files).ConfigureAwait(false);
            foreach (var path in filesCollection)
            {
                var answerModel = await audioProcessingService.ProcessAudio(path, settings, apiType).ConfigureAwait(false);
                answerModels.Add(answerModel);
            }

            ClearFiles(filesCollection);

            return GetVoiceFeaturesObject(answerModels);
        }

        /// <summary>
        /// Create stt task, it downloads audio file, recognizes speech in it asynchronously and send result to special link
        /// </summary>
        /// <param name="task">Task with loading link</param>
        /// <param name="apikey">client key on a service</param>
        [Route("CreateSttTask")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult> CreateSttTask([FromBody] RecognitionTask task, string apikey) // API для usedesk
        {
            _logger.LogMessage($"Using controller CreateSttTask {DateTime.Now}");

            var path = await LoadFile(task.File_url).ConfigureAwait(false);
            if (path == null)
                return new NotFoundResult();

            var settings = new InputSettings {ChannelsCount = 1, GiveStatRegulation = false, GiveTextFromAudio = true};

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var processingTask = new ProcessingTask { RecognitionTask = task, InputSettings = settings, FilePath = path, ApiType = ApiType.UseDesk };
            _collector.AddTask(processingTask);

            return new OkResult();
        }

        /// <summary>
        /// Create stt task, it downloads audio file, recognizes text asynchronously and returns recognition task id to get result of this task later
        /// </summary>
        /// <param name="task">Task with loading link</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <param name="useYandex">use yandex for stt</param>
        /// <returns> { "id" : id } </returns>
        [Route("CreateTask")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult> CreateTask([FromBody] RecognitionTask task, string apikey, string? channelsCount, bool useYandex)
        {
            _logger.LogMessage($"Using controller CreateTask {DateTime.Now}");

            var path = await LoadFile(task.File_url).ConfigureAwait(false);
            if (path == null)
                return new NotFoundResult();

            var settings = new InputSettings();
            if (int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = false;
            settings.GiveTextFromAudio = true;

            if (useYandex)
                settings.RecognizerType = RecognizerType.Yandex;

            var processingTask = new ProcessingTask { RecognitionTask = task, InputSettings = settings, FilePath = path, ApiType = ApiType.Default };

            var id = _collector.AddTask(processingTask);

            return GetIdResult(id);
        }

        /// <summary>
        /// Create stt task with 2 files with one voice in each channel, it downloads both audio files, recognizes text asynchronously and returns recognition task id to get result of this task later
        /// </summary>
        /// <param name="task">Task with loading link</param>
        /// <param name="apikey">client key on a service</param>
        /// <returns> { "id" : id } </returns>
        [Route("CreateTaskTwoFiles")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult> CreateTaskTwoFiles([FromBody] RecognitionTaskTwoFiles task, string apikey)
        {
            _logger.LogMessage($"Using controller CreateTask {DateTime.Now}");

            var pathLeft = await LoadFile(task.File_url_Left).ConfigureAwait(false);
            if (pathLeft == null)
                return new NotFoundResult();

            var pathRight = await LoadFile(task.File_url_Right).ConfigureAwait(false);
            if (pathRight == null)
                return new NotFoundResult();

            var settings = new InputSettings();
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = false;
            settings.GiveTextFromAudio = true;
            settings.RecognizerType = RecognizerType.Kaldi;

            var processingTask = new ProcessingTask { RecognitionTask = new RecognitionTask(), InputSettings = settings, FilePath = pathLeft, SecondFilePath = pathRight, ApiType = ApiType.Default };

            var id = _collector.AddTask(processingTask);

            return GetIdResult(id);
        }

        /// <summary>
        /// Create voice features recognition task, it downloads audio file, recognizes voice features asynchronously and returns recognition task id to get result of this task later
        /// </summary>
        /// <param name="task">Task with loading link</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <returns> { "id" : id } </returns>
        [Route("CreateTaskVoiceFeatures")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult> CreateTaskVoiceFeatures([FromBody] RecognitionTask task, string apikey, string? channelsCount)
        {
            _logger.LogMessage($"Using controller CreateTask {DateTime.Now}");

            var path = await LoadFile(task.File_url).ConfigureAwait(false);
            if (path == null)
                return new NotFoundResult();

            var settings = new InputSettings();
            if (int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = true;
            settings.GiveTextFromAudio = false;

            var processingTask = new ProcessingTask { RecognitionTask = task, InputSettings = settings, FilePath = path, ApiType = ApiType.Default };

            var id = _collector.AddTask(processingTask);

            return GetIdResult(id);
        }

        /// <summary>
        /// Create voice features and text recognition task, it downloads audio file, recognizes voice features and text asynchronously and returns recognition task id to get result of this task later
        /// </summary>
        /// <param name="task">Task with loading link</param>
        /// <param name="apikey">client key on a service</param>
        /// <param name="channelsCount">channels count in a file record</param>
        /// <param name="language">language of a speaker</param>
        /// <returns> { "id" : id } </returns>
        [Route("CreateTaskSttAndVoiceFeatures")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [Produces("application/json")]
        [Consumes(MediaTypeNames.Application.Json)]
        public async Task<ActionResult> CreateTaskSttAndVoiceFeatures([FromBody] RecognitionTask task, string apikey, string? channelsCount, string? language)
        {
            _logger.LogMessage($"Using controller CreateTask {DateTime.Now}");

            var path = await LoadFile(task.File_url).ConfigureAwait(false);
            if (path == null)
                return new NotFoundResult();

            var settings = new InputSettings();
            if (int.TryParse(channelsCount, out var channels))
                settings.ChannelsCount = channels;

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            settings.GiveStatRegulation = false;
            settings.GiveMainVoiceFeatures = true;
            settings.GiveTextFromAudio = true;

            if (language != null && language == "en")
                settings.Language = "en";

            var processingTask = new ProcessingTask { RecognitionTask = task, InputSettings = settings, FilePath = path, ApiType = ApiType.Default };

            var id = _collector.AddTask(processingTask);

            return GetIdResult(id);
        }

        /// <summary>
        /// Get result by task id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of fragments with features or recognized text or both</returns>
        [Route("GetTaskResult")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel>> GetTaskResult(string id)
        {
            _logger.LogMessage($"Using controller GetTaskResult {DateTime.Now}");

            if (string.IsNullOrEmpty(id))
                return new BadRequestResult();

            var result = await _collector.GetTaskResult(id);
            return result == null ? new NotFoundResult() : GetVoiceFeaturesObject(result);
        }

        /// <summary>
        /// Send notification about call
        /// </summary>
        /// <param name="apikey">client key on a service</param>
        /// <param name="id">task id</param>
        /// <returns>List of fragments with features or recognized text or both</returns>
        [Route("SendNotification")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel>> SendNotification(string apikey, string id)
        {
            _logger.LogMessage($"Using controller SendNotification {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            if (string.IsNullOrEmpty(id))
                return new BadRequestResult();

            var result = await _collector.SendNotification(apikey, id);
            if (!result)
                return new BadRequestResult();

            return new OkResult();
        }

        /// <summary>
        /// Clear error tasks
        /// </summary>
        /// <param name="apikey">client key on a service</param>
        [Route("ClearErrorTasks")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel>> ClearErrorTasks(string apikey)
        {
            _logger.LogMessage($"Using controller ClearErrorTasks {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var result = await _collector.ClearErrorTasks();
            if (!result)
                return new BadRequestResult();

            return new OkResult();
        }

        /// <summary>
        /// Get all data of handled tasks
        /// </summary>
        /// <param name="apikey">Api key</param>
        /// <returns></returns>
        [Route("GetAllTasks")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<AnswerModel[]>> GetAllTasks(string apikey)
        {
            _logger.LogMessage($"Using controller GetAllTasks {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var taskResults = await _collector.GetTaskResults();
            if (taskResults == null)
                return new NotFoundResult();

            var result = new List<AnswerModel>();
            foreach (var item in taskResults)
            {
                result.Add(item.AnswerModel);
            }

            return GetVoiceFeaturesObject(result);
        }

        /// <summary>
        /// Direct download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="apikey"></param>
        /// <returns></returns>
        [Route("Download")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public ActionResult DownloadDocument(string filename, string apikey)
        {
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var dir = MainProcessingConfiguration.MainLocalAudioDir;
            var filePath = Path.Combine(dir, apikey, filename);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            Response.Headers.Add("Accept-Ranges", "bytes");
            return File(fileBytes, "application/force-download", filename);
        }

        /// <summary>
        /// Say server I'm alive
        /// </summary>
        /// <param name="apikey"></param>
        /// <param name="pubkey"></param>
        /// <returns></returns>
        [Route("HeartBeat")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [HttpGet]
        public ActionResult HeartBeat(string apikey, string pubkey)
        {
            var result_api_key = "";
            if (UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                result_api_key = apikey;

            var message_for_client = Encryptor.GetHash(pubkey, result_api_key);

            return new ObjectResult(message_for_client);
        }

        /// <summary>
        /// Get Processing States
        /// </summary>
        /// <param name="apikey">Api Key</param>
        /// <returns>Dictionary with (k,v) = (filename, taskid(or error))</returns>
        [Route("GetProcessingStates")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<ProcessingTaskState[]>> GetProcessingStates(string apikey)
        {
            _logger.LogMessage($"Using controller GetProcessingStates {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var result = await _collector.GetTasksStates(apikey);
            if (result == null)
            {
                return new NotFoundResult();
            }
            return result;
        }

        /// <summary>
        /// Index Elastic Search Data(call once to load db data)
        /// </summary>
        /// <param name="apikey">Api Key</param>
        [Route("IndexElasticSearchData")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult> IndexElasticSearchData(string apikey)
        {
            _logger.LogMessage($"Using controller IndexElasticSearchData {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var processingTaskStates = await _collector.GetTasksStates(apikey);
            if (processingTaskStates == null)
            {
                return new BadRequestResult();
            }

            foreach (var processingTaskState in processingTaskStates)
            {
                try
                {
                    await _elasticSearchRepository.AddAsync(processingTaskState);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "AudioProcessingController IndexElasticSearchData");
                }
            }

            return new OkResult();
        }

        /// <summary>
        /// ElasticSearch Find Documents By Query
        /// </summary>
        /// <param name="apikey">Api Key</param>
        /// <param name="query">Search query</param>
        [Route("ElasticSearchFindDocumentsByQuery")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<ProcessingTaskState[]>> ElasticSearchFindDocumentsByQuery(string apikey, string query)
        {
            _logger.LogMessage($"Using controller ElasticSearchFindDocumentsByQuery {DateTime.Now}");

            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            var result = await _elasticSearchRepository.FindMatch(query);
            return new ObjectResult(result);
        }

        [Route("ClearLog")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult ClearLog(string apikey, string logtype)
        {
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            if (logtype == "error")
            {
                _logger.CleanLogError();
            }

            if (logtype == "message")
            {
                _logger.CleanLogMessage();
            }

            return new OkResult();
        }

        [Route("GetLog")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<string> GetLog(string apikey, string logtype)
        {
            if (!UsersCollector.Instance.CheckUserAndUpdateStat(apikey))
                return new NotFoundResult();

            if (logtype == "error")
            {
                return _logger.GetErrorLog();
            }

            if (logtype == "message")
            {
                return _logger.GetMessageLog();
            }

            return new NotFoundResult();
        }

        private async Task<List<string>> GetFilesCollectionFromContent(IFormFileCollection files)
        {
            var result = new List<string>();

            foreach (var file in files)
            {
                result.Add(await ReadContentFile(file));
            }

            return result;
        }

        private static async Task<string> LoadFile(string url)
        {
            var extension = GetFileExtensionFromUrl(url).EndsWith(".wav") ? ".wav" : ".mp3";
            var resultFilePath = Path.Combine(_tmpDir, Guid.NewGuid() + extension);
            var client = new WebClient();
            try
            {
                await client.DownloadFileTaskAsync(url, resultFilePath).ConfigureAwait(false);
            }
            catch (Exception)
            {
                resultFilePath = null;
            }
            client.Dispose();
            
            return resultFilePath!;
        }

        private static string GetFileExtensionFromUrl(string url)
        {
            url = url.Split('?')[0];
            url = url.Split('/').Last();
            return url.Contains('.') ? url.Substring(url.LastIndexOf('.')) : "";
        }

        private static async Task<string> ReadContentFile(IFormFile file)
        {
            var resultFilePath = Path.Combine(_tmpDir, Guid.NewGuid() + Path.GetExtension(file.FileName));
            try
            {
                await using var inputStream = new FileStream(resultFilePath, FileMode.Create);
                // read file to stream
                await file.CopyToAsync(inputStream).ConfigureAwait(false);
                // stream to byte array
                var array = new byte[inputStream.Length];
                inputStream.Seek(0, SeekOrigin.Begin);
                await inputStream.ReadAsync(array, 0, array.Length).ConfigureAwait(false);
                await inputStream.DisposeAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CallProcessingController ReadContentFile");
                return null!;
            }

            return resultFilePath;
        }

        private static void ClearFiles(List<string> filesCollection)
        {
            foreach (var filePath in filesCollection)
            {
                if (System.IO.File.Exists(filePath))
                    System.IO.File.Delete(filePath);
            }
        }

        private static ActionResult GetVoiceFeaturesObject(List<AnswerModel> answerModels)
        {
            var result = JsonConvert.SerializeObject(answerModels, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            return new ObjectResult(result);
        }

        private static ActionResult GetVoiceFeaturesObject(AnswerModel answerModel)
        {
            var result = JsonConvert.SerializeObject(answerModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            return new ObjectResult(result);
        }

        private static ActionResult GetIdResult(string id)
        {
            var result = JsonConvert.SerializeObject(new
            {
                id
            }, Formatting.Indented, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            return new ObjectResult(result);
        }

        //        [Route("RecognizeBatchFromDir")]
        //        [HttpGet]
        //#pragma warning disable 1998
        //        public async Task<ActionResult> ProcessMp3AudioBatchFromDirectory(List<IFormFile> data)
        //#pragma warning restore 1998
        //        {
        //            //EmailSender.Send("Using voice discriminator detected", "using controller RecognizeBatchFromDir");
        //            _logger.LogMessage($"Using controller RecognizeBatchFromDir {DateTime.Now}");
        //            Console.WriteLine("Request accepted");
        //            var settings = new InputSettings();
        //            if (int.TryParse(HttpContext.Request.Query["channelscount"], out var channels))
        //                settings.ChannelsCount = channels;


        //            var filePaths = Directory.GetFiles(@"D:\SDA_data\ContactCenter");

        //            var answerModels = new List<AnswerModel>();
        //            var sync = new object();
        //            var readyText = System.IO.File.ReadAllText(@"C:\Users\Alexandr\Desktop\result.txt");

        //            Parallel.For(0, filePaths.Length, new ParallelOptions() { MaxDegreeOfParallelism = 4 }, async (i) =>
        //            {
        //                try
        //                {
        //                    var name = Path.GetFileName(filePaths[i]);
        //                    if (!readyText.Contains(name))
        //                    {
        //                        var answerModel = await _audioProcessingService.ProcessAudio(filePaths[i], settings);
        //                        lock (sync)
        //                        {
        //                            System.IO.File.AppendAllLines(@"C:\Users\Alexandr\Desktop\result.txt",
        //                                new[]
        //                                {
        //                                $"<{i + 1}><{name}><{((CallProcessingAnswerModel) answerModel).AudioDialogDescriptor.FullText}>"
        //                                });

        //                            answerModels.Add(answerModel);
        //                        }
        //                    }
        //                }
        //                catch (Exception)
        //                {
        //                    // ignored
        //                }
        //            });


        //            return new JsonResult(answerModels);
        //        }
    }
}

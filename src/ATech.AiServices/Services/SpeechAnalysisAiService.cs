﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using ATech.AiServices.Contract;
using ATech.Configuration;
using ATech.Logging.Interfaces;

namespace ATech.AiServices.Services
{
    public class SpeechAnalysisAiService : ISpeechAnalysisAiService
    {
        private static TimeSpan _timeout;

        private static Uri _uri;

        private readonly ILogger _logger;

        public SpeechAnalysisAiService()
        {
            _logger = MainProcessingConfiguration.Instance.Logger;
            _uri = new Uri(MainProcessingConfiguration.Instance.AiSpeechAnalysisBaseUrl);
            _timeout = TimeSpan.FromMinutes(5);
        }


        public async Task<string> ClassifyTextFromSpeech(string text)
        {
            string result;
            try
            {
                using var httpClient = new HttpClient {BaseAddress = _uri, Timeout = _timeout};
                var answer = await httpClient.GetAsync($"classifyappeal?text={text}");
                result = await answer.Content.ReadAsStringAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SpeechAnalysisAiService ClassifyTextFromSpeech");
                result = "{}";
            }

            return result;
        }

        public async Task<string> GetSimpleVoiceFeaturesFromFile(string filePath)
        {
            return await PostAttachment(filePath, true);
        }

        public async Task<string> GetVoiceFeaturesFromFile(string filePath)
        {
            return await PostAttachment(filePath, false);
        }

        public async Task<string> PostAttachment(string filePath, bool isSimpleFeatures)
        {
            var result = "{}";

            try
            {
                using var httpClient = new HttpClient { BaseAddress = _uri, Timeout = _timeout };
                await using var file = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                var streamContent = new StreamContent(file);

                using var content = new MultipartFormDataContent { { streamContent, "file", "_wav.wav" } };

                using var response = await httpClient.PostAsync(new Uri("classifysoundmood", UriKind.Relative), content).ConfigureAwait(false);
                result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "SpeechAnalysisAiService PostAttachment");
                return result;
            }
            return result;
        }
    }
}

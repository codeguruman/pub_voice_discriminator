﻿using System.Threading.Tasks;

namespace ATech.AiServices.Contract
{
    public interface ISpeechAnalysisAiService
    {
        Task<string> GetVoiceFeaturesFromFile(string filePath);
        Task<string> GetSimpleVoiceFeaturesFromFile(string filePath);
        Task<string> ClassifyTextFromSpeech(string text);
    }
}

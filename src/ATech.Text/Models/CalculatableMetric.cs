﻿using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.Models
{
    public class CalculatableMetric : ICalculatableMetric
    {
        public string Type { get; set; }
        public string Description { get; set; }
    }
}

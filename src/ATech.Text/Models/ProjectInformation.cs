﻿using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.Models
{
    public class ProjectInformation : IProjectInformation
    {
        public string ProjectId { get; set; }

        public string Description { get; set; }
    }
}

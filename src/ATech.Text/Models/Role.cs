﻿using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.Models
{
    public class Role : IRole
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
    }
}

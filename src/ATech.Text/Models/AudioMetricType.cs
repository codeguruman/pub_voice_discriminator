﻿namespace ATech.TextAnalysis.Models
{
    public enum AudioMetricType
    {
        OperatorDialogsCount,
        MiddleAudioDurationInSeconds,
        MiddleAudioDurationInSecondsByOperator,
        MiddleSilenceDurationInSeconds,
        MiddleSilenceDurationInSecondsByOperator,
        MiddleMaxSilenceIntervalDurationInSeconds,
        MiddleMaxSilenceIntervalDurationInSecondsByOperator,
        CountIncreasedMaxAllowedIntervalSilenceByOperator,
        MiddleInterruptsCount,
        MiddleInterruptsCountByOperator,
        MiddleDurationOneTimeSpeech,
        MiddleDurationOneTimeSpeechByOperator,
        MiddleDurationClientSpeechInPercents,
        MiddleDurationOperatorSpeechInPercents
    }
}

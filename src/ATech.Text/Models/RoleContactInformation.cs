﻿using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.Models
{
    public class RoleContactInformation : IRoleContactInformation
    {
        public string Name { get; set; }
        public string RoleType { get; set; }
        public string RoleId { get; set; }
        public string ProjectId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
    }
}

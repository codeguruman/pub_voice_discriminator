﻿namespace ATech.TextAnalysis.Models
{
    public enum TextMetricType
    {
        OperatorDialogsCount,
        LoyalityClientsByEveryProject,
        LoyalityClientsByEveryOperator,
        MiddleLoyalityClients,
        StandardDeviationLoyalityClients,
        MedianLoyalityClients,
        CountKeywordsEveryIndicator,
        ProjectCountKeywordsEveryIndicator,
        OperatorCountKeywordsEveryIndicator,
        OperatorPhrasesMistakes
    }
}

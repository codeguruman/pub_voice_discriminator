﻿namespace ATech.TextAnalysis.Models
{
    public enum DialogChannel
    {
        Ticket,
        Email,
        Vk,
        Instagram,
        Audio,
        NoMatter
    }
}

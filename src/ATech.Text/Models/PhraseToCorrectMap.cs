﻿using System.Collections.Generic;
using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.Models
{
    public class PhraseToCorrectMap : IPhraseToCorrectMap
    {
        public PhraseToCorrectMap()
        {
            Map = new Dictionary<string, List<string>>();
        }
        public Dictionary<string, List<string>> Map { get; set; }
    }
}

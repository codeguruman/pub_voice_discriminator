﻿using System;
using System.Collections.Generic;
using ATech.Common.CommonModels;
using ATech.Common.Dal.Models;

namespace ATech.TextAnalysis.Models
{
    public class DeserializableDescriptor
    {
        public string DialogId { get; set; }
        public string OperatorId { get; set; }
        public string ClientId { get; set; }
        public string FullText { get; set; }
        public DateTime Date { get; set; }
        public string Channel { get; set; }
        public List<AudioDialogPhrase> Phrases { get; set; }

        public IDialogDescriptor Convert()
        {
            IDialogDescriptor descriptor = new DialogDescriptor();
            descriptor.DialogId = DialogId;
            descriptor.Phrases = new List<AudioDialogPhrase>();
            foreach (var phrase in Phrases)
            {
                descriptor.Phrases.Add(phrase);
            }

            descriptor.Channel = Channel;
            descriptor.ClientId = ClientId;
            descriptor.OperatorId = OperatorId;
            descriptor.Date = Date;
            descriptor.FullText = FullText;
            return descriptor;
        }
    }
}

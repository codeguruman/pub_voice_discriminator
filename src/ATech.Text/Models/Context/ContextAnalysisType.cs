﻿using System.Collections.Generic;

namespace ATech.TextAnalysis.Models.Context
{
    public class ContextAnalysisType : IContextAnalysisType
    {
        public string Type { get; set; }
        public string Role { get; set; }
        public string ContextTypeName { get; set; }
        public List<string> KeyPhrases { get; set; }
    }
}

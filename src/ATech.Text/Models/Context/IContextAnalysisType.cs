﻿using System.Collections.Generic;

namespace ATech.TextAnalysis.Models.Context
{
    public interface IContextAnalysisType
    {
        string Type { get; set; }
        string Role { get; set; }
        string ContextTypeName { get; set; }
        List<string> KeyPhrases { get; set; }
    }
}

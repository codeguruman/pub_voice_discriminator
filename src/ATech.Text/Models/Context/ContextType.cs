﻿namespace ATech.TextAnalysis.Models.Context
{
    public enum ContextType
    {
        NotLoyalCustomers,
        ConflictDialogs,
        ForbiddenPhrases,
        UnwantedPhrases,
        CustomerProblems,
        UnfulfilledPromisesByCompany,
        BusinessPointsGrowth,
        MostInterestingProducts,
        СustomerSatisfactionRates,
        BadOperatorUnderstanding,
        UserKeyWords,
        UserRemarks
    }
}

﻿using System.Collections.Generic;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.TextAnalysis.Interfaces;
using ATech.TextAnalysis.Models.Context;

namespace ATech.TextAnalysis.Models
{
    public class DialogsData : IDialogsData
    {
        public List<IDialogDescriptor> Dialogs { get; set; }
        public List<IAudioDialogDetails> AudioDialogDetails { get; set; }
        public List<IRoleContactInformation> Roles { get; set; }
        public List<IContextAnalysisType> Contexts { get; set; }
        public List<ICalculatableMetric> TextMetrics { get; set; }
        public List<ICalculatableMetric> AudioMetrics { get; set; }
        public List<IProjectInformation> Projects { get; set; }
        public IPhraseToCorrectMap PhraseToCorrectMap { get; set; }
    }
}

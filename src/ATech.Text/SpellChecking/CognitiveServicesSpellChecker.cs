﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json.Linq;

namespace ATech.TextAnalysis.SpellChecking
{
    public class CognitiveServicesSpellChecker : ISpellChecker
    {
        private readonly string _url;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        private readonly HttpClient _client;

        public CognitiveServicesSpellChecker()
        {
            _url = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.SpellCheckUrl;
            var apiKey = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.SpellCheckApiKey;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", apiKey);
        }

        public string Process(string phrase, out HashSet<string> mistakes)
        {
            mistakes = new HashSet<string>();
            try
            {
                var values = new List<KeyValuePair<string, string>> {new KeyValuePair<string, string>("text", phrase)};
                var resultStr = Post(values);
                if (resultStr != null)
                    return ParseSpellSheckResult(resultStr, phrase, out mistakes);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesSpellChecker SpellCheck");
            }

            return null;
        }

        private string Post(IEnumerable<KeyValuePair<string, string>> values)
        {
            try
            {
                HttpResponseMessage response;
                using (var content = new FormUrlEncodedContent(values))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    response = _client.PostAsync(_url, content).Result;
                }

                return response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesSpellChecker Post");
            }
            return null;
        }

        private string ParseSpellSheckResult(string jsonData, string srcString, out HashSet<string> mistakes)
        {
            var result = srcString;
            mistakes = new HashSet<string>();
            try
            {
                var srcDstWordsMapper = new Dictionary<string, string>();
                var jObj = JObject.Parse(jsonData);
                var flaggedTokens = jObj["flaggedTokens"];
                if (flaggedTokens == null)
                {
                    return null;
                }

                foreach (var token in flaggedTokens)
                {
                    // Find best score suggestion
                    if (token["suggestions"] == null)
                        continue;
                    var suggestions = token["suggestions"];
                    var maxScore = double.MinValue;
                    var maxClosePhrase = "";
                    foreach (var itemSuggestion in suggestions)
                    {
                        var suggestion = itemSuggestion["suggestion"];
                        var score = itemSuggestion["score"];
                        if (suggestion == null)
                            continue;
                        if (score == null)
                            continue;

                        var scoreDouble = Convert.ToDouble(score);
                        if (scoreDouble > maxScore)
                        {
                            maxScore = scoreDouble;
                            maxClosePhrase = suggestion.ToString();
                        }
                        if (token["token"] == null)
                            continue;
                        var srcWord = token["token"].ToString();
                        if (!srcDstWordsMapper.ContainsKey(srcWord))
                        {
                            srcDstWordsMapper.Add(srcWord, maxClosePhrase);
                        }
                    }
                }

                foreach (var item in srcDstWordsMapper)
                {
                    mistakes.Add(item.Key);
                    result = result.Replace(item.Key, item.Value);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesSpellChecker ParseSpellSheckResult");
            }

            return result;
        }
    }
}

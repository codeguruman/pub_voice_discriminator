﻿using System.Collections.Generic;

namespace ATech.TextAnalysis.SpellChecking
{
    public interface ISpellChecker
    {
        string Process(string phrase, out HashSet<string> mistakes);
    }
}

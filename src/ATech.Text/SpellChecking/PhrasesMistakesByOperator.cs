﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ATech.TextAnalysis.SpellChecking
{
    public class PhrasesMistakesByOperator
    {
        public Dictionary<string, HashSet<string>> Data { get; set; }

        public PhrasesMistakesByOperator(ConcurrentDictionary<string, HashSet<string>> src)
        {
            Data = new Dictionary<string, HashSet<string>>();
            foreach (var item in src)
            {
                if (!Data.ContainsKey(item.Key))
                    Data.Add(item.Key, item.Value);
            }
        }
        
    }
}

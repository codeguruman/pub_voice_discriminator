﻿namespace ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis.Utils
{
    public class CsResultKeywords
    {
        public string PhraseId { get; set; }
        public string [] Keywords { get; set; }
    }
}

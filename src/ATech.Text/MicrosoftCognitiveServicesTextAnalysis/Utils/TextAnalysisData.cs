﻿namespace ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis.Utils
{
    public class TextAnalysisData
    {
        public string Language { get; set; }
        public string Id { get; set; }
        public string Text { get; set; }
        public string [] KeyPhrases { get; set; }
        public double Score { get; set; }
    }
}

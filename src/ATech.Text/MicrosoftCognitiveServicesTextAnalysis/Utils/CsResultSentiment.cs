﻿namespace ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis.Utils
{
    public class CsResultSentiment
    {
        public string PhraseId { get; set; }
        public double Sentiment { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Newtonsoft.Json;

namespace ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis.Utils
{
    public class AnalysisResultsCollector
    {
        private readonly string _folderWithResultsKeywords = StatisticsAnalysisConfiguration.Instance.CsAnalysisKeywordsResultsDir;
        private readonly string _folderWithResultsSentiment = StatisticsAnalysisConfiguration.Instance.CsAnalysisSentimentResultsDir;
        private readonly Dictionary<string, List<string>> _keyWordsByPhrase;
        private readonly Dictionary<string, double> _sentimentByPhrase;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public AnalysisResultsCollector()
        {
            _keyWordsByPhrase = new Dictionary<string, List<string>>();
            _sentimentByPhrase = new Dictionary<string, double>();
            PrepareKeywords();
            PrepareSentiment();
        }

        public void AddPhrasesKeyWords(Dictionary<string, List<string>> data)
        {
            try
            {
                foreach (var item in data)
                {
                    if (!_keyWordsByPhrase.ContainsKey(item.Key))
                    {
                        _keyWordsByPhrase.Add(item.Key, item.Value);
                        var fileResultPath = Path.Combine(_folderWithResultsKeywords, Guid.NewGuid() + ".json");
                        var result = new CsResultKeywords
                        {
                            Keywords = item.Value.ToArray(),
                            PhraseId = item.Key
                        };
                        SaveCsKeyWordsDataToFile(result, fileResultPath);
                    }
                    else
                        _keyWordsByPhrase[item.Key] = item.Value;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector AddPhrasesKeyWords");
            }
        }

        
        public void AddPhrasesSentiment(Dictionary<string, double> data)
        {
            try
            {
                foreach (var item in data)
                {
                    if (!_sentimentByPhrase.ContainsKey(item.Key))
                    {
                        _sentimentByPhrase.Add(item.Key, item.Value);
                        var fileResultPath = Path.Combine(_folderWithResultsSentiment, Guid.NewGuid() + ".json");
                        var result = new CsResultSentiment()
                        {
                            Sentiment = item.Value,
                            PhraseId = item.Key
                        };
                        SaveCsSentimentDataToFile(result, fileResultPath);
                    }
                    else
                        _sentimentByPhrase[item.Key] = item.Value;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector AddPhrasesSentiment");
            }
        }

        public List<string> GetPhraseKeyWords(string phrase)
        {
            return _keyWordsByPhrase.ContainsKey(phrase) ? _keyWordsByPhrase[phrase] : null;
        }

        public double? GetPhraseSentiment(string phrase)
        {
            return _sentimentByPhrase.ContainsKey(phrase) ? _sentimentByPhrase[phrase] : (double?) null;
        }

        private void PrepareKeywords()
        {
            if (!Directory.Exists(_folderWithResultsKeywords))
                Directory.CreateDirectory(_folderWithResultsKeywords);
            else
            {
                var files = Directory.GetFiles(_folderWithResultsKeywords);
                foreach (var file in files)
                {
                    GetCsKeywordsDataFromFile(file);
                }
            }
        }

        private void PrepareSentiment()
        {
            if (!Directory.Exists(_folderWithResultsSentiment))
                Directory.CreateDirectory(_folderWithResultsSentiment);
            else
            {
                var files = Directory.GetFiles(_folderWithResultsSentiment);
                foreach (var file in files)
                {
                    GetCsSentimentDataFromFile(file);
                }
            }
        }

        private void GetCsKeywordsDataFromFile(string filePath)
        {
            try
            {
                var jsonData = File.ReadAllText(filePath);
                var result = JsonConvert.DeserializeObject<CsResultKeywords>(jsonData);
                if (!_keyWordsByPhrase.ContainsKey(result.PhraseId))
                    _keyWordsByPhrase.Add(result.PhraseId, result.Keywords.ToList());
                else
                    _keyWordsByPhrase[result.PhraseId] = result.Keywords.ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector GetCsKeywordsDataFromFile");
            }
        }

        private void GetCsSentimentDataFromFile(string filePath)
        {
            try
            {
                var jsonData = File.ReadAllText(filePath);
                var result = JsonConvert.DeserializeObject<CsResultSentiment>(jsonData);

                if (!_sentimentByPhrase.ContainsKey(result.PhraseId))
                    _sentimentByPhrase.Add(result.PhraseId, result.Sentiment);
                else
                    _sentimentByPhrase[result.PhraseId] = result.Sentiment;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector GetCsSentimentDataFromFile");
            }
        }

        private void SaveCsKeyWordsDataToFile(CsResultKeywords data, string fileName)
        {
            try
            {
                var jsonData = JsonConvert.SerializeObject(data);
                File.WriteAllText(fileName, jsonData);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector SaveCsKeyWordsDataToFile");
            }
        }

        private void SaveCsSentimentDataToFile(CsResultSentiment data, string fileName)
        {
            try
            {
                var jsonData = JsonConvert.SerializeObject(data);
                File.WriteAllText(fileName, jsonData);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "AnalysisResultsCollector SaveCsSentimentDataToFile");
            }
        }

    }
}

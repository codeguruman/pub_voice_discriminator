﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ATech.Common.CommonModels;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis.Utils;
using ATech.TextAnalysis.TextAnalyzers;
using Newtonsoft.Json;

namespace ATech.TextAnalysis.MicrosoftCognitiveServicesTextAnalysis
{
    public class CognitiveServicesTextAnalyzer : ITextAnalyzer
    {
        private readonly string _url;

        private readonly string _apiKey;

        private readonly string _language;

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        private readonly AnalysisResultsCollector _analysisResultsCollector;

        public CognitiveServicesTextAnalyzer()
        {
            _url = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.TextAnalysisUrl;
            _apiKey = MainProcessingConfiguration.Instance.MicrosoftCognitiveServicesApiData.TextAnalysisApiKey;
            _language = StatisticsAnalysisConfiguration.Instance.Language.ToString();
            _analysisResultsCollector = new AnalysisResultsCollector();
        }

        public Dictionary<string, List<string>> GetKeyWords(List<IDialogPhrase> phrases)
        {
            var result = new Dictionary<string, List<string>>();
            var notCalculatedEarlierPhrases = new List<IDialogPhrase>();
            foreach (var phrase in phrases)
            {
                var keywords = _analysisResultsCollector.GetPhraseKeyWords(phrase.TextId);
                if (keywords != null)
                    result.Add(phrase.TextId, keywords);
                else
                    notCalculatedEarlierPhrases.Add(phrase);
            }

            if (notCalculatedEarlierPhrases.Count == 0)
                return result;

            var jDoc = GetJsonFromPhrases(notCalculatedEarlierPhrases);
            if (jDoc == null)
                return null;

            var answerKeyPhrases = Post(jDoc, "keyPhrases");
            if (answerKeyPhrases == null)
                return null;

            try
            {
                var textAnalysisAllPhrasesData = JsonConvert.DeserializeObject<TextAnalysisAllPhrasesData>(answerKeyPhrases);
                if (textAnalysisAllPhrasesData == null)
                    return null;

                foreach (var doc in textAnalysisAllPhrasesData.Documents)
                {
                    var key = doc.Id;
                    if (!result.ContainsKey(key))
                        result.Add(key, doc.KeyPhrases.ToList());
                    else
                        result[key] = doc.KeyPhrases.ToList();
                }

                _analysisResultsCollector.AddPhrasesKeyWords(result); // Backup
                return result;
                
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesTextAnalyzer GetKeyWords");
                return null;
            }
        }

        public Dictionary<string, double> GetSentiment(List<IDialogPhrase> phrases)
        {
            var result = new Dictionary<string, double>();

            var notCalculatedEarlierPhrases = new List<IDialogPhrase>();
            foreach (var phrase in phrases)
            {
                var sentiment = _analysisResultsCollector.GetPhraseSentiment(phrase.TextId);
                if (sentiment != null)
                {
                    result.Add(phrase.TextId, (double)sentiment);
                }
                else
                {
                    notCalculatedEarlierPhrases.Add(phrase);
                }
            }
            if (notCalculatedEarlierPhrases.Count == 0)
                return result;

            var jDoc = GetJsonFromPhrases(notCalculatedEarlierPhrases);
            if (jDoc == null)
                return null;

            var answerSentiment = Post(jDoc, "sentiment");
            if (answerSentiment == null)
                return null;

            try
            {
                var textAnalysisAllPhrasesData = JsonConvert.DeserializeObject<TextAnalysisAllPhrasesData>(answerSentiment);
                if (textAnalysisAllPhrasesData == null)
                    return null;

                foreach (var doc in textAnalysisAllPhrasesData.Documents)
                {
                    var key = doc.Id;
                    if (!result.ContainsKey(key))
                        result.Add(key, doc.Score);
                    else
                        result[key] = doc.Score;
                }

                _analysisResultsCollector.AddPhrasesSentiment(result); // Backup
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesTextAnalyzer GetSentiment");
                return null;
            }
        }

        private string GetJsonFromPhrases(IReadOnlyList<IDialogPhrase> phrases)
        {
            try
            {
                var textAnalysisAllPhrasesData = new TextAnalysisAllPhrasesData
                {
                    Documents = new TextAnalysisData[phrases.Count]
                };

                for (var i = 0; i < phrases.Count; i++)
                {
                    textAnalysisAllPhrasesData.Documents[i] = new TextAnalysisData
                    {
                        Id = phrases[i].TextId,
                        Language = _language,
                        Text = phrases[i].Text
                    };
                }
                var json = JsonConvert.SerializeObject(textAnalysisAllPhrasesData);
                return json;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "CognitiveServicesTextAnalyzer GetJsonFromPhrases");
                return null;
            }
        }

        private string Post(string jsonData, string path)
        {
            var bytes = Encoding.UTF8.GetBytes(jsonData);
            var request = (HttpWebRequest)WebRequest.Create(_url + path);

            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "application/json";
            request.Headers.Add("Ocp-Apim-Subscription-Key", $"{_apiKey}");

            try
            {
                request.GetRequestStream().Write(bytes, 0, bytes.Length);
                var response = (HttpWebResponse)request.GetResponse();

                string result;
                using (var rdr = new StreamReader(response.GetResponseStream() ?? throw new Exception("No response from cognitive services")))
                    result = rdr.ReadToEnd();
                return result;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CognitiveServicesTextAnalyzer Post");
                return null;
            }
        }
    }
}

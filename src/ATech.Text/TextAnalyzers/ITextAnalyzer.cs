﻿using System.Collections.Generic;
using ATech.Common.CommonModels;

namespace ATech.TextAnalysis.TextAnalyzers
{
    public interface ITextAnalyzer
    {
        Dictionary<string, List<string>> GetKeyWords(List<IDialogPhrase> phrases);
        Dictionary<string, double> GetSentiment(List<IDialogPhrase> phrases);
    }
}

﻿using System.Collections.Generic;

namespace ATech.TextAnalysis.Interfaces
{
    public interface IPhraseToCorrectMap
    {
        Dictionary<string, List<string>> Map { get; set; }
    }
}

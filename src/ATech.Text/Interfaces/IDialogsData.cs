﻿using System.Collections.Generic;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.TextAnalysis.Models.Context;

namespace ATech.TextAnalysis.Interfaces
{
    public interface IDialogsData
    {
        List<IDialogDescriptor> Dialogs { get; set; }
        List<IAudioDialogDetails> AudioDialogDetails { get; set; }
        List<IRoleContactInformation> Roles { get; set; }
        List<IContextAnalysisType> Contexts { get; set; }
        List<ICalculatableMetric> TextMetrics { get; set; }
        List<ICalculatableMetric> AudioMetrics { get; set; }
        List<IProjectInformation> Projects { get; set; }
        IPhraseToCorrectMap PhraseToCorrectMap { get; set; }
    }
}

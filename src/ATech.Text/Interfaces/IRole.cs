﻿namespace ATech.TextAnalysis.Interfaces
{
    public interface IRole
    {
        string Type { get; set; }

        string Name { get; set; }

        string Id { get; set; }

        string Description { get; set; }
    }
}

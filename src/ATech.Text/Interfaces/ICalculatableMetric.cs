﻿namespace ATech.TextAnalysis.Interfaces
{
    public interface ICalculatableMetric
    {
        string Type { get; set; }
        string Description {get; set; }
    }
}

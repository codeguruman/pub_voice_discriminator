﻿namespace ATech.TextAnalysis.Interfaces
{
    public interface IProjectInformation
    {
        string ProjectId { get; set; }

        string Description { get; set; }
    }
}

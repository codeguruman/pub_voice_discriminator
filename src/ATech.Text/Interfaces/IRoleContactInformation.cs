﻿namespace ATech.TextAnalysis.Interfaces
{
    public interface IRoleContactInformation
    {
        string Name { get; set; }
        string RoleType { get; set; }
        string RoleId { get; set; }
        string ProjectId { get; set; }
        string PhoneNumber { get; set; }
        string Email { get; set; }
        string Description { get; set; }
    }
}

﻿
using ATech.TextAnalysis.Interfaces;

namespace ATech.TextAnalysis.TextDataAnalysisDownloading
{
    public interface IDialogsSourceDataStorage
    {
        IDialogsData Data { set; }
        void Download();
        void DownloadConfigs();
        IDialogsData GetData();
    }
}

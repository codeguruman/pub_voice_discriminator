﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.Configuration;
using ATech.Configuration.Types;
using ATech.Logging.Interfaces;
using ATech.TextAnalysis.Interfaces;
using ATech.TextAnalysis.Models;
using ATech.TextAnalysis.Models.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ATech.TextAnalysis.TextDataAnalysisDownloading
{
    public class DialogsSourceDataDownloaderFromJsonStorage : IDialogsSourceDataStorage
    {
        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public IDialogsData Data { get; set; }

        public void Download()
        {
            var dialogs = DownloadDialogs();
            var roles = DownloadRolesContactInformation();
            var contexts = DownloadContextAnalysisTypes();
            var textMetrics = DownloadTextCalculatableMetrics();
            List<ICalculatableMetric> audioMetrics = null;
            List<IAudioDialogDetails> audioDialogDetails = null;
            if (StatisticsAnalysisConfiguration.Instance.DialogSourceType == DialogSourceType.Audio)
            {
                audioMetrics = DownloadAudioCalculatableMetrics();
                audioDialogDetails = DownloadAudioDialogDetails();
            }
            var projects = DownloadProjectInformation();
            var phraseToCorrectMap = DownloadPhraseToCorrectMap();

            var data = new DialogsData
            {
                Dialogs = dialogs,
                AudioDialogDetails = audioDialogDetails,
                Roles = roles,
                Contexts = contexts,
                AudioMetrics = audioMetrics,
                TextMetrics = textMetrics,
                Projects = projects,
                PhraseToCorrectMap = phraseToCorrectMap
            };
            Data = data;
            CalculateStartAndFinishDates();
        }

        public void DownloadConfigs()
        {
            var contexts = DownloadContextAnalysisTypes();
            var textMetrics = DownloadTextCalculatableMetrics();
            var roles = new List<IRoleContactInformation>()
            {
                new RoleContactInformation()
                {
                    RoleType = "Operator",
                    RoleId = "Operator"
                },
                new RoleContactInformation()
                {
                    RoleType = "Client",
                    RoleId = "Client"
                }
            };

            Data = new DialogsData
            {
                Contexts = contexts,
                TextMetrics = textMetrics,
                Roles = roles
            };
        }

        public IDialogsData GetData()
        {
            return Data;
        }

        private void CalculateStartAndFinishDates()
        {
            var dialogs = Data.Dialogs;
            var minDate = DateTime.MaxValue;
            var maxDate = DateTime.MinValue;

            foreach (var dialog in dialogs)
            {
                if (dialog.Date < minDate)
                {
                    minDate = dialog.Date;
                    continue;
                }
                if (dialog.Date > maxDate)
                {
                    maxDate = dialog.Date;
                }
            }

            StatisticsAnalysisConfiguration.Instance.PeriodStartTakingStatistics = minDate;
            StatisticsAnalysisConfiguration.Instance.PeriodFinishTakingStatistics = maxDate;
        }

        private List<IDialogDescriptor> DownloadDialogs()
        {
            _logger.LogMessage("Downloading Dialogs");
            var result = new List<IDialogDescriptor>();
            try
            {
                var dir = StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.DialogsFolderPath;
                if (!Directory.Exists(dir))
                    throw new Exception($"Not found directory {dir}");

                var dialogFiles = Directory.GetFiles(dir);
                foreach (var dialogFile in dialogFiles)
                {
                    var jsonData = File.ReadAllText(dialogFile);
                    var descriptor = JsonConvert.DeserializeObject<DeserializableDescriptor>(jsonData);
                    var newDesk = descriptor.Convert();

                    if (result.Count > StatisticsAnalysisConfiguration.Instance.MaxProcessingDialogs)
                        break;
                    
                    result.Add(newDesk);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadDialogs");
                _logger.LogError("Error getting dialogs from json", "DialogsSourceDataDownloaderJsonStorage DownloadDialogs");
            }

            return result;
        }

        private List<IAudioDialogDetails> DownloadAudioDialogDetails()
        {
            _logger.LogMessage("Downloading AudioDialogDetails");
            var result = new List<IAudioDialogDetails>();
            try
            {
                var dir = StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.AudioDetailsFolderPath;
                if (!Directory.Exists(dir))
                    throw new Exception($"Not found directory {dir}");

                var audioDetailsFiles = Directory.GetFiles(dir);
                foreach (var item in audioDetailsFiles)
                {
                    var jsonData = File.ReadAllText(item);
                    var audioDialogDetails = JsonConvert.DeserializeObject<AudioDialogDetails>(jsonData);

                    if (result.Count > StatisticsAnalysisConfiguration.Instance.MaxProcessingDialogs)
                        break;

                    result.Add(audioDialogDetails);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadAudioDialogDetails");
                _logger.LogError("Error getting dialogs from json", "DialogsSourceDataDownloaderJsonStorage DownloadAudioDialogDetails");
            }

            return result;
        }

        private List<IRoleContactInformation> DownloadRolesContactInformation()
        {
            _logger.LogMessage("Downloading Roles Contact Information");
            var result = new List<IRoleContactInformation>();
            try
            {
                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.RolesDataPath);
                var rolesJArray = (JArray) JsonConvert.DeserializeObject(jsonData);
                foreach (var roleContactInformation in rolesJArray)
                {
                    result.Add((RoleContactInformation) roleContactInformation.ToObject(typeof(RoleContactInformation)));
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadRolesContactInformation");
                _logger.LogError("Error getting roles from json", "DialogsSourceDataDownloaderJsonStorage DownloadRolesContactInformation");
            }

            return result;
        }

        private List<IProjectInformation> DownloadProjectInformation()
        {
            _logger.LogMessage("Downloading Project Information");
            var result = new List<IProjectInformation>();
            try
            {
                if (!File.Exists(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.ProjectContextFilePath))
                    return result;

                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.ProjectContextFilePath);
                var projectsJArray = (JArray)JsonConvert.DeserializeObject(jsonData);
                foreach (var project in projectsJArray)
                {
                    result.Add((ProjectInformation)project.ToObject(typeof(ProjectInformation)));
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadProjectInformation");
                _logger.LogError("Error getting projects from json", "DialogsSourceDataDownloaderJsonStorage DownloadProjectInformation");
            }

            return result;
        }

        private List<IContextAnalysisType> DownloadContextAnalysisTypes()
        {
            _logger.LogMessage("Downloading Context Analysis Types");
            var result = new List<IContextAnalysisType>();
            try
            {
                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.DialogAnalysisContextFilePath);
                var contextsJArray = (JArray)JsonConvert.DeserializeObject(jsonData);
                foreach (var context in contextsJArray)
                {
                    var item = (ContextAnalysisType)context.ToObject(typeof(ContextAnalysisType));
                    item.KeyPhrases = item.KeyPhrases.Distinct().ToList();
                    result.Add(item);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadContextAnalysisTypes");
                _logger.LogError("Error getting contexts from json", "DialogsSourceDataDownloaderJsonStorage DownloadContextAnalysisTypes");
            }

            return result;
        }



        private List<ICalculatableMetric> DownloadAudioCalculatableMetrics()
        {
            _logger.LogMessage("Downloading Audio Calculatable Metrics");
            var result = new List<ICalculatableMetric>();
            try
            {
                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.AudioMetricsContextFilePath);
                var metricsJArray = (JArray)JsonConvert.DeserializeObject(jsonData);
                foreach (var metric in metricsJArray)
                {
                    result.Add((CalculatableMetric)metric.ToObject(typeof(CalculatableMetric)));
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadAudioCalculatableMetrics");
                _logger.LogError("Error getting metrics from json", "DialogsSourceDataDownloaderJsonStorage DownloadAudioCalculatableMetrics");
            }

            return result;
        }

        private List<ICalculatableMetric> DownloadTextCalculatableMetrics()
        {
            _logger.LogMessage("Downloading Text Calculatable Metrics");
            var result = new List<ICalculatableMetric>();
            try
            {
                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.TextMetricsContextFilePath);
                var metricsJArray = (JArray)JsonConvert.DeserializeObject(jsonData);
                foreach (var metric in metricsJArray)
                {
                    result.Add((CalculatableMetric)metric.ToObject(typeof(CalculatableMetric)));
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadTextCalculatableMetrics");
                _logger.LogError("Error getting metrics from json", "DialogsSourceDataDownloaderJsonStorage DownloadTextCalculatableMetrics");
            }

            return result;
        }

        private IPhraseToCorrectMap DownloadPhraseToCorrectMap()
        {
            _logger.LogMessage("Downloading Phrases To Correct Map");
            var result = new PhraseToCorrectMap();
            try
            {
                var jsonData = File.ReadAllText(StatisticsAnalysisConfiguration.Instance.DialogsDataPathes.PhraseToCorrectMapFilePath);
                var jObj = JObject.Parse(jsonData);
                var pairs = jObj["Map"];
                foreach (var pair in pairs)
                {
                    var key = pair["phrase"].ToString();
                    var value = pair["correctPhrases"];
                    var listCorrectResults = value.Select(correctrPhrase => correctrPhrase.ToString()).ToList();

                    if (result.Map.ContainsKey(key))
                        result.Map[key] = listCorrectResults;
                    else
                        result.Map.Add(key, listCorrectResults);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "DialogsSourceDataDownloaderJsonStorage DownloadPhraseToCorrectMap");
            }

            return result;
        }
    }
}

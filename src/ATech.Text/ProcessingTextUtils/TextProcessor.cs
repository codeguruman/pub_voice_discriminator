﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using HtmlAgilityPack;

namespace ATech.TextAnalysis.ProcessingTextUtils
{
    public class TextProcessor
    {
        private static readonly HashSet<string> NotAnalysisablePhrases;

        private static readonly ILogger Logger = MainProcessingConfiguration.Instance.Logger;

        static TextProcessor()
        {
            NotAnalysisablePhrases = new HashSet<string> { "Здравствуйте!Данное сообщение сформировано автоматически.Наш оператор ответит Вам" };
        }

        private static bool IsStringAnalysible(string input)
        {
            foreach (var item in NotAnalysisablePhrases)
            {
                if (input.Contains(item))
                    return false;
            }

            return true;
        }

        public static string Process(string input)
        {
            try
            {
                if (string.IsNullOrEmpty(input))
                    return null;

                var processedText = HtmlTagsRemoverHtmlAgility(input);
                if (processedText == null)
                    processedText = input;

                processedText = processedText.Replace("\r", @".").Replace("\n", @".");

                processedText = RemoveTagsUseRegex(processedText);
                if (processedText == null)
                    return null;

                processedText = RemoveNotUsedSymbols(processedText);
                if (processedText == null)
                    return null;

                processedText = RemoveGarbageUseRegex(processedText);
                if (processedText == null)
                    return null;

                processedText = RemoveMultSpaces(processedText);

                if (!IsStringAnalysible(processedText))
                    return null;

                return processedText;
            }
            catch (Exception e)
            {
                Logger.LogError(e, "TextProcessor Process");
                return input;
            }

        }

        private static string HtmlTagsRemoverHtmlAgility(string input)
        {
            var result = WebUtility.HtmlDecode(input);
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(result);
            if (htmlDoc.ParseErrors.Count() != 0)
                return null;

            result = htmlDoc.DocumentNode.InnerText;
            return result;
        }

        private static string RemoveTagsUseRegex(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            return Regex.Replace(input, "<.*?>", " ");
        }

        private static string RemoveGarbageUseRegex(string input)
        {
            if (string.IsNullOrEmpty(input))
                return null;

            return Regex.Replace(input, @"[^а-я^А-Я^-^\\n^(^)^,^;^.^!^:^\s^-]", " ");
        }

        private static string RemoveNotUsedSymbols(string input)
        {
            if (input == null)
                return null;

            var newStr = input.Replace("&", " ").Replace("nbsp;", " ");
            var noMultDashesStr = Regex.Replace(newStr, "[-]{2,}", "-");
            var noMultDotsStr = Regex.Replace(noMultDashesStr, "[.]{2,}", ".");
            var noMultCommas = Regex.Replace(noMultDotsStr, "[,]{2,}", ",");
            var noMultExclamationMark = Regex.Replace(noMultCommas, "[!]{2,}", "!");
            return noMultExclamationMark;
        }

        private static string RemoveMultSpaces(string input)
        {
            if (input == null)
                return null;

            var noMultSpacesStr = Regex.Replace(input, "[ ]{2,}", " ");
            return noMultSpacesStr;
        }
    }
}

﻿using System;

namespace ATech.Helpers
{
    public static class Extensions
    {

        public static string ToHexString(this byte[] bytes)
        {
            return BitConverter.ToString(bytes).Replace("-", string.Empty);
        }

        public static long ToUnix(this DateTime dateTime)
        {
            return  (long)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        }
        public static DateTime ToDateTime(this long unixTimeStamp )
        {
            var dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
            return dtDateTime;
        }
    }
}
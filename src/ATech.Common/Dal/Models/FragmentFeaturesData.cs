﻿using ATech.Common.Dal.Models.VoiceFeatures;

namespace ATech.Common.Dal.Models
{
    public class FragmentFeaturesData
    {
        public FragmentFeatures Features { get; set; }
        public int FromSecond { get; set; }
        public int ToSecond { get; set; }
        public string Owner { get; set; }
    }
}

﻿namespace ATech.Common.Dal.Models
{
    public class MoodMetrics
    {
        public const double Threshold = 0.97;
        
        public double AvgMoodLevel { get; set; }

        public int CountBlocksMoodLowerThreshold { get; set; }

        public int CountAnomalySigmaRule { get; set; }
    }
}

﻿using MongoDB.Bson.Serialization.Attributes;

namespace ATech.Common.Dal.Models
{
    [BsonIgnoreExtraElements]
    public class RecognitionTask
    {
        [BsonIgnore]
        public string Secret { get; set; }

        [BsonIgnore]
        public string token { get; set; }
        
        public string Ticket_id { get; set; }
        
        public string File_url { get; set; }

        [BsonIgnore]
        public string callback_url { get; set; }
    }
}

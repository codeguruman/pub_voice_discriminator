﻿namespace ATech.Common.Dal.Models
{
    public class InputSettings
    {
        public int ChannelsCount { get; set; }

        public bool GiveStatRegulation { get; set; }

        public bool GiveTextCategory { get; set; }

        public bool GiveMainVoiceFeatures { get; set; }

        public bool GiveTextFromAudio { get; set; }

        public bool ProcessingFromFile { get; set; }

        public string Language { get; set; }

        public RecognizerType RecognizerType { get; set; }

        public InputSettings()
        {
            ChannelsCount = 2;
            RecognizerType = RecognizerType.Kaldi;
        }
    }
}

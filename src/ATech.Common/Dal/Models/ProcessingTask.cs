﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ATech.Common.Dal.Models
{
    public class ProcessingTask
    {
        [BsonId]
        public string Id { get; set; }

        public string TaskId { get; set; }

        public DateTime CreateDate { get; set; }

        public RecognitionTask RecognitionTask { get; set; }

        public ApiType ApiType { get; set; }

        public AnswerModel AnswerModel { get; set; }

        public InputSettings InputSettings { get; set; }

        public string FilePath { get; set; }

        public string SecondFilePath { get; set; }

        public string FileName { get; set; }

        public string UserKey { get; set; }

        public bool NotificationSent { get; set; }

    }
}

﻿namespace ATech.Common.Dal.Models.VoiceFeatures
{
    public class FragmentFeatures
    {
        public ClassifiedAnswer classifiedanswer { get; set; }

        public double? elapsed { get; set; }

        public double? elapsed_preprocessed { get; set; }

        public LowDetailedFeature low_detailed_features { get; set; }
    }
}

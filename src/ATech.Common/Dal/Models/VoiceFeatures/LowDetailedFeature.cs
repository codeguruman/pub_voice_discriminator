﻿namespace ATech.Common.Dal.Models.VoiceFeatures
{
    public class LowDetailedFeature
    {
        public string mean_amplitude { get; set; }

        public string tempo { get; set; }

        public string zero_crossing_accuracy { get; set; }
    }
}

﻿namespace ATech.Common.Dal.Models.VoiceFeatures
{
    public class RecognitionResult
    {
        public string answer { get; set; }

        public string confidence { get; set; }
    }
}

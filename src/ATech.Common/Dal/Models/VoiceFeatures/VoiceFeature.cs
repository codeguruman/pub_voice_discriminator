﻿namespace ATech.Common.Dal.Models.VoiceFeatures
{
    public class VoiceFeature
    {
        public RecognitionResult recognition_result { get; set; }

        public double recognition_time { get; set; }
    }
}

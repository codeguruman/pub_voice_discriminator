﻿namespace ATech.Common.Dal.Models.VoiceFeatures
{
    public class ClassifiedAnswer
    {
        public string status { get; set; }

        public VoiceFeature age { get; set; }

        public VoiceFeature sex { get; set; }

        public VoiceFeature mood { get; set; }

        public VoiceFeature temp { get; set; }

        public VoiceFeature intelligibility { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ATech.Common.AudioDetails;
using ATech.Common.CommonModels;
using ATech.Common.DialogMetrics;

namespace ATech.Common.Dal.Models
{
    public class AnswerModel
    {
        public AnswerModel()
        {
            Status = "Ok";
        }

        public AnswerModel(string error)
        {
            Error = error;
            Status = "Error";
        }

        public int ProcessingTimeInSeconds { get; set; }

        public string Status { get; set; }

        public AudioDialogDetails AudioDialogDetails { get; set; }

        public AudioDialogDescriptor AudioDialogDescriptor { get; set; }

        public TextCalculatedMetrics TextCalculatedMetrics { get; set; }

        public string TextCategory { get; set; }

        public List<FragmentFeaturesData> FragmentFeatures { get; set; }

        public string Error { get; set; }

        public void FilterFeatureIntervals()
        {
            if (AudioDialogDescriptor == null)
                return;
            
            if (FragmentFeatures == null)
                return;

            var allowIntervals = AudioDialogDescriptor.GetPhrasesIntervalStarts();
            FragmentFeatures = FragmentFeatures.Where(item => allowIntervals.Contains(item.FromSecond)).ToList();
        }

        public BadWordsMetrics CalcBadPhrasesMetrics()
        {
            var result = new BadWordsMetrics();
            var data = TextCalculatedMetrics?.EveryContextKeyPhrases;

            if (data?.Data == null) 
                return result;
            
            result.Phrases = new List<string>();
            var badTypes = new List<string>()
            {
                "ForbiddenPhrases",
                "NotLoyalCustomers",
                "ConflictDialogs",
                "CustomerProblems",
                "UnfulfilledPromisesByCompany",
                "BusinessPointsGrowth"
            };
                
            foreach (var badType in badTypes.Where(badType => data.Data.ContainsKey(badType)))
            {
                if (data.CountByContextType != null)
                {
                    if (data.CountByContextType.ContainsKey(badType))
                    {
                        result.Count += data.CountByContextType[badType];
                    }
                }
                else
                {
                    result.Count += data.Data.Values.Count;
                }

                foreach (var dict in data.Data.Values)
                {
                    result.Phrases.AddRange(dict.Values.Select(x => x.Phrase));
                }
            }

            result.Phrases = result.Phrases.Distinct().ToList();

            return result;
        }

        public bool NeedToSendNotificationAutomatically(int threshold)
        {
            var metrics = CalcMoodMetrics();
            if (metrics?.CountBlocksMoodLowerThreshold != null)
            {
                return metrics.CountBlocksMoodLowerThreshold > threshold;
            }

            return false;
        }

        public MoodMetrics CalcMoodMetrics()
        {
            var metrics = new MoodMetrics();
            FilterFeatureIntervals();

            if (FragmentFeatures == null)
                return metrics;

            var sum = 0.0;
            var count = 0;
            var points = new List<double>();
            foreach (var item in FragmentFeatures)
            {
                var conf = item?.Features?.classifiedanswer?.mood?.recognition_result?.confidence;
                var owner = item?.Owner;

                if (conf == null || owner != "Client")
                {
                    continue;
                }

                var res = item.Features?.classifiedanswer?.mood?.recognition_result?.answer;
                switch (res)
                {
                    case null:
                        continue;
                    case "normal or happy":
                    {
                        var point = Convert.ToDouble(conf, CultureInfo.InvariantCulture);
                        sum += point;
                        points.Add(point);
                        count++;
                        break;
                    }
                    default:
                    {
                        var point = 1.0 - Convert.ToDouble(conf, CultureInfo.InvariantCulture);
                        sum += point;
                        points.Add(point);
                        count++;
                        break;
                    }
                }
            }

            if (count == 0)
            {
                return metrics;
            }

            var mean = Math.Min(sum / count, 1.0);
            var std = Math.Sqrt(points.Sum(p => (p - mean) * (p - mean)) / count);

            metrics.AvgMoodLevel = mean;
            metrics.CountBlocksMoodLowerThreshold = points.Count(p => p < MoodMetrics.Threshold);
            metrics.CountAnomalySigmaRule = points.Count(p => Math.Abs(p - mean) > 2*std);

            return metrics;
        }
    }
}

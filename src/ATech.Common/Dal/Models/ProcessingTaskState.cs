﻿using System;

namespace ATech.Common.Dal.Models
{
    public class ProcessingTaskState
    {
        public string FileName { get; set; }

        public string TaskId { get; set; }

        public DateTime CreateDate { get; set; }

        public string Error { get; set; }

        public string RecognizedText { get; set; }

        public MoodMetrics MoodMetrics { get; set; }

        public BadWordsMetrics BadWordsMetrics { get; set; }

        public bool NotificationSent { get; set; }
    }
}

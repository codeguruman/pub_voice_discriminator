﻿using System.Collections.Generic;

namespace ATech.Common.Dal.Models
{
    public class BadWordsMetrics
    {
        public int Count { get; set; }
        public List<string> Phrases { get; set; }
    }
}

﻿using ATech.Common.CommonModels;

namespace ATech.Common.Dal.Models
{
    public class AudioDialogPhrase : IDialogPhrase
    {
        public string TextId { get; set; }
        public string Confidence { get; set; }
        public string Text { get; set; }
        public string AuthorId { get; set; }
        public double StartingSecond { get; set; }
        public double EndingSecond { get; set; }


    }
}

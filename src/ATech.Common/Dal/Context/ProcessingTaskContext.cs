﻿using ATech.Common.Dal.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ATech.Common.Dal.Context
{
    public class ProcessingTaskContext
    {
        private readonly IMongoDatabase _database;

        private const string CollectionName = "processingtasks";

        public ProcessingTaskContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            _database = client.GetDatabase(settings.Value.Database);
        }

        private void CreateCollection()
        {
            var collectionOptions = new CreateCollectionOptions();
            _database.CreateCollection(CollectionName);
        }

        public IMongoCollection<ProcessingTask> ProcessingTasks
        {
            get
            {
                var collection = _database.GetCollection<ProcessingTask>(CollectionName);
                if (collection != null) return collection;
                CreateCollection();
                collection = _database.GetCollection<ProcessingTask>(CollectionName);
                return collection;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ATech.Common.Dal.Context;
using ATech.Common.Dal.Models;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ATech.Common.Dal.Repositiory
{
    public class ProcessingTaskRepository : IProcessingTaskRepository
    {
        private readonly ProcessingTaskContext _context;

        private readonly ILogger _logger;

        public ProcessingTaskRepository(IOptions<Settings> settings)
        {
            _context = new ProcessingTaskContext(settings);
            _logger = MainProcessingConfiguration.Instance.Logger;
        }

        public async Task<ProcessingTask> GetProcessingTaskByName(string name)
        {
            try
            {
                var filter = Builders<ProcessingTask>.Filter.Eq(item => item.TaskId, name);
                return await _context.ProcessingTasks.Find(filter).FirstOrDefaultAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository GetProcessingTaskByName");
                return null;
            }
        }

        public async Task<ProcessingTask> GetProcessingTaskByFilename(string filename)
        {
            try
            {
                var filter = Builders<ProcessingTask>.Filter.Eq(item => item.FileName, filename);
                return await _context.ProcessingTasks.Find(filter).FirstOrDefaultAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository GetProcessingTaskByFilename");
                return null;
            }
        }

        public async Task InsertProcessingTask(ProcessingTask item)
        {
            try
            {
                if (item != null)
                {
                    item.Id = Guid.NewGuid().ToString();
                    await _context.ProcessingTasks.InsertOneAsync(item);
                }
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository InsertProcessingTask");
            }
        }

        public async Task UpdateProcessingTask(ProcessingTask item)
        {
            try
            {
                await _context.ProcessingTasks.ReplaceOneAsync(t => t.Id == item.Id, item);
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository UpdateProcessingTask");
            }
        }

        public async Task<bool> RemoveTasksWithError()
        {
            try
            {
                var filter = Builders<ProcessingTask>.Filter.
                    Where(item => item.AnswerModel == null 
                                || !string.IsNullOrEmpty(item.AnswerModel.Error)
                                || item.AnswerModel.FragmentFeatures == null 
                                || item.AnswerModel.FragmentFeatures.Count == 0);
                
                await _context.ProcessingTasks.DeleteManyAsync(filter);
                return true;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository RemoveTasksWithError");
                return false;
            }
        }

        public async Task<bool> RemoveTaskById(string taskId)
        {
            try
            {
                var filter = Builders<ProcessingTask>.Filter.
                    Where(item => item.TaskId == taskId);

                await _context.ProcessingTasks.DeleteOneAsync(filter);
                return true;
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository RemoveTasksWithError");
                return false;
            }
        }

        public async Task<List<ProcessingTask>> GetAllProcessingTasks()
        {
            try
            {
                //await _context.ProcessingTasks.DeleteManyAsync(_ => true);
                return await _context.ProcessingTasks.Find(_ => true).ToListAsync();
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, "ProcessingTaskRepository GetAllProcessingTasks");
            }

            return null;
        }
    }
}

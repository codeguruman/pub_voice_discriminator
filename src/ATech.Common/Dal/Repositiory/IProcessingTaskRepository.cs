﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;

namespace ATech.Common.Dal.Repositiory
{
    public interface IProcessingTaskRepository
    {
        Task<ProcessingTask> GetProcessingTaskByName(string name);

        Task<ProcessingTask> GetProcessingTaskByFilename(string filename);

        Task InsertProcessingTask(ProcessingTask item);

        Task UpdateProcessingTask(ProcessingTask item);

        Task<bool> RemoveTasksWithError();

        Task<bool> RemoveTaskById(string taskId);

        Task<List<ProcessingTask>> GetAllProcessingTasks();
    }
}

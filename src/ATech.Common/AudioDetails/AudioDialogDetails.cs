﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace ATech.Common.AudioDetails
{
    public class AudioDialogDetails : IAudioDialogDetails
    {
        public TimeSpan AudioDialogDuration { get; set; }
        public string OperatorId { get; set; }
        public string ClientId { get; set; }
        public double SilenceDurationInSeconds { get; set; }
        public double MaxSilenceIntervalInSeconds { get; set; }
        public int InterruptsCount { get; set; }
        public double MiddleDurationOneTimeSpeech { get; set; }
        public double ClientSpeechDurationPercents { get; set; }
        public double OperatorSpeechDurationPercents { get; set; }

        [JsonIgnore]
        public string MainAudioFilePath { get; set; }

        public void Save(string path)
        {
            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(path, json);
        }
    }
}

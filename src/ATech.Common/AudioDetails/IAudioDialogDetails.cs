﻿using System;

namespace ATech.Common.AudioDetails
{
    public interface IAudioDialogDetails
    {
        TimeSpan AudioDialogDuration { get; set; }

        string OperatorId { get; set; }

        string ClientId { get; set; }

        double SilenceDurationInSeconds { get; set; }

        double MaxSilenceIntervalInSeconds { get; set; }

        int InterruptsCount { get; set; }

        double MiddleDurationOneTimeSpeech { get; set; }

        double ClientSpeechDurationPercents { get; set; }

        double OperatorSpeechDurationPercents { get; set; }

        string MainAudioFilePath { get; set; }

        void Save(string path);
    }
}

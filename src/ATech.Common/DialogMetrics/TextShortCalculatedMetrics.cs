﻿using System.Collections.Generic;
using Newtonsoft.Json; //using ATech.TextAnalysis.SpellChecking;

namespace ATech.Common.DialogMetrics
{
    public class TextShortCalculatedMetrics : ICalculatedMetrics
    {

        #region All Context key Phrases

        public ContextKeyPhrases EveryContextKeyPhrases { get; set; }

        public ContextKeyPhrases EveryContextKeyPhrasesUseCs { get; set; }

        #endregion

        #region Operator context key phrases

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByOperator { get; set; }

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByOperatorUseCs { get; set; }

        #endregion

    }
}

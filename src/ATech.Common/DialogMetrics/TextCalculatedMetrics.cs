﻿using System.Collections.Generic;
using Newtonsoft.Json; //using ATech.TextAnalysis.SpellChecking;

namespace ATech.Common.DialogMetrics
{
    public class TextCalculatedMetrics : ICalculatedMetrics
    {
        public Dictionary<string, int> OperatorDialogsCount { get; set; }

        #region Loyality
        
        [JsonIgnore]
        public Dictionary<string, double> MiddleLoyalityClientsByProjects { get; set; }

        [JsonIgnore]
        public Dictionary<string, double> MiddleLoyalityClientsByOperator { get; set; }

        [JsonIgnore]
        public double MiddleLoyalityClients { get; set; }

        [JsonIgnore]
        public double StandardDeviationLoyalityClients { get; set; }

        [JsonIgnore]
        public double MedianLoyalityClients { get; set; }

        #endregion

        #region Operator phrases mistakes ans typos

        //public PhrasesMistakesByOperator PhrasesMistakesByOperator { get; set; }

        #endregion

        #region All Context key Phrases

        public ContextKeyPhrases EveryContextKeyPhrases { get; set; }

        public ContextKeyPhrases EveryContextKeyPhrasesUseCs { get; set; }

        #endregion

        #region Operator context key phrases

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByOperator { get; set; }

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByOperatorUseCs { get; set; }

        #endregion

        #region Projects context key phrases

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByProject { get; set; }

        public Dictionary<string, ContextKeyPhrases> AllKeyPhrasesByProjectUseCs { get; set; }

        #endregion
    }
}

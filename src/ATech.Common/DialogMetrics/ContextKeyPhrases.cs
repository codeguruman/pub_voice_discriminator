﻿using System;
using System.Collections.Generic;
using System.Linq;
using ATech.Configuration;
using ATech.Logging.Interfaces;

namespace ATech.Common.DialogMetrics
{
    public class ContextKeyPhrases
    {
        public ContextKeyPhrases()
        {
            Data = new Dictionary<string, Dictionary<string, PhraseDescriptor>>();
        }

        public Dictionary<string, Dictionary<string, PhraseDescriptor>> Data { get; set; }

        public Dictionary<string, int> CountByContextType { get; private set; }

        private readonly ILogger _logger = MainProcessingConfiguration.Instance.Logger;

        public int GetCountPhrasesByContextType(string type)
        {
            if (CountByContextType.ContainsKey(type))
                return CountByContextType[type];
            return 0;
        }

        public void ProcessKeyPhrases()
        {
            if (Data == null)
                return;

            CountByContextType = new Dictionary<string, int>();
            foreach (var item in Data)
            {
                foreach (var phraseDescriptor in item.Value)
                {
                    if (CountByContextType.ContainsKey(item.Key))
                        CountByContextType[item.Key] += phraseDescriptor.Value.Count;
                    else
                        CountByContextType.Add(item.Key, phraseDescriptor.Value.Count);
                }
            }
        }

        public List<Tuple<string, PhraseDescriptor>> GetContextKeyPhrasesWithBeingFrequences(string type)
        {
            var result = new List<Tuple<string, PhraseDescriptor>>();
            try
            {
                if (!Data.ContainsKey(type))
                    return result;
                var phrasesDescriptors = Data[type];
                var sortedDict = from entry in phrasesDescriptors orderby entry.Value.Count select entry;
                result.AddRange(sortedDict.Select(item => new Tuple<string, PhraseDescriptor>(item.Key, item.Value)));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "ContextKeyPhrases GetContextKeyPhrasesWithBeingFrequences");
            }

            return result;
        }
    }
}

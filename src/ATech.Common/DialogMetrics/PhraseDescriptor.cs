﻿using System.Collections.Generic;

namespace ATech.Common.DialogMetrics
{
    public class PhraseDescriptor
    {
        /// <summary> Phrase part, we found</summary>
        public string Phrase { get; set; }

        public int Count => PhraseContexts.Count;
        /// <summary> Dialog phrases of talker, that include Phrase part with dialogs id (full phrase, dialogId)</summary>
        public List<PhraseLocationDescriptor> PhraseContexts { get; set; }
    }
}

﻿namespace ATech.Common.DialogMetrics
{
    public class PhraseLocationDescriptor
    {
        public string Phrase { get; set; }

        public string DialogId { get; set; }
    }
}

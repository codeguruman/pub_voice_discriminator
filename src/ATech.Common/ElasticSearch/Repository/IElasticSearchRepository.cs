﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;

namespace ATech.Common.ElasticSearch.Repository
{
    public interface IElasticSearchRepository
    {
        Task UpdateAsync(ProcessingTaskState state);
        
        Task UpdateManyAsync(ProcessingTaskState[] states);

        Task AddAsync(ProcessingTaskState state);

        Task DeleteAsync(ProcessingTaskState state);

        Task<IReadOnlyCollection<ProcessingTaskState>> Find(string query);

        Task<IReadOnlyCollection<ProcessingTaskState>> FindMatch(string phrase);
    }
}

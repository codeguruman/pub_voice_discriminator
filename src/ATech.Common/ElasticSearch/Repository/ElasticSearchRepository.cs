﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ATech.Common.Dal.Models;
using ATech.Configuration;
using ATech.Logging.Interfaces;
using Nest;

namespace ATech.Common.ElasticSearch.Repository
{
    public class ElasticSearchRepository : IElasticSearchRepository
    {
        private readonly ElasticClient _elasticClient;

        private readonly ILogger _logger;

        public ElasticSearchRepository(ElasticClient elasticClient)
        {
            _elasticClient = elasticClient;
            _logger = MainProcessingConfiguration.Instance.Logger;
        }

        public async Task UpdateAsync(ProcessingTaskState state)
        {
            await _elasticClient.UpdateAsync<ProcessingTaskState>(state, u => u.Doc(state));
        }

        public async Task UpdateManyAsync(ProcessingTaskState[] states)
        {
            var result = await _elasticClient.IndexManyAsync(states);
            if (result.Errors)
            {
                foreach (var itemWithError in result.ItemsWithErrors)
                {
                    _logger.LogMessage($"ElasticSearch Error: Failed to index document { itemWithError.Id}: {itemWithError.Error}");
                }
            }
        }

        public async Task AddAsync(ProcessingTaskState state)
        {
            await _elasticClient.IndexDocumentAsync(state);
        }

        public async Task DeleteAsync(ProcessingTaskState state)
        {
            await _elasticClient.DeleteAsync<ProcessingTaskState>(state);
        }

        public async Task<IReadOnlyCollection<ProcessingTaskState>> Find(string query)
        {
            var response = await _elasticClient.SearchAsync<ProcessingTaskState>
            (
                s => 
                    s.Query(q => q.QueryString(d => d.Query(query)))
                    .From(0)
                    .Size(1000));

            if (!response.IsValid)
            {
                _logger.LogMessage("ElasticSearch Error: Failed to search documents");
                return null;
            }

            return response.Documents;
        }

        public async Task<IReadOnlyCollection<ProcessingTaskState>> FindMatch(string phrase)
        {
            return await Find(phrase);
        }
    }
}

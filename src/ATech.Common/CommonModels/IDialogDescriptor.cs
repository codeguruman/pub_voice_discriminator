﻿using System;
using System.Collections.Generic;
using ATech.Common.Dal.Models;

namespace ATech.Common.CommonModels
{
    public interface IDialogDescriptor
    {
        string DialogId { get; set; }
        Guid ReportId { get; set; }
        string OperatorId { get; set; }
        string ClientId { get; set; }
        DateTime Date { get; set; }
        string Channel { get; set; }
        List<AudioDialogPhrase> Phrases { get; set; }
        string AudioFileName { get; set; }
        string FullText { get; set; }

        int GetPhrasesCount(bool isOperator);

        HashSet<int> GetPhrasesIntervalStarts();
    }
}

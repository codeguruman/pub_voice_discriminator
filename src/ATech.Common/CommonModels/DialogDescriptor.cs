﻿using System;
using System.Collections.Generic;
using System.Linq;
using ATech.Common.Dal.Models;
using Newtonsoft.Json;

namespace ATech.Common.CommonModels
{
    public class DialogDescriptor :  IDialogDescriptor
    {
        public Guid ReportId { get; set; }
        public string DialogId { get; set; }
        public string OperatorId { get; set; }
        public string ClientId { get; set; }
        public DateTime Date { get; set; }
        public string Channel { get; set; }
        public List<AudioDialogPhrase> Phrases { get; set; }
        public string AudioFileName { get; set; }
        public string FullText { get; set; }
        public int GetPhrasesCount(bool isOperator)
        {
            var id = isOperator ? OperatorId : ClientId;
            return Phrases.Count(audioDialogPhrase => audioDialogPhrase.AuthorId == id);
        }

        public static DialogDescriptor FromJson(string json, Guid reportId)
        {
            var d = JsonConvert.DeserializeObject<DialogDescriptor>(json);
            d.ReportId = reportId;
            return d;
        }

        public HashSet<int> GetPhrasesIntervalStarts() // Получение интревалов по 3 секунды, которые целиком попадают во фразы
        {
            var result = new HashSet<int>();
            foreach (var phraseIntervals in Phrases
                .Select(audioDialogPhrase => GetIntervalsInsidePhrase(audioDialogPhrase.StartingSecond, audioDialogPhrase.EndingSecond)))
            {
                result.UnionWith(phraseIntervals);
            }

            return result;
        }

        private static HashSet<int> GetIntervalsInsidePhrase(double startPhrase, double endPhrase)
        {
            var result = new HashSet<int>();
            var startInvervalPoint = (((int)startPhrase / 3) + 1) * 3;
            while (startInvervalPoint < endPhrase - 3)
            {
                result.Add(startInvervalPoint);
                startInvervalPoint += 3;
            }
            return result;
        }
    }
}

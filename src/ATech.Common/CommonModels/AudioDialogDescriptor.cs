﻿using System;
using System.Collections.Generic;
using System.Linq;
using ATech.Common.Dal.Models;
using Newtonsoft.Json;

namespace ATech.Common.CommonModels
{
    public class AudioDialogDescriptor : IDialogDescriptor
    {
        public string DialogId { get; set; }
        [JsonIgnore]
        public Guid ReportId { get; set; }
        public string OperatorId { get; set; }
        public string ClientId { get; set; }
        public DateTime Date { get; set; }
        [JsonIgnore]
        public string Channel { get; set; }
        public List<AudioDialogPhrase> Phrases { get; set; }

        public string AudioFileName { get; set; }
        [JsonIgnore]
        public string SecondAudioFileName { get; set; }
        [JsonIgnore]
        public string FullText { get; set; }

        public int GetPhrasesCount(bool isOperator)
        {
            var id = isOperator ? OperatorId : ClientId;
            return Phrases.Count(audioDialogPhrase => audioDialogPhrase.AuthorId == id);
        }

        public HashSet<int> GetPhrasesIntervalStarts() // Получение интревалов по 3 секунды, начала которых попадают во фразы
        {
            var result = new HashSet<int>();
            foreach (var phraseIntervals in Phrases
                .Select(audioDialogPhrase => GetIntervalsInsidePhrase(audioDialogPhrase.StartingSecond, audioDialogPhrase.EndingSecond)))
            {
                result.UnionWith(phraseIntervals);
            }

            return result;
        }

        private static HashSet<int> GetIntervalsInsidePhrase(double startPhrase, double endPhrase)
        {
            var result = new HashSet<int>();
            var startInvervalPoint = (int)startPhrase / 3 * 3;
            while (startInvervalPoint < endPhrase)
            {
                result.Add(startInvervalPoint);
                startInvervalPoint += 3;
            }
            return result;
        }
    }
}

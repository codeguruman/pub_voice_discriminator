﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace ATech.Common.CommonModels
{
    [BsonIgnoreExtraElements]
    public class DbDialog
    {
        public Guid DialogId { get; set; }
        public string OperatorId { get; set; }
        public Guid ClientId { get; set; }
        public Guid ReportId { get; set; }
        public DateTime Date { get; set; }
        public string Channel { get; set; }
        public DbPhrase [] Phrases { get; set; }
        public string AudioFileName { get; set; }
        public string FullText { get; set; }

        public static DbDialog FromJson(string json, Guid reportId)
        {
            var d = JsonConvert.DeserializeObject<DbDialog>(json);
            d.ReportId = reportId;
            return d;
        }
    }
}
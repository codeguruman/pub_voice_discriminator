﻿namespace ATech.Common.CommonModels
{
    public class DialogPhrase : IDialogPhrase
    {
        public string TextId { get; set; }
        public string Text { get; set; }
        public string AuthorId { get; set; }
    }
}

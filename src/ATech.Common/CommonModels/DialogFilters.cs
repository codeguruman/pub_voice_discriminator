﻿using System;

namespace ATech.Common.CommonModels
{
    public class DialogFilters
    {
        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string[] FilteringPhrases { get; set; }

        public string[] Agents { get; set; }
    }

    public class GetDialogRequest
    {
        public string DateFrom { get; set; }

        public string DateTo { get; set; }

        public string Text { get; set; }

        public string Operator { get; set; }
    }
}

﻿using System;

namespace ATech.Common.CommonModels
{
    public class DbPhrase
    {
        public Guid TextId { get; set; }
        public string Text { get; set; }
        public string AuthorId { get; set; }
        public double StartingSecond { get; set; }
        public double EndingSecond { get; set; }
    }
}
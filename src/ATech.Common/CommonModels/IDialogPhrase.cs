﻿namespace ATech.Common.CommonModels
{
    public interface IDialogPhrase
    {
        string TextId { get; set; }
        string Text { get; set; }
        string AuthorId { get; set; }
    }
}

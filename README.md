# README #

** Audio conversation file processor on .NET**
## Determination features, such as recognized speech, interrupts silence, voice features, etc.
## Rules
* No commit binary data
* No commit obj/etc

## API
** Handle mp3 file with two splited channels
POST http://ip:port/api/CallProcessing/HandleMp3
Type: form-data
** answer
good result:
status code 200,
body json
bad result:
status code 400, 500
body null or json with status code